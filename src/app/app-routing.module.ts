import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PagesComponent } from './pages/pages.component';

import { AuthGuard } from './auth/guards/auth.guard';


const routes: Routes = [
  { path: 'auth', loadChildren: () => import('./auth/auth.module').then(m => m.AuthModule) },

  {
    path: '',
    component: PagesComponent,
    children: [
      { path: '', redirectTo: '/dashboard' ,pathMatch:'full'},
      { path: 'dashboard', loadChildren: () => import('./pages/dashboard/dashboard.module').then(m => m.DashboardModule)},
      { path: 'departamentos', loadChildren: () => import('./pages/locations/states/states.module').then(m => m.StatesModule) },
      { path: 'ciudades', loadChildren: () => import('./pages/locations/cities/cities.module').then(m => m.CitiesModule) },
      { path: 'paises', loadChildren: () => import('./pages/locations/countries/countries.module').then(m => m.CountriesModule) },
      { path: 'pacientes', loadChildren: () => import('./pages/patients/patients.module').then(m => m.PatientsModule) },
      { path: 'solicitudes', loadChildren: () => import('./pages/study-requests/study-request.module').then(m => m.StudyRequetsModule) },
      { path: 'instituciones-medicas', loadChildren: () => import('./pages/medical-institutions/medical-institutions.module').then(m => m.MedicalInstitutionsModule) },
      { path: 'medicos', loadChildren: () => import('./pages/doctors/doctors.module').then(m => m.DoctorsModule) },
      { path: 'cirugias', loadChildren: () => import('./pages/surgeries/surgeries.module').then(m => m.SurgeriesModule) },
      { path: 'topografias', loadChildren: () => import('./pages/topographies/topographies.module').then(m => m.TopographiesModule) },
      { path: 'muestras', loadChildren: () => import('./pages/samples/samples.module').then(m => m.SamplesModule) },
      { path: 'usuarios', loadChildren: () => import('./pages/users/users.module').then(m => m.UsersModule) },
      { path: 'roles', loadChildren: () => import('./pages/roles/roles.module').then(m => m.RolesModule) },
      { path: 'permisos', loadChildren: () => import('./pages/permissions/permissions.module').then(m => m.PermissionsModule) },
      { path: 'etapas', loadChildren: () => import('./pages/stages/stages.module').then(m => m.StagesModule) },
      { path: 'evoluciones', loadChildren: () => import('./pages/evolutions/evolutions.module').then(m => m.EvolutionsModule) },
      { path: 'estadios', loadChildren: () => import('./pages/estadio/estadio.module').then(m => m.EstadioModule) },
      { path: 'tnm', loadChildren: () => import('./pages/tnm/tnm.module').then(m => m.TnmModule) },
      { path: 'estirpes-tumorales', loadChildren: () => import('./pages/tumor-lineage/tumor-lineage.module').then(m => m.TumorLineageModule) },
      { path: 'tipos-muestras', loadChildren: () => import('./pages/types-samples/types-samples.module').then(m => m.TypesSamplesModule) },
      { path: 'tipo-ubicacion-muestra', loadChildren: () => import('./pages/type-location-sample/type-location-sample.module').then(m => m.TypeLocationSampleModule) },
      { path: 'almacenes-muestras', loadChildren: () => import('./pages/location-sample/location-sample.module').then(m => m.LocationSampleModule) },

      { path: '**', loadChildren: () => import('./pages/not-found/not-found.module').then(m => m.NotFoundModule) },
    ],
    canActivate:[AuthGuard],
    canActivateChild: [AuthGuard]
  },
  { path: 'families', loadChildren: () => import('./pages/families/families.module').then(m => m.FamiliesModule) },

];

@NgModule({
  imports: [RouterModule.forRoot(routes, { relativeLinkResolution: 'legacy',useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
