import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';


import { AppRoutingModule } from './app-routing.module';

import { SharedModule } from './shared/shared.module';
import { LayoutModule } from "./layout/layout.module";

import { AppComponent } from './app.component';
import { PagesComponent } from './pages/pages.component';
import { MAT_DATE_LOCALE } from '@angular/material/core';
import { indexAuthInterceptor } from './auth/helpers/index-auth.interceptor';
import { MAT_MOMENT_DATE_ADAPTER_OPTIONS } from '@angular/material-moment-adapter';
import { NgScrollbarModule } from 'ngx-scrollbar';



@NgModule({
  declarations: [
    AppComponent,
    PagesComponent,

  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    LayoutModule,
    SharedModule,
    NgScrollbarModule
  ],
  providers: [
    indexAuthInterceptor,
    {provide: MAT_DATE_LOCALE, useValue: 'es-UY'},
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
