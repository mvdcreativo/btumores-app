import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { NologinGuard } from './guards/nologin.guard';


const routes: Routes = [
  // {
  //   path : 'acceder',
  //   component : AccessComponent
  // },
  {
    path : 'login',
    component : LoginComponent,
    canActivate: [
      NologinGuard
    ]
  },
  {
    path : '',
    redirectTo : 'login',
    pathMatch: 'full'
  },
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class AuthRoutingModule { }
