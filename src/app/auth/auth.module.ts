import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

import { LoginComponent } from './login/login.component';
import { AuthRoutingModule } from './auth-routing.module';
import { UserActionsComponent } from './user-actions/user-actions.component';
// import { ModalAuthComponent } from './modal-auth/modal-auth.component';
import { SharedModule } from '../shared/shared.module';


@NgModule({
    declarations: [
        LoginComponent,
        UserActionsComponent,
        // ModalAuthComponent,
    ],
    imports: [
        CommonModule,
        AuthRoutingModule,
        FormsModule,
        ReactiveFormsModule,
        SharedModule,
    ],
    exports: [
        UserActionsComponent
    ]
})
export class AuthModule { }
