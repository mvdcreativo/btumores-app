import { Injectable, EventEmitter, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { map, tap, take, first, catchError } from 'rxjs/operators';
import { Observable, BehaviorSubject, Subscription, pipe } from 'rxjs';
import { HttpClient } from '@angular/common/http';

// import { AuthService as AuthServiceSocial, SocialUser  } from "angularx-social-login";
// import { FacebookLoginProvider, GoogleLoginProvider } from "angularx-social-login";

import { environment } from 'src/environments/environment';
import { CurrentUser, User } from '../pages/users/interfaces/user';
import { MatSnackBar } from '@angular/material/snack-bar';
// import { MatSnackBar } from '@angular/material/snack-bar';

@Injectable({
  providedIn: 'root'
})
export class AuthService implements OnInit, OnDestroy {

  private currentUserSubject$: BehaviorSubject<User> = new BehaviorSubject<any>(null);
  public errorSubject: BehaviorSubject<any> = new BehaviorSubject<any>(null)
  public error$: Observable<any>;
  subscriptions: Subscription[] = []

  // private user: SocialUser;
  private loggedIn: boolean;

  constructor(
    private router: Router,
    private http: HttpClient,
    public snackBar: MatSnackBar,
    // private authServiceSocial: AuthServiceSocial,

  ) {
    this.error$ = this.errorSubject.asObservable();
    // this.subscriptions.push(
    //   this.error$.subscribe( err => err ? this.router.navigate['/auth/login']: null)
    // )
    // if(this.errorValue) this.router.navigate['/auth/login']
  }

  ngOnInit(): void {
  }

  public get errorValue(): any {
    return this.errorSubject.value;
  }

  public setError(err): any {
    return this.errorSubject.next(err);
  }

  setUserNull(){
    this.currentUserSubject$.next(null);
  }

  get currentUser() {
    return this.currentUserSubject$
  }

  /////////////////////////////////
  checkUser() {
    const token = localStorage.getItem('tokenU');

    if (token) {
      return true;

    } else {
      this.currentUserSubject$.next(null);
      ////////////console.log('no log');

      return false
    }


  }
  /////////////////////////////

  //////////////////////////////
  getUserAuth(): Promise<User> {

    return new Promise<User>((resolve, reject) => {
      this.http.post<User>(`${environment.API}auth/user`, "")
        .subscribe(
          res => {
            if (res) {
              // console.log(res);
              this.currentUserSubject$.next(res)

             //console.log('log');
              resolve(res);
            }
          },
          error => {
            localStorage.removeItem('tokenU');
            this.setError(error)
            this.currentUserSubject$.next(null);
            this.router.navigate(['/auth/login']);
           //console.log('nolog');

           //TODO hacer alert con "error token invalido"
            // reject(Error(error))

          }
        )

    })


  }
  ///////////////////////////////

  ////////////////////////////////
  register(credentials: User): Observable<CurrentUser> {
    return this.http.post<any>(`${environment.API}auth/signup`, credentials)
      .pipe(
        map(res => {
         //console.log(res);

          // this.router.navigate(['acceder']);
          // console.log(user);
          if (res?.user?.token) {
            let message, status;
            message = `Hola!! Gracias por egistrarte ${res.user.user.name} `;
            status = 'success';
            // this.snackBar.open(message, '×', { panelClass: [status], verticalPosition: 'top', duration: 5000 });
           //console.log(res);

            // store user details ands token in local storage to keep user logged in between page refreshes
            localStorage.setItem('tokenU', JSON.stringify(res?.user?.token));
            this.currentUserSubject$.next(res?.user?.user);
            // this.router.navigate(['admin'])
            // console.log(user);

          }


          return res;
        }),
      );
  }
  ////////////////////////////////////

  ////////////////////////////////////
  login(credentials: User): Observable<CurrentUser> {
    return this.http.post<any>(`${environment.API}auth/login`, credentials)
      .pipe(
        map(res => {
          // console.log(res);

          // login successful if there's a jwt token in the response
          if (res?.token) {
            // store user details ands token in local storage to keep user logged in between page refreshes
            localStorage.setItem('tokenU', JSON.stringify(res?.token));

            this.currentUserSubject$.next(res.user);

          }

          return res;
        }),
      );
  }
  ///////////////////////////////////

  //////////////////////////////////
  logout() {

    return this.http.get<any>(`${environment.API}auth/logout`)
      .pipe(
        take(1)
      ).subscribe(
        res => {
         //console.log(res);
          // remove user from local storage to log user out
          localStorage.removeItem('tokenU');

          // this.signOut();
          this.currentUserSubject$.next(null);
          this.router.navigate(['/auth/login']);
        },
        error => {
          localStorage.removeItem('tokenU');
          this.currentUserSubject$.next(null);
          this.router.navigate(['/auth/login']);
          this.setError(error)
        }

      )


  }
  ///////////////////////////////////////


  //////////////////////////////////////
  solicitaResetPass(data) {
    return this.http.post<any>(`${environment.API}password/create`, data)
      .pipe(
        take(1)
      ).subscribe(
        res => {
         //console.log(res);

          let message, status;
          message = res.message;
          status = 'success';
          // this.snackBar.open(message, '×', { panelClass: [status], verticalPosition: 'top', duration: 5000 });
        },
        error => {
          let message, status;
          message = error;
          status = 'error';
          // this.snackBar.open(message, '×', { panelClass: [status], verticalPosition: 'top', duration: 5000 });
        }
      )

  }

  ////////////////////////////////////////////////////
  updatePass(data) {
    return this.http.post<any>(`${environment.API}password/reset`, data)
      .pipe(
        take(1)
      )
      .subscribe(
        res => {
         //console.log(res);

          let message, status;
          message = res.message;
          status = 'success';
          // this.snackBar.open(message, '×', { panelClass: [status], verticalPosition: 'top', duration: 5000 });
        },
        error => {
          let message, status;
          message = error;
          status = 'error';
          // this.snackBar.open(message, '×', { panelClass: [status], verticalPosition: 'top', duration: 5000 });
        }

      )
  }

  /////////////////////////////////////////

  ///////////////////////////////////////
  ngOnDestroy() {
    this.subscriptions.map(v => v.unsubscribe())
  }

}
