import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
} from '@angular/common/http';
import { Observable, of, throwError, timer } from 'rxjs';
import { catchError, mergeMap, retryWhen, tap } from 'rxjs/operators';

import { AuthService } from '../auth.service';
import { ActivatedRoute, Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {
  constructor(
    private authService: AuthService,
    private router: Router,
    private snackBar : MatSnackBar
    ) {}
  retryDelay = 500;
  retryMaxAttempts = 1;

  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    return next.handle(request).pipe(this.retryAfterDelay());
  }

  retryAfterDelay(): any {
    return retryWhen((errors) => {
      return errors.pipe(
        mergeMap((err, count) => {
          console.log(err);

          //error usuario no autenticado
          if (err.status === 401) {
            if (err.error.message === 'Unauthenticated.') {
              // console.log("no Autorizado");
              this.authService.setUserNull();
              localStorage.removeItem('tokenU');
              this.router.navigate(['/auth/login']);
              return throwError('Usuario no logueado');
              // return this.router.navigate(['/auth/login']);
            }
          }

          //error roles y permisos
          if (err.status === 403) {
            this.openSnackBar('No cuenta con privilegios para esta operación', 'error-snack-bar')
            return throwError('No cuenta con privilegios para esta operación');
          }

          //error servidor base de datos, registros que contienen relaciones "restrict"
          if (err.error?.message === "actives_relations") {
            this.openSnackBar('Error de integridad! Existen registros que incluyen a este como atributo',
            'error-snack-bar',
            'x',
            5000)
            return throwError('Error de integridad');
          }

          //error servidor generico
          if (count === this.retryMaxAttempts) {
            this.openSnackBar(
              'La comunicación con el servidor ha fallado. Intentelo nuevamente, presione F5. Si el error continua comuniquese con servicio técnico',
              'error-snack-bar',
              '',
              10000
            )
            return throwError(err);
          }

          return of(err).pipe(
            // tap(error => console.log(`Retrying ${error.url}. Retry count ${count + 1}`)),
            mergeMap(() => timer(this.retryDelay))
          );
        })
      );
    });
  }

  openSnackBar(message: string, refClass:string, action: string = 'x', time = 3000) {
    this.snackBar.open(message, action, {
      duration: time,
      horizontalPosition: 'center',
      verticalPosition: 'top',
      panelClass: refClass
    });
  }
}
