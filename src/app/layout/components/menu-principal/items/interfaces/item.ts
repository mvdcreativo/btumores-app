
export interface Item{
    name:string;
    link?:string;
    permissions?:any[];
    icon?:any;
    subItem?:Item[]
}