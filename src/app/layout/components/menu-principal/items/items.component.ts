import { Component, OnDestroy, OnInit } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { AuthService } from 'src/app/auth/auth.service';
import { Item } from './interfaces/item';

@Component({
  selector: 'mvd-items',
  templateUrl: './items.component.html',
  styleUrls: ['./items.component.scss']
})
export class ItemsComponent implements OnInit {

  public items :Item[] =[
    {name:"Inicio",link:"/dashboard",permissions:['patient.index','patient.show','patient.update','patient.create','patient.delete']},
    {name:"Solicitudes",link:"/solicitudes",permissions:['study_request.index','study_request.show','study_request.update','study_request.create','study_request.delete']},
    {name:"Pacientes",link:"/pacientes",permissions:['patient.index','patient.show','patient.update','patient.create','patient.delete']},
    {name:"Muestras",link:"/muestras",permissions:['sample.index','sample.show','sample.update','sample.create','sample.delete']},
    {name:"Conf. Usuarios",permissions:['user.index','user.show','user.update','user.create','user.delete'], subItem:[
      {name:"Usuarios",link:"/usuarios",permissions:['user.index','user.show','user.update','user.create','user.delete']},
      {name:"Roles",link:"/roles",permissions:['role.index','role.show','role.update','role.create','role.delete']},
      {name:"Permisos",link:"/permisos",permissions:['permission.index','permission.show','permission.update','permission.create','permission.delete']},
    ]},
    {name:"Almacén de Muestras",permissions:['stage.show','stage.update','stage.create','stage.delete'], subItem:[
      {name:"Tipos",link:"/tipo-ubicacion-muestra",permissions:['stage.index','stage.show','stage.update','stage.create','location.delete']},
      {name:"Almacenes de Muestras",link:"/almacenes-muestras",permissions:['stage.index','stage.show','stage.update','stage.create','stage.delete']},
    ]},
    {name:"Conf. Funcionales",permissions:['stage.update','stage.create','stage.delete'], subItem:[
      {name:"Etapas Muestras",link:"/etapas",permissions:['stage.update','stage.create','stage.delete']},
      {name:"Evoluciones Paciente",link:"/evoluciones",permissions:['evolution.update','evolution.create','evolution.delete']},
      {name:"Médicos",link:"/medicos",permissions:['doctor.update','doctor.create','doctor.delete']},
      {name:"Estadios",link:"/estadios",permissions:['estadio.update','estadio.create','estadio.delete']},
      {name:"Instituciones Médicas",link:"/instituciones-medicas",permissions:['medicalInstitution.update','medicalInstitution.create','medicalInstitution.delete']},
      {name:"Cirugías",link:"/cirugias",permissions:['surgery.update','surgery.create','surgery.delete']},
      // {name:"TNM",link:"/tnm",permissions:['tnm.update','tnm.create','tnm.delete']},
      {name:"Topografías",link:"/topografias",permissions:['topography.update','topography.create','topography.delete']},
      {name:"Estirpes Tumorales",link:"/estirpes-tumorales",permissions:['tumorLineage.update','tumorLineage.create','tumorLineage.delete']},
      {name:"Tipos de Muestras",link:"/tipos-muestras",permissions:['typeSample.update','typeSample.create','typeSample.delete']},
    ]},
    {name:"Conf. Geolacalización",permissions:['ubication.show','ubication.update','ubication.create','ubication.delete'], subItem:[
      {name:"Paises",link:"/paises",permissions:['ubication.index','ubication.show','ubication.update','ubication.create','ubication.delete']},
      {name:"Departamentos",link:"/departamentos",permissions:['ubication.index','ubication.show','ubication.update','ubication.create','ubication.delete']},
      {name:"Ciudades",link:"/ciudades",permissions:['ubication.index','ubication.show','ubication.update','ubication.create','ubication.delete']},
    ]},

  ]


  constructor(
    private authService: AuthService
  ) {

   }

  ngOnInit(): void {

  }





}
