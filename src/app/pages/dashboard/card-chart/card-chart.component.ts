import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';


@Component({
  selector: 'mvd-card-chart',
  templateUrl: './card-chart.component.html',
  styleUrls: ['./card-chart.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CardChartComponent implements OnInit {

  @Input() title;
  @Input() data = [];


  view: number[] = [700, 400];

  // options
  gradient: boolean = false;
  showLegend: boolean = true;
  showLabels: boolean = true;
  isDoughnut: boolean = false;
  animations: boolean = false;

  colorScheme: any = {
    domain: ['#2962FF', 'rgb(41, 98, 255,0.5)', '#40a6ce','#40a6ce']
  };

  constructor() {

  }

  ngOnInit(): void {
    // console.log(this.data);

    Object.assign(this.data);
  }

  onSelect(data): void {
    console.log('Item clicked', JSON.parse(JSON.stringify(data)));
  }

  onActivate(data): void {
    console.log('Activate', JSON.parse(JSON.stringify(data)));
  }

  onDeactivate(data): void {
    console.log('Deactivate', JSON.parse(JSON.stringify(data)));
  }
}
