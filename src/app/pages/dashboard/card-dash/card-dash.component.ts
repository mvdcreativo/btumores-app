import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'mvd-card-dash',
  templateUrl: './card-dash.component.html',
  styleUrls: ['./card-dash.component.scss']
})
export class CardDashComponent implements OnInit {

  @Input() title;
  @Input() value = 0;

  constructor() { }

  ngOnInit(): void {
  }

}
