import { Component, OnInit } from '@angular/core';
import { concat, forkJoin, merge, Observable, Subscription } from 'rxjs';
import { debounce, debounceTime, first } from 'rxjs/operators';
import { DashService } from './service/dash.service';

@Component({
  selector: 'mvd-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  familyCount: number = 0 ;
  familyStudiedCount: number = 0;
  subscriptions: Subscription[] = [];
  patients: number = 0;
  samples: number = 0;
  samples_bt: number = 0;
  samples_gcu: number = 0;
  samples_end: number = 0;
  samples_in_process: number = 0;
  patientsGCU: number = 0;
  patientsBT: number = 0;
  constructor(
    private dashService:DashService
  ) { }

  ngOnInit(): void {

    this.subscriptions.push(
      this.dashService.familyCount$.subscribe(res=> {
        this.familyCount = res
      }),
      this.dashService.familyStudiedCount$.subscribe(res=> {
        this.familyStudiedCount = res
      }),
      this.dashService.patientCount$.subscribe(res=> {
        this.patients = res
      }),
      this.dashService.samplesCount$.subscribe(res=> {
        this.samples = res
      }),
      this.dashService.samplesBTCount$.subscribe(res=> {
        this.samples_bt = res
      }),
      this.dashService.samplesCGUCount$.subscribe(res=> {
        this.samples_gcu = res
      }),
      this.dashService.samplesEndCount$.subscribe(res=> {
        this.samples_end = res
      }),
      this.dashService.samplesInProcessCount$.subscribe(res=> {
        this.samples_in_process = res
      }),
      this.dashService.patientsBTCount$.subscribe(res=> {
        this.patientsBT = res
      }),
      this.dashService.patientsCGUCount$.subscribe(res=> {
        this.patientsGCU = res
      }),
    )

  }


  dataFamily(){
    return [
      {name: 'Estudiadas', value: +this.familyStudiedCount  },
      {name: 'No estudiadas', value: +this.familyCount - +this.familyStudiedCount },
    ]
  }
  dataSamples(){
    return [
      {name: 'Muestras BT', value: +this.samples_bt },
      {name: 'Muestras CGU', value: +this.samples_gcu },
    ]
  }
  dataSamples_status(){
    return [
      {name: 'Sin procesar', value: +this.samples - +this.samples_end },
      {name: 'En proceso', value: +this.samples_in_process  },
      {name: 'Procesadas', value: +this.samples_end  },
    ]
  }

  get dataPatients(){
    return [
      {name: 'Pacientes BT', value: +this.patientsBT  },
      {name: 'Pacientes CGU', value: +this.patientsGCU  },
    ]
  }

  ngOnDestroy(): void {
    this.subscriptions.map(s=> s.unsubscribe())
  }
}
