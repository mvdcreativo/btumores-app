import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { DashboardComponent } from './dashboard.component';
import { CardDashComponent } from './card-dash/card-dash.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { CardChartComponent } from './card-chart/card-chart.component';
import { NgxChartsModule } from '@swimlane/ngx-charts';

@NgModule({
  declarations: [
    DashboardComponent,
    CardDashComponent,
    CardChartComponent
  ],
  imports: [
    CommonModule,
    DashboardRoutingModule,
    NgxChartsModule,
    SharedModule,
  ]
})
export class DashboardModule { }
