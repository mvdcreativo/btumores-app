import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, take } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { Response } from 'src/app/shared/interfaces/response';
import { BehaviorSubject } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class DashService {

  constructor(
    private http: HttpClient
  ) { }


  private familyCountSubject$: BehaviorSubject<number> = new BehaviorSubject(0)
  private familyStudiedCountSubject$: BehaviorSubject<number> = new BehaviorSubject(0)
  private patientCountSubject$: BehaviorSubject<number> = new BehaviorSubject(0)
  private samplesCountSubject$: BehaviorSubject<number> = new BehaviorSubject(0)
  private samplesBTCountSubject$: BehaviorSubject<number> = new BehaviorSubject(0)
  private samplesCGUCountSubject$: BehaviorSubject<number> = new BehaviorSubject(0)
  private samplesEndCountSubject$: BehaviorSubject<number> = new BehaviorSubject(0)
  private samplesInProcessCountSubject$: BehaviorSubject<number> = new BehaviorSubject(0)
  private patientsCGUCountSubject$: BehaviorSubject<number> = new BehaviorSubject(0)
  private patientsBTCountSubject$: BehaviorSubject<number> = new BehaviorSubject(0)

///
  public get familyCount$() {
    this.getDataDash('family').pipe(take(1)).subscribe()
    return this.familyCountSubject$.asObservable()
  }
  public setFamilyCount(value) {
    this.familyCountSubject$.next(value)
  }

///
  public get familyStudiedCount$() {
    this.getDataDash('family','studied','1','=').pipe(take(1)).subscribe()
    return this.familyStudiedCountSubject$.asObservable()
  }
  public setFamilyStudiedCount(value) {
    this.familyStudiedCountSubject$.next(value)
  }
///
  public get patientCount$() {
    this.getDataDash('patient').pipe(take(1)).subscribe()
    return this.patientCountSubject$.asObservable()
  }
  public setpatientCount(value) {
    this.patientCountSubject$.next(value)
  }
///
  public get samplesCount$() {
    this.getDataDash('sample').pipe(take(1)).subscribe()
    return this.samplesCountSubject$.asObservable()
  }
  public setSamplesCount(value) {
    this.samplesCountSubject$.next(value)
  }
///
public get samplesBTCount$() {
  this.getDataDash('sample','type_patient','#BT','=','patient').pipe(take(1)).subscribe()
  return this.samplesBTCountSubject$.asObservable()
}
public setSamplesBTCount(value) {
  this.samplesBTCountSubject$.next(value)
}

///

public get samplesCGUCount$() {
  this.getDataDash('sample','type_patient','#GCU','=','patient').pipe(take(1)).subscribe()
  return this.samplesCGUCountSubject$.asObservable()
}
public setSamplesCGUCount(value) {
  this.samplesCGUCountSubject$.next(value)
}
///

///

public get samplesEndCount$() {
  this.getSamplesEnd().pipe(take(1)).subscribe()
  return this.samplesEndCountSubject$.asObservable()
}
public setSamplesEndCount(value) {
  this.samplesEndCountSubject$.next(value)
}
///

public get samplesInProcessCount$() {
  this.getSamplesInProcess().pipe(take(1)).subscribe()
  return this.samplesInProcessCountSubject$.asObservable()
}
public setSamplesInProcessCount(value) {
  this.samplesInProcessCountSubject$.next(value)
}
///

public get patientsCGUCount$() {
  this.getDataDash('patient','type_patient','#GCU','=').pipe(take(1)).subscribe()
  return this.patientsCGUCountSubject$.asObservable()
}
public setPatientsCGUCount(value) {
  this.patientsCGUCountSubject$.next(value)
}
///

public get patientsBTCount$() {
  this.getDataDash('patient','type_patient','#BT','=').pipe(take(1)).subscribe()
  return this.patientsBTCountSubject$.asObservable()
}
public setPatientsBTCount(value) {
  this.patientsBTCountSubject$.next(value)
}
///


  getDataDash(model='',column='',filter_value='',conditional='',model_relation=''){

    let params = new HttpParams();
    params = params.append('model', model);
    params = params.append('column', column);
    params = params.append('filter_value', filter_value);
    params = params.append('conditional', conditional);
    params = params.append('model_relation', model_relation);

    return this.http.get<Response>(`${environment.API}data_dash`, {
      params,
    }).pipe(map(
      res => {

        if(model==='family'&& column===''&& filter_value===''&& conditional===''&& model_relation===''){
            this.setFamilyCount(res.data)
        }
        if(model==='family'&& column==='studied'&& filter_value==='1'&& conditional==='='&& model_relation===''){
          this.setFamilyStudiedCount(res.data)
        }
        if(model==='patient'&& column===''&& filter_value===''&& conditional===''&& model_relation===''){
          this.setpatientCount(res.data)
        }
        if(model==='sample'&& column===''&& filter_value===''&& conditional===''&& model_relation===''){
          this.setSamplesCount(res.data)
        }
        if(model==='sample'&& column==='type_patient'&& filter_value==='#BT'&& conditional==='='&& model_relation==='patient'){
          this.setSamplesBTCount(res.data)
        }
        if(model==='sample'&& column==='type_patient'&& filter_value ==='#GCU'&& conditional ==='=' && model_relation==='patient'){
          this.setSamplesCGUCount(res.data)
        }
        if(model==='patient' && column==='type_patient' && filter_value==='#BT' && conditional ==='=' && model_relation===''){
          this.setPatientsBTCount(res.data)
        }
        if(model==='patient'&& column==='type_patient'&& filter_value==='#GCU'&& conditional==='='&& model_relation===''){
          this.setPatientsCGUCount(res.data)
        }

        const resp = res.data
        return resp;
      }
    ))
  }

  getSamplesEnd(){

    return this.http.get<Response>(`${environment.API}data_dash_samples_end`).pipe(map(
      res => {
        const resp = res.data
        this.setSamplesEndCount(resp)
        return resp;
      }
    ))
  }

  getSamplesInProcess(){

    return this.http.get<Response>(`${environment.API}data_dash_samples_in_process`).pipe(map(
      res => {
        const resp = res.data
        this.setSamplesInProcessCount(resp);
        return resp;
      }
    ))
  }

}


