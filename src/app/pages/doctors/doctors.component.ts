import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { Observable, Subscription } from 'rxjs';
import { map, take } from 'rxjs/operators';
import { Column } from 'src/app/shared/components/data-table/interfaces/table';
import { ModalReutilComponent } from 'src/app/shared/components/modals/modal-reutil/modal-reutil.component';
import { ResponsePaginate } from 'src/app/shared/interfaces/response';
import { Doctor } from './interfaces/doctor';
import { DoctorsService } from './services/doctors.service';

@Component({
  selector: 'mvd-doctors',
  templateUrl: './doctors.component.html',
  styleUrls: ['./doctors.component.scss']
})
export class DoctorsComponent implements OnInit {


  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  ////COLUMNAS TABLA
  public columns: Column[] = [
    { title: 'Nombre', col: 'name' },
    { title: 'Apellido', col: 'last_name' },
    { title: 'Contacto', col: 'contact' },
  ]

  totalResut: Observable<number>;
  pageDefault = 1;
  perPage: number = 10;
  orden: string = 'desc';
  filter: string = '';
  result: Observable<ResponsePaginate>;
  subscroption: Subscription;
  dataSource: Observable<any[]>;


  constructor(
    private doctorService: DoctorsService,
    public dialog: MatDialog,

  ) {
    this.result = this.doctorService.resultItems$

  }

  ngOnInit(): void {

    this.getDoctors(this.pageDefault, this.perPage, this.filter, this.orden)

  }

  paginatorChange(e: PageEvent) {
    //console.log(e);
    this.getDoctors(e.pageIndex + 1, e.pageSize)

  }

  getDoctors(currentPage?, perPage?, filter?, sort?) {
    this.subscroption = this.doctorService.getDoctors(currentPage, perPage, filter, sort).subscribe(next => this.loadData());
  }


  loadData() {

    this.dataSource = this.result.pipe(map(v => {
      const dataTable = v.data.data.map(x => {
        return {
          id: x.id,
          name: x.name,
          last_name: x?.last_name,
          contact: x?.contact
        }
      })
      return dataTable;
    }))

    this.totalResut = this.result.pipe(map(v => v.data.total))
  }

  search(filter) {
    this.getDoctors(this.pageDefault, this.perPage, filter, this.orden)
  }


  deleteItem(id): Observable<any> {
    return this.doctorService.deleteDoctor(id)
  }

  itemAction(event) {
    //console.log(event);

    if (event.action === "delete") {
      this.deleteItem(event.element.id).pipe(take(1)).subscribe()
    }
    if (event.action === "edit") {
      this.openDialog('Edita Médico',event.element)
    }
  }

  openDialog(title_form = 'Agrega Médico', data?) {


    const dataDielog = { title: title_form, data: this.setFields(data) };

    const dialogRef = this.dialog.open(ModalReutilComponent, {
      width: '550px',
      data: dataDielog
    });

    return dialogRef.afterClosed().subscribe(result => {
      // this.animal = result;
      if (result) {
        if (result.id) {
          //console.log(result);

          this.updateDoctor(result)

        } else {
          this.storeDoctor(result)
        }

      }

    });
  }

  updateDoctor(data) {
    this.doctorService.updateDoctor(data).pipe(take(1)).subscribe(res=> this.getDoctors(this.pageDefault, this.perPage, this.filter, this.orden))
  }

  storeDoctor(data) {
    this.doctorService.storeDoctor(data).pipe(take(1)).subscribe(
      res => {
        this.getDoctors(this.pageDefault, this.perPage, this.filter, this.orden)
      }
    )
  }

  setFields(elementEdit? : Doctor) {
    // console.log(elementEdit);

    const fields = [
      { nameControl: 'id', type: 'hidden', value: elementEdit?.id, label: 'Id' },
      { nameControl: 'name', type: 'text', value: elementEdit?.name, label: 'Nombre', class:'mvd-col1--1' },
      { nameControl: 'last_name', type: 'text', value: elementEdit?.last_name, label: 'Apellido', class:'mvd-col1--1' },
      { nameControl: 'contact', type: 'text', value: elementEdit?.contact, label: 'Contacto', class:'mvd-col1--1' },
    ]

    return fields
  }

  ngOnDestroy(): void {
    this.subscroption.unsubscribe()
  }


}
