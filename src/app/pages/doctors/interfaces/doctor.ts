export interface Doctor {
    id: number;
    name:string;
    last_name:string;
    contact:string;

}
