import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EstadioComponent } from './estadio.component';

const routes: Routes = [{ path: '', component: EstadioComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EstadioRoutingModule { }
