import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EstadioRoutingModule } from './estadio-routing.module';
import { EstadioComponent } from './estadio.component';
import { SharedModule } from 'src/app/shared/shared.module';


@NgModule({
  declarations: [EstadioComponent],
  imports: [
    CommonModule,
    EstadioRoutingModule,
    SharedModule
  ]
})
export class EstadioModule { }
