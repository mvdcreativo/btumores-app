import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { Observable, Subscription } from 'rxjs';
import { map, take } from 'rxjs/operators';
import { Column } from 'src/app/shared/components/data-table/interfaces/table';
import { Fields } from 'src/app/shared/components/dinamic-form/interfaces/fields';
import { ModalReutilComponent } from 'src/app/shared/components/modals/modal-reutil/modal-reutil.component';
import { ResponsePaginate } from 'src/app/shared/interfaces/response';
import { Evolution } from './interfaces/evolution';
import { EvolutionsService } from './services/evolutions.service';

@Component({
  selector: 'mvd-evolutions',
  templateUrl: './evolutions.component.html',
  styleUrls: ['./evolutions.component.scss']
})
export class EvolutionsComponent implements OnInit {


  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  ////COLUMNAS TABLA
  public columns: Column[] = [
    { title: 'Nombre', col: 'name' },
    { title: 'Etapa de fin', col: 'end', type: 'boolean'},

  ]

  totalResut: Observable<number>;
  pageDefault = 1;
  perPage: number = 10;
  orden: string = 'desc';
  filter: string = '';
  result: Observable<ResponsePaginate>;
  subscroption: Subscription;
  dataSource: Observable<any[]>;


  constructor(
    private evolutionService: EvolutionsService,
    public dialog: MatDialog,

  ) {
    this.result = this.evolutionService.resultItems$

  }

  ngOnInit(): void {

    this.getEvolutions(this.pageDefault, this.perPage, this.filter, this.orden)

  }

  paginatorChange(e: PageEvent) {
    //console.log(e);
    this.getEvolutions(e.pageIndex + 1, e.pageSize)

  }

  getEvolutions(currentPage?, perPage?, filter?, sort?) {
    this.subscroption = this.evolutionService.getEvolutions(currentPage, perPage, filter, sort).subscribe(next => this.loadData());
  }


  loadData() {

    this.dataSource = this.result.pipe(map(v => {
      const dataTable = v.data.data.map(x => {
        return {
          id: x.id,
          name: x.name,
          end: x?.end,
        }
      })
      return dataTable;
    }))

    this.totalResut = this.result.pipe(map(v => v.data.total))
  }

  search(filter) {
    this.getEvolutions(this.pageDefault, this.perPage, filter, this.orden)
  }


  deleteItem(id): Observable<any> {
    return this.evolutionService.deleteEvolution(id)
  }

  itemAction(event) {
    //console.log(event);

    if (event.action === "delete") {
      this.deleteItem(event.element.id).pipe(take(1)).subscribe()
    }
    if (event.action === "edit") {
      this.openDialog('Edita Evolución',event.element)
    }
  }

  openDialog(title_form = 'Agrega Evolución', data?) {


    const dataDielog = { title: title_form, data: this.setFields(data) };

    const dialogRef = this.dialog.open(ModalReutilComponent, {
      width: '550px',
      data: dataDielog
    });

    return dialogRef.afterClosed().subscribe(result => {
      // this.animal = result;
      if (result) {
        if (result.id) {
          //console.log(result);

          this.updateEvolution(result)

        } else {
          this.storeEvolution(result)
        }

      }

    });
  }

  updateEvolution(data) {
    this.evolutionService.updateEvolution(data).pipe(take(1)).subscribe()
  }

  storeEvolution(data) {
    this.evolutionService.storeEvolution(data).pipe(take(1)).subscribe(
      res => {
        //console.log(res);
      }
    )
  }

  setFields(elementEdit? : Evolution) {
    // console.log(elementEdit);

    const fields = [
      { nameControl: 'id', type: 'hidden', value: elementEdit?.id, label: 'Id' },
      { nameControl: 'name', type: 'text', value: elementEdit?.name, validators: [Validators.required], label: 'Nombre', class:'mvd-col1--1' },
      { nameControl: 'end', type: 'checkbox', value: elementEdit?.end, label: 'Etapa final' },
    ]

    return fields
  }

  ngOnDestroy(): void {
    this.subscroption.unsubscribe()
  }





}
