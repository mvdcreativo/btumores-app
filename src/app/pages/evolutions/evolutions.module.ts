import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EvolutionsRoutingModule } from './evolutions-routing.module';
import { EvolutionsComponent } from './evolutions.component';
import { SharedModule } from 'src/app/shared/shared.module';


@NgModule({
  declarations: [EvolutionsComponent],
  imports: [
    CommonModule,
    EvolutionsRoutingModule,
    SharedModule
  ]
})
export class EvolutionsModule { }
