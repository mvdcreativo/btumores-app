export interface Evolution {
    id?: number;
    name:string;
    end:boolean;
    active:boolean;
}
