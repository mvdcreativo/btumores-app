import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { concatMap, map, take } from 'rxjs/operators';
import { ResponsePaginate, Response } from 'src/app/shared/interfaces/response';
import { UbicationService } from 'src/app/shared/services/ubications/ubication.service';
import { environment } from 'src/environments/environment';
import { Evolution } from '../interfaces/evolution'

@Injectable({
  providedIn: 'root'
})
export class EvolutionsService {


  private dataOptionsForms$ : BehaviorSubject<any> = new BehaviorSubject<any>(null)
  public get optionsForms$() {
    return this.dataOptionsForms$
  }

  private evolutionEditSubject$ : BehaviorSubject<Evolution> = new BehaviorSubject<Evolution>(null)
  private urlUtils = "src/app/shared/utils/data/";

  private resultSubject$: BehaviorSubject<ResponsePaginate> = new BehaviorSubject(null)
  public get resultItems$() {
    return this.resultSubject$
  }
  public setItems(value) {
    this.resultSubject$.next(value)
  }



  constructor(
    private http:HttpClient,
    private snackBar: MatSnackBar,
    private ubicationService: UbicationService,
  ) {
    // this.loadData()
  }

  get evolutionOnEdit():Observable<Evolution>{
    return this.evolutionEditSubject$
  }

  setEvolutionOnEdit(value){
    this.evolutionEditSubject$.next(value)
  }

  loadData(){
    const data = of(
      this.ubicationService.getStates(),
      this.ubicationService.getCities(),
      this.ubicationService.getCountries(),
    )

    const dataMerge = data.pipe(
      concatMap(v=>v)
    );

    dataMerge.subscribe(
      res => console.log(res)
    );

  }

  storeEvolution(data): Observable<Evolution>{
    return this.http.post<Response>(`${environment.API}evolutions`, data).pipe(
      map( v => {
        this.getEvolutions(1, 20, '', 'desc').pipe(take(1)).subscribe()
        //snacbarr
        this.openSnackBar('Se creó correctamente','success-snack-bar')
        //////////
        return v.data
      })

    )

  }

  updateEvolution(data): Observable<Evolution>{
    data._method = "put";
    return this.http.post<Response>(`${environment.API}evolutions/${data.id}`, data).pipe(
      map( v => {
        this.getEvolutions(1, 20, '', 'desc').pipe(take(1)).subscribe()
        //snacbarr
        this.openSnackBar('Actualizado correctamente','success-snack-bar')
        //////////
        return v.data
      })

    )

  }

  deleteEvolution(id){
    return this.http.delete<Response>(`${environment.API}evolutions/${id}`).pipe(
      map( v => {

        this.getEvolutions(1, 20, '', 'desc').pipe(take(1)).subscribe()
        //snacbarr
        this.openSnackBar('Eliminado correctamente','success-snack-bar')
        //////////
        return v.data

      })

    )
  }


  getEvolution(id) : Observable<Evolution>{
    return this.http.get<Response>(`${environment.API}evolutions/${id}`).pipe(map(
      res => {
        this.setEvolutionOnEdit(res.data)
        const resp = res.data
        return resp;
      }
    )

    )
  }


///listar
  getEvolutions(currentPage = 1, perPage = 20, filter='', sort= 'desc') : Observable<ResponsePaginate>{
    return this.http.get<ResponsePaginate>(`${environment.API}evolutions`, {
      params: new HttpParams()
        .set('page', currentPage.toString())
        .set('filter', filter)
        .set('sort', sort)
        .set('per_page', perPage.toString())

    }).pipe(map(
      res => {

        this.setEvolutionOnEdit(null)
        this.setItems(res)
        const resp = res
        return resp;
      }
    )

    )
  }



  openSnackBar(message: string, refClass:string, action: string = '') {
    this.snackBar.open(message, action, {
      duration: 2000,
      horizontalPosition: 'center',
      verticalPosition: 'top',
      panelClass: refClass
    });
  }
}
