import { Component, Input, OnInit } from '@angular/core';
import {
  FormGroup,
  FormBuilder,
  Validators,
  AbstractControl,
} from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Subscription, Observable, Subject } from 'rxjs';
import { map, startWith, take, takeUntil } from 'rxjs/operators';
import { Patient } from 'src/app/pages/patients/interfaces/patient';
import { PatientsService } from 'src/app/pages/patients/services/patients.service';
import { ModalReutilComponent } from 'src/app/shared/components/modals/modal-reutil/modal-reutil.component';
import { ConsultaCgu } from '../../interfaces/consulta-cgu';
import { Dg } from '../../interfaces/dg';
import { Family } from '../../interfaces/family';
import { ConsultaCguService } from '../../services/consulta-cgu.service';
import { DgService } from '../../services/dg.service';
import { FamilyService } from '../../services/family.service';

@Component({
  selector: 'mvd-add-family',
  templateUrl: './add-family.component.html',
  styleUrls: ['./add-family.component.scss'],
})
export class AddFamilyComponent implements OnInit {


  form: FormGroup;

  dgs$: Observable<Dg[]>;
  consultaCgus$: Observable<ConsultaCgu[]>;
  filteredDgs$: Observable<Dg[]>;
  dgs: Dg[];
  consultaCgus: ConsultaCgu[];
  filteredConsultaCgus$: Observable<ConsultaCgu[]>;
  family: Family;
  destroy$ = new Subject();
  patient : Patient
  add: boolean = false;
  edit : boolean = false

  constructor(
    private fb: FormBuilder,
    private dgsServices: DgService,
    private consultaCguService: ConsultaCguService,
    private familyService: FamilyService,
    public dialog: MatDialog,
    private patientService:PatientsService
  ) {
    this.createForm();
  }

  ngOnInit(): void {
    this.loadData();
  }

  createForm() {
    this.form = this.fb.group({
      id:[null],
      code: [null, Validators.required] ,
      dg_id: [null, Validators.required],
      consulta_cgu_id: [null, Validators.required],
      studied: [false],
      created_at: [null],
      updated_at: [null],
      patient_id : [this.patient?.id]
    });

  }

  updateForm(family: Family) {
    this.form.patchValue({
      id: family?.id,
      code: family?.code ,
      dg_id: family?.dg_id,
      consulta_cgu_id: family?.consulta_cgu_id,
      studied: family?.studied,
      created_at: family?.created_at,
      updated_at: family?.updated_at,
      patient_id : this.patient?.id
    });
  }

  loadData() {
    this.patientService.patientOnEdit.pipe(takeUntil(this.destroy$)).subscribe(res=>{

      this.patient = res
      this.form.get('patient_id').setValue(this.patient?.id)
    })
    this.dgs$ = this.dgsServices.resultItems$.pipe(map((v) => v?.data.data));
    this.consultaCgus$ = this.consultaCguService.resultItems$.pipe(
      map((v) => v?.data.data)
    );

    this.familyService.familyOnEdit
    .pipe(takeUntil(this.destroy$))
    .subscribe((res) => {
      if(res){
        this.family = res
        this.updateForm(this.family)
      }
    });

    this.dgsServices
      .getDgs(1, 1000)
      .pipe(takeUntil(this.destroy$))
      .subscribe((res) => (this.dgs = res.data.data));
    this.consultaCguService
      .getConsultaCgus(1, 1000)
      .pipe(takeUntil(this.destroy$))
      .subscribe((res) => (this.consultaCgus = res.data.data));


    this.filteredDgs$ = this.form.get('dg_id').valueChanges.pipe(
      startWith(''),
      map((val) => {
        return val ? this._filterDg(val) : this.dgs;
      })
    );

    this.filteredConsultaCgus$ = this.form
      .get('consulta_cgu_id')
      .valueChanges.pipe(
        startWith(''),
        map((val) => {
          return val ? this._filterConsultaCgu(val) : this.consultaCgus;
        })
      );
  }

  private _filterDg(value: string): any[] {
    const filterValue = typeof value === 'string' ? value.toLowerCase() : '';
    if (this.dgs) {
      return this.dgs.filter((item) => {
        return item.name.toLowerCase().includes(filterValue);
      });
    }
  }

  getNameDg(id: number) {
    if (id && this.dgs) {
      const dg = this.dgs.find((item) => item.id === id);
      // console.log(dg);
      return `${dg.name}`;
    }
  }

  private _filterConsultaCgu(value: string): any[] {
    const filterValue = typeof value === 'string' ? value.toLowerCase() : '';
    if (this.consultaCgus) {
      return this.consultaCgus.filter((item) => {
        return (
          item.name.toLowerCase().includes(filterValue) ||
          item.last_name.toLowerCase().includes(filterValue)
        );
      });
    }
  }

  getNameConsultaCgu(id: number) {
    if (id && this.consultaCgus) {
      const consultaCgu = this.consultaCgus.find((item) => item.id === id);
      // console.log(consultaCgu);
      return `${consultaCgu.name} ${consultaCgu.last_name}`;
    }
  }

  submitFamily() {
    console.log(this.form.value);

    if(this.family){
      this.familyService
      .updateFamily(this.form.value)
      .subscribe((res) =>{
            this.edit = false
      });
    }else{
      this.familyService
      .storeFamily(this.form.value)
      .subscribe((res) =>{
            this.edit = false
      });
    }

  }

  addDg(control: AbstractControl) {
    const title_form = 'Agrega DG';
    const fields = [
      {
        nameControl: 'name',
        type: 'text',
        value: '',
        validators: [Validators.required],
        label: 'Nombre',
        class: 'mvd-col1--1',
      },

    ];

    const dataDielog = { title: title_form, data: fields };

    setTimeout(() => {
      const dialogRef = this.dialog.open(ModalReutilComponent, {
        width: '300px',
        data: dataDielog,
      });
      return dialogRef.afterClosed().subscribe((result) => {
        // this.animal = result;
        if (result) {
          this.dgsServices
            .storeDg(result)
            .pipe(take(1))
            .subscribe((res) => {
              // console.log(res);
              this.dgs = [...this.dgs, res];
              // control.setValue(res.name)
              control.setValue(res.id);
            });
        }
      });
    }, 100);
  }

  addConsultaCgu(control: AbstractControl) {
    const title_form = 'Agrega Consulta CGU';
    const fields = [
      {
        nameControl: 'name',
        type: 'text',
        value: '',
        validators: [Validators.required],
        label: 'Nombre',
        class: 'mvd-col1--1',
      },
      {
        nameControl: 'last_name',
        type: 'text',
        value: '',
        validators: [Validators.required],
        label: 'Apellido',
        class: 'mvd-col1--1',
      },
      {
        nameControl: 'contact',
        type: 'text',
        value: '',
        label: 'Contacto',
        class: 'mvd-col1--1',
      },
    ];

    const dataDielog = { title: title_form, data: fields };

    setTimeout(() => {
      const dialogRef = this.dialog.open(ModalReutilComponent, {
        width: '300px',
        data: dataDielog,
      });
      return dialogRef.afterClosed().subscribe((result) => {
        // this.animal = result;
        if (result) {
          this.consultaCguService
            .storeConsultaCgu(result)
            .pipe(take(1))
            .subscribe((res) => {
              // console.log(res);
              this.consultaCgus = [...this.consultaCgus, res];
              // control.setValue(res.name)
              control.setValue(res.id);
            });
        }
      });
    }, 100);
  }


  addFamily(){
    this.add = true
    this.edit = true
  }
  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
