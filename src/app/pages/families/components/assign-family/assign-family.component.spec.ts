import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AssignFamilyComponent } from './assign-family.component';

describe('AssignFamilyComponent', () => {
  let component: AssignFamilyComponent;
  let fixture: ComponentFixture<AssignFamilyComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AssignFamilyComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AssignFamilyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
