import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Observable, Subject } from 'rxjs';
import { debounceTime, map, startWith, take, takeUntil } from 'rxjs/operators';
import { Patient } from 'src/app/pages/patients/interfaces/patient';
import { PatientsService } from 'src/app/pages/patients/services/patients.service';
import { FamilyService } from '../../services/family.service';
import { ModalFormComponent } from '../members/add-member/modal-form/modal-form.component';

@Component({
  selector: 'mvd-assign-family',
  templateUrl: './assign-family.component.html',
  styleUrls: ['./assign-family.component.scss']
})
export class AssignFamilyComponent implements OnInit {

  family_id : FormControl
  destroy$= new Subject();
  families: any[];
  families$: Observable<any[]>;
  patient: Patient;
  constructor(
    private familyService: FamilyService,
    private patientService: PatientsService,
    public dialog: MatDialog,
  ) {
    this.family_id = new FormControl(null)
   }


  ngOnInit(): void {
    this.patientService.patientOnEdit.pipe(take(1)).subscribe(res=> this.patient = res)
    this.family_id
      .valueChanges.pipe(
        takeUntil(this.destroy$),
        debounceTime(500),
        startWith(''),
      )
      .subscribe((val) => {
        console.log(val);

          this.families$ = this.familyService.getFilterFamilies(1,20, val).pipe(take(1),map(v=> {
            this.families = v.data.data
            return v.data.data
          }))
          // return val ? this._filterConsultaCgu(val) : this.consultaCgus;
      });
  }


  getNameFamily(id){
    if (id && this.families) {
      const family = this.families.find((item) => item.id === id);
      // console.log(family);
      return family.code;
    }
  }

  selectFamily(e){
    this.openForm(e.option.value)

  }

  openForm(family_id) {

    const dataDielog = {patient_id: this.patient?.id }

    const dialogRef = this.dialog.open(ModalFormComponent, {
      width: '550px',
      data: dataDielog
    });

    return dialogRef.afterClosed().subscribe(result => {
      // this.animal = result;
      if (result) {

          result.id = family_id;

          this.familyService.addMember(result)


      }

    });
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
