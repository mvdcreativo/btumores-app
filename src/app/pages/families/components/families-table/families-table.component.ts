import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { Router } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import { map, take } from 'rxjs/operators';
import { Patient } from 'src/app/pages/patients/interfaces/patient';
import { Column } from 'src/app/shared/components/data-table/interfaces/table';
import { ModalReutilComponent } from 'src/app/shared/components/modals/modal-reutil/modal-reutil.component';
import { ResponsePaginate } from 'src/app/shared/interfaces/response';
import { Family } from '../../interfaces/family';
import { FamilyService } from '../../services/family.service';

@Component({
  selector: 'mvd-families-table',
  templateUrl: './families-table.component.html',
  styleUrls: ['./families-table.component.scss']
})
export class FamiliesTableComponent implements OnInit {

  @Input() patient: Patient
  // displayedColumns: string[] = ['code', 'title', 'user_owner.name', 'address', 'neighborhood.name'];

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  ////COLUMNAS TABLA
  public columns: Column[] = [
    { title: 'Código', col: 'code' },
    { title: 'Creado', col: 'created_at' }
  ]

  dataSource: Observable<any[]>;
  families: Family[];

  ////paginator
  totalResut: Observable<number>;
  pageDefault = 1;
  perPage: number = 20;
  orden: string = 'desc';
  filter: string = '';
  result: Observable<ResponsePaginate>;
  subscroption: Subscription;
  

  paginatorChange(e: PageEvent) {
   //console.log(e);
    this.getFamilies(e.pageIndex + 1, e.pageSize)

  }
  /////////////


  constructor(
    private familyService: FamilyService,
    public router: Router,
    public dialog: MatDialog,

  ) {

    this.result = this.familyService.resultItems$

  }


  ngOnInit(): void {
    this.getFamilies(this.pageDefault, this.perPage, this.filter, this.orden, this.patient?.id)
    
  }

  getFamilies(currentPage?, perPage?, filter?, sort?, patient_id?) {
    this.subscroption = this.familyService.getFamilies(currentPage, perPage, filter, sort, patient_id).subscribe(next => this.loadData());
  }

  loadData() {
    
    this.dataSource = this.result.pipe(map(v => {
     //console.log("prueba");
      const dataTable = v.data.data.map(x => {
        return {
          id: x.id,
          code: x.code,
          created_at : x.created_at
        }
      })
      return dataTable;
    }))

    this.totalResut = this.result.pipe(map(v => v.data.total))
  }

  search(filter) {
    this.getFamilies(this.pageDefault, this.perPage, filter, this.orden, this.patient?.id )
  }

  deleteItem(id): Observable<any> {
    return this.familyService.deleteFamily(id)
  }

  itemAction(event) {
   //console.log(event);

    if (event.action === "delete") {
      this.deleteItem(event.element.id).pipe(take(1)).subscribe()
    }

    if (event.action === "edit") {
      this.openDialog(event.element)    
    }

  }

  updateFamily(data){
    this.familyService.updateFamily(data).pipe(take(1)).subscribe()
  }

  storeFamily(data){
    this.familyService.storeFamily(data).pipe(take(1)).subscribe()
  }

  openDialog(data?) {
    
    const dialogRef = this.dialog.open(ModalReutilComponent, {
      width: '850px',
      data: this.setFields(data)
    });

    return dialogRef.afterClosed().subscribe(result => {
      // this.animal = result;
      if(result){
        if (result.id) {
          this.updateFamily(result)
          
        }else{
          this.storeFamily(result)
        }

      }
      
    });
  }

  setFields(elementEdit? : Family) {
    
    const fields = [
      { nameControl: 'id', type: 'hidden', value: elementEdit?.id, label: 'Id' },
      { nameControl: 'code', type: 'text', value: elementEdit?.code, label: 'Código', validators: [Validators.required] },
      { nameControl: 'patient_id', type: 'hidden', value: this.patient.id, label: 'Código', validators: [Validators.required] },

    ]

    return fields
  }
  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    this.subscroption.unsubscribe()
  }
}
