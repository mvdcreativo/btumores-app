import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { PatientsService } from 'src/app/pages/patients/services/patients.service';
import { FamilyService } from '../../../services/family.service';
import { ModalFormComponent } from './modal-form/modal-form.component';

@Component({
  selector: 'mvd-add-member',
  templateUrl: './add-member.component.html',
  styleUrls: ['./add-member.component.scss']
})
export class AddMemberComponent implements OnInit {

  constructor(
    private patientService: PatientsService,
    private familyService: FamilyService,
    public dialog: MatDialog,
  ) { }

  ngOnInit(): void {
  }


  openForm(item?) {


    const dataDielog = ""

    const dialogRef = this.dialog.open(ModalFormComponent, {
      width: '550px',
      data: dataDielog
    });

    return dialogRef.afterClosed().subscribe(result => {
      // this.animal = result;
      if (result) {
        if (result.id) {
          this.familyService.updateMember(result)
        } else {


          this.familyService.addMember(result)
        }

      }

    });
  }


}
