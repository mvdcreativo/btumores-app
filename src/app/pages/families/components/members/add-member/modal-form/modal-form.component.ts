import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { Observable, Subject } from 'rxjs';
import { debounceTime, map, startWith, take, takeUntil } from 'rxjs/operators';
import { Kinship } from 'src/app/pages/families/interfaces/family';
import { FamilyService } from 'src/app/pages/families/services/family.service';
import { Patient } from 'src/app/pages/patients/interfaces/patient';
import { PatientsService } from 'src/app/pages/patients/services/patients.service';

@Component({
  templateUrl: './modal-form.component.html',
  styleUrls: ['./modal-form.component.scss']
})
export class ModalFormComponent implements OnInit {
  filteredPatient$: any;
  form : FormGroup
  patients$: Observable<Patient[]>;
  destroy$ = new Subject();
  patients: Patient[];
  kinships$: Observable<Kinship[]>;

  constructor(
    public dialogRef: MatDialogRef<ModalFormComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private fb:FormBuilder,
    private patientService: PatientsService,
    private familyService: FamilyService,
    private router:Router
  ) {

    this.createForm()
  }

  ngOnInit(): void {
    this.kinships$ = this.familyService.getKinships(1,100).pipe(map(v=>v.data.data))

    if(!this.data?.patient_id){
      this.form
        .get('patient_id')
        .valueChanges.pipe(
          takeUntil(this.destroy$),
          debounceTime(500),
          startWith(''),
        )
        .subscribe((val) => {

            this.patients$ = this.patientService.filterPatients(1,20, val).pipe(take(1),map(v=> {
              this.patients = v.data.data
              return v.data.data
            }))
            // return val ? this._filterConsultaCgu(val) : this.consultaCgus;
        });
    }else{
      this.updateForm()
    }


  }
  updateForm() {
    this.form.get('patient_id').setValue(this.data.patient_id)
  }


  createForm(){
    this.form= this.fb.group({
      patient_id : [null, Validators.required],
      kinship_id : [null, Validators.required]
    })
  }

  onNoClick(): void {
    this.dialogRef.close();
  }


  submit(){
    this.dialogRef.close(this.form.value);


  }

  addPatient(){
    this.router.navigate(['/pacientes/paciente'])
    this.dialogRef.close();
  }


  getNamePatient(id: number){

      if (id && this.patients) {
        const patient = this.patients.find((item) => item.id === id);
        // console.log(patient);
        return `(${patient.code}) ${patient.name} ${patient.last_name}`;
      }

  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }


}
