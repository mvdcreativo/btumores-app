import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatTable } from '@angular/material/table';
import moment from 'moment';
import { Observable } from 'rxjs';
import { map, take } from 'rxjs/operators';
import { ConfirmComponent } from 'src/app/shared/components/modals/confirm/confirm.component';
import { Family, Kinship } from '../../interfaces/family';
import { FamilyService } from '../../services/family.service';

export interface KinshipItems {
  patient_id: number;
  patient_code: string;
  patient_name: string;
  kinship_name: string;
  patient_age: number | string;
  patient_dead: boolean;
}

@Component({
  selector: 'mvd-members',
  templateUrl: './members.component.html',
  styleUrls: ['./members.component.scss']
})
export class MembersComponent implements OnInit {

  displayedColumns: string[] = ['patient_code', 'patient_name', 'kinship_name', 'patient_age', 'actions'];
  dataSource : KinshipItems[] ;

  @ViewChild(MatTable) table: MatTable<any>;
  family$: Observable<Family>;
  kinships: Kinship[];
  family_id: number;


  constructor(
    private familyService: FamilyService,
    public dialog: MatDialog,

  ) {


  }

  ngOnInit(): void {

    this.family$ = this.familyService.familyOnEdit.pipe(map(res=> {
      this.family_id = res?.id;
      this.dataSource = res?.patients.map( (v,k ) =>{

        let isDead = res.patients[k].dead;

        let age;
        if(isDead){
          age = this.calculoEdad(res.patients[k].birth , res.patients[k].date_dead)
        }else{
          age = this.calculoEdad(res.patients[k].birth)
        }


        const data : KinshipItems = {
          patient_id: res.patients[k].id,
          patient_code: res.patients[k].code ,
          patient_name: `${res.patients[k].name} ${res.patients[k].last_name}`  ,
          kinship_name: v.pivot?.kinship?.name,
          patient_age:  age,
          patient_dead: res.patients[k].dead
        }
        return data;
      })
      return res
    }))

  }

  calculoEdad(value, date_dead?){
    const date = moment()
    const birth = moment(value);

    let years;
    if (date_dead) {
      const dead = moment(date_dead);
      years = dead.diff(birth,"years");
    }else{
      years = date.diff(birth,"years");
    }

    return years;
  }


  deleteItem(element, message?) {
    this.openDialog(element,'DEL', message)
  }

  openDialog(element, accion, message?): void {
    !message ? message = "" : message = message;
    let name
    if (element?.name) {
      name = element.name
    } else {
      name = element.title
    }

    const dialogRef = this.dialog.open(ConfirmComponent, {
      width: '350px',
      data: { name: name, message: message, value: false }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result?.value) {
        if (accion === "DEL") {

          this.removeMember(element)
        }
      }
    });

  }

  removeMember(element){
    this.familyService.removeMember({id : this.family_id, patient_id : element.patient_id}).pipe(take(1)).subscribe()
  }

}
