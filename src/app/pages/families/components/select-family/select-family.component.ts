import { Component, OnInit, Input } from '@angular/core';

import { Observable, Subject, Subscription } from 'rxjs';

import { PatientsService } from 'src/app/pages/patients/services/patients.service';
import { Patient } from 'src/app/pages/patients/interfaces/patient';
import { FamilyService } from './../../services/family.service';
import { Family } from '../../interfaces/family';
import { map, takeUntil } from 'rxjs/operators';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'mvd-select-family',
  templateUrl: './select-family.component.html',
  styleUrls: ['./select-family.component.scss'],
})
export class SelectFamilyComponent implements OnInit {
  @Input() patient: Patient;

  private subscriptions: Subscription[] = [];
  families$: Observable<Family[]>;
  changeFamily: FormControl;
  destroy$ = new Subject();
  textTrigger: string;
  onEdit: Family;
  add: boolean;

  constructor(
    private patientService: PatientsService,
    private familyService: FamilyService
  ) {}

  ngOnInit(): void {
    this.getFamilies()
    this.changeFamily = new FormControl(null);
    this.familyService.familyOnEdit.pipe(takeUntil(this.destroy$)).subscribe(val=> {
      this.onEdit = val
      if(val){
        this.changeFamily.setValue(val)
        // console.log(val);
      }

    })

    this.families$ = this.familyService.resultItems$.pipe(
      map((res) => {
        const result = res?.data?.data;


          if(!this.onEdit){

            if(result?.length >= 1) {

              this.changeValue(result[0])

            }
          };

          return result;
      })
    )



    // this.familyService.familyOnEdit.subscribe(res=> this.changeFamily.setValue(res))


  }


  ngAfterViewInit(): void {


  }


  changeValue(value){
    // console.log(value);

    this.familyService.setFamilyOnEdit(value)
    this.changeFamily.setValue(value)
    // console.log(this.changeFamily.value);

  }

  getFamilies(){
    this.familyService.getFamilies(1,1000,'','', this.patient.id.toString()).pipe(takeUntil(this.destroy$)).subscribe()
  }


  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
