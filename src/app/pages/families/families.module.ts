import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FamiliesRoutingModule } from './families-routing.module';
import { FamiliesComponent } from './families.component';
import { FamiliesTableComponent } from './components/families-table/families-table.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { FamilyComponent } from './family/family.component';
import { AddFamilyComponent } from './components/add-family/add-family.component';
import { SelectFamilyComponent } from './components/select-family/select-family.component';
import { MembersComponent } from './components/members/members.component';
import { AddMemberComponent } from './components/members/add-member/add-member.component';
import { ModalFormComponent } from './components/members/add-member/modal-form/modal-form.component';
import { AssignFamilyComponent } from './components/assign-family/assign-family.component';


@NgModule({
  declarations: [
    FamiliesComponent,
    FamiliesTableComponent,
    FamilyComponent,
    AddFamilyComponent,
    SelectFamilyComponent,
    MembersComponent,
    AddMemberComponent,
    ModalFormComponent,
    AssignFamilyComponent
  ],
  imports: [
    CommonModule,
    FamiliesRoutingModule,
    SharedModule
  ],
  exports: [
    FamiliesTableComponent,
    FamilyComponent
  ]
})
export class FamiliesModule { }
