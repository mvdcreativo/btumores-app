import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

import { Patient } from '../../patients/interfaces/patient';
import { FamilyService } from '../services/family.service';


@Component({
  selector: 'mvd-family',
  templateUrl: './family.component.html',
  styleUrls: ['./family.component.scss']
})
export class FamilyComponent implements OnInit {

  @Input() patient: Patient;
  families$: Observable<any>;



  constructor(
    private familyService:FamilyService
  ) {

  }

  ngOnInit(): void {
    this.families$ = this.familyService.familyOnEdit

  }



ngOnDestroy(): void {
  this.familyService.setFamilyOnEdit(null)
  this.familyService.setItems(null)

}

}
