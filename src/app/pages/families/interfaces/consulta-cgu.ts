export interface ConsultaCgu {
  id?:number;
  name:string;
  last_name:string;
  contact:string;
}
