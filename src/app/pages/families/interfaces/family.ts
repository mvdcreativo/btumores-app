import { Patient } from "../../patients/interfaces/patient";

export interface Family {
    id?:number;
    code:string;
    dg_id: number;
    studied: boolean;
    consulta_cgu_id: number;
    created_at?: string;
    updated_at?: string;
    kinships?: Kinship[]
    patient_id?: number,
    patients: PatientFamily[]
}

export interface PatientFamily extends Patient{
  pivot: Pivot;
}

export interface Pivot{
  patient_id: number;
  family_id: number;
  kinship_id: number;
  kinship: Kinship
}

export interface Kinship {
    id?:number;
    name:string;
    patients?: Patient[]
}
