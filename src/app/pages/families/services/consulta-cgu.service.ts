import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { BehaviorSubject, Observable } from 'rxjs';
import { map, take } from 'rxjs/operators';
import { ResponsePaginate, Response } from 'src/app/shared/interfaces/response';
import { environment } from 'src/environments/environment';
import { ConsultaCgu } from '../interfaces/consulta-cgu';

@Injectable({
  providedIn: 'root'
})
export class ConsultaCguService {

  private dataOptionsForms$ : BehaviorSubject<any> = new BehaviorSubject<any>(null)
  public get optionsForms$() {
    return this.dataOptionsForms$
  }

  private consultaCguEditSubject$ : BehaviorSubject<ConsultaCgu> = new BehaviorSubject<ConsultaCgu>(null)
  private urlUtils = "src/app/shared/utils/data/";

  private resultSubject$: BehaviorSubject<ResponsePaginate> = new BehaviorSubject(null)
  public get resultItems$() {
    return this.resultSubject$
  }
  public setItems(value) {
    this.resultSubject$.next(value)
  }



  constructor(
    private http:HttpClient,
    private snackBar: MatSnackBar,
  ) {
    // this.loadData()
  }

  get consultaCguOnEdit():Observable<ConsultaCgu>{
    return this.consultaCguEditSubject$
  }

  setConsultaCguOnEdit(value){
    this.consultaCguEditSubject$.next(value)
  }

  storeConsultaCgu(data): Observable<ConsultaCgu>{
    return this.http.post<Response>(`${environment.API}consulta_cgus`, data).pipe(
      map( v => {
        this.setConsultaCguOnEdit(v.data)
        //snacbarr
        this.openSnackBar('Se creó correctamente','success-snack-bar')
        //////////
        return v.data
      })

    )

  }

  updateConsultaCgu(data): Observable<ConsultaCgu>{
    const id = this.consultaCguEditSubject$.value.id
    data._method = "put";
    return this.http.post<Response>(`${environment.API}consulta_cgus/${id}`, data).pipe(
      map( v => {
        this.setConsultaCguOnEdit(v.data)
        //snacbarr
        this.openSnackBar('Actualizado correctamente','success-snack-bar')
        //////////
        return v.data
      })

    )

  }

  deleteConsultaCgu(id){
    return this.http.delete<Response>(`${environment.API}consulta_cgus/${id}`).pipe(
      map( v => {
        // console.log(v.data);

        this.getConsultaCgus(1, 20, '', 'desc').pipe(take(1)).subscribe()
        //snacbarr
        this.openSnackBar('Eliminado correctamente','success-snack-bar')
        //////////
        return v.data

      })

    )
  }


  getConsultaCgu(id) : Observable<ConsultaCgu>{
    return this.http.get<Response>(`${environment.API}consulta_cgus/${id}`).pipe(map(
      res => {
        this.setConsultaCguOnEdit(res.data)
        const resp = res.data
        return resp;
      }
    )

    )
  }


///listar
  getConsultaCgus(currentPage = 1, perPage = 20, filter='', sort= 'desc' ) : Observable<ResponsePaginate>{
    return this.http.get<ResponsePaginate>(`${environment.API}consulta_cgus`, {
      params: new HttpParams()
        .set('page', currentPage.toString())
        .set('filter', filter)
        .set('sort', sort)
        .set('per_page', perPage.toString())

    }).pipe(map(
      res => {

        this.setItems(res)
        this.setConsultaCguOnEdit(null)
        const resp = res
        return resp;
      }
    )

    )
  }



  openSnackBar(message: string, refClass:string, action: string = '') {
    this.snackBar.open(message, action, {
      duration: 2000,
      horizontalPosition: 'center',
      verticalPosition: 'top',
      panelClass: refClass
    });
  }
}
