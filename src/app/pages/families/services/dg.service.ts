import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { BehaviorSubject, Observable } from 'rxjs';
import { map, take } from 'rxjs/operators';
import { ResponsePaginate, Response } from 'src/app/shared/interfaces/response';
import { environment } from 'src/environments/environment';
import { Dg } from '../interfaces/dg';

@Injectable({
  providedIn: 'root'
})
export class DgService {

  private dataOptionsForms$ : BehaviorSubject<any> = new BehaviorSubject<any>(null)
  public get optionsForms$() {
    return this.dataOptionsForms$
  }

  private dgEditSubject$ : BehaviorSubject<Dg> = new BehaviorSubject<Dg>(null)
  private urlUtils = "src/app/shared/utils/data/";

  private resultSubject$: BehaviorSubject<ResponsePaginate> = new BehaviorSubject(null)
  public get resultItems$() {
    return this.resultSubject$
  }
  public setItems(value) {
    this.resultSubject$.next(value)
  }



  constructor(
    private http:HttpClient,
    private snackBar: MatSnackBar,
  ) {
    // this.loadData()
  }

  get dgOnEdit():Observable<Dg>{
    return this.dgEditSubject$
  }

  setDgOnEdit(value){
    this.dgEditSubject$.next(value)
  }

  storeDg(data): Observable<Dg>{
    return this.http.post<Response>(`${environment.API}dgs`, data).pipe(
      map( v => {
        this.setDgOnEdit(v.data)
        //snacbarr
        this.openSnackBar('Se creó correctamente','success-snack-bar')
        //////////
        return v.data
      })

    )

  }

  updateDg(data): Observable<Dg>{
    const id = this.dgEditSubject$.value.id
    data._method = "put";
    return this.http.post<Response>(`${environment.API}dgs/${id}`, data).pipe(
      map( v => {
        this.setDgOnEdit(v.data)
        //snacbarr
        this.openSnackBar('Actualizado correctamente','success-snack-bar')
        //////////
        return v.data
      })

    )

  }

  deleteDg(id){
    return this.http.delete<Response>(`${environment.API}dgs/${id}`).pipe(
      map( v => {
        // console.log(v.data);

        this.getDgs(1, 20, '', 'desc').pipe(take(1)).subscribe()
        //snacbarr
        this.openSnackBar('Eliminado correctamente','success-snack-bar')
        //////////
        return v.data

      })

    )
  }


  getDg(id) : Observable<Dg>{
    return this.http.get<Response>(`${environment.API}dgs/${id}`).pipe(map(
      res => {
        this.setDgOnEdit(res.data)
        const resp = res.data
        return resp;
      }
    )

    )
  }


///listar
  getDgs(currentPage = 1, perPage = 20, filter='', sort= 'desc' ) : Observable<ResponsePaginate>{
    return this.http.get<ResponsePaginate>(`${environment.API}dgs`, {
      params: new HttpParams()
        .set('page', currentPage.toString())
        .set('filter', filter)
        .set('sort', sort)
        .set('per_page', perPage.toString())

    }).pipe(map(
      res => {

        this.setItems(res)
        this.setDgOnEdit(null)
        const resp = res
        return resp;
      }
    )

    )
  }



  openSnackBar(message: string, refClass:string, action: string = '') {
    this.snackBar.open(message, action, {
      duration: 2000,
      horizontalPosition: 'center',
      verticalPosition: 'top',
      panelClass: refClass
    });
  }
}
