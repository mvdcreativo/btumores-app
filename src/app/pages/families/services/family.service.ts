import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { concatMap, map, take } from 'rxjs/operators';
import { ResponsePaginate, Response } from 'src/app/shared/interfaces/response';
import { UbicationService } from 'src/app/shared/services/ubications/ubication.service';
import { environment } from 'src/environments/environment';
import { Family } from '../interfaces/family';

@Injectable({
  providedIn: 'root'
})
export class FamilyService {

  private dataOptionsForms$ : BehaviorSubject<any> = new BehaviorSubject<any>(null)
  public get optionsForms$() {
    return this.dataOptionsForms$
  }

  private familyEditSubject$ : BehaviorSubject<Family> = new BehaviorSubject<Family>(null)
  private urlUtils = "src/app/shared/utils/data/";

  private resultSubject$: BehaviorSubject<ResponsePaginate> = new BehaviorSubject(null)
  public get resultItems$() {
    return this.resultSubject$
  }
  public setItems(value : ResponsePaginate):void {
    this.resultSubject$.next(value)
  }
  public updateItemsResults(data:Family){
    let resultsPaginate = this.resultSubject$.value
    let results : Family[] = this.resultSubject$.value.data.data
    let items = results.map((v,k) => {
      if(v.id === data.id){
        return data
      }
      return v

    })
    // items = [...items, data];
    resultsPaginate.data.data =  items;
    this.setItems(resultsPaginate)

  }


  constructor(
    private http:HttpClient,
    private snackBar: MatSnackBar,
  ) {
    // this.loadData()
  }

  get familyOnEdit():Observable<Family>{
    return this.familyEditSubject$
  }

  setFamilyOnEdit(value : Family): void{
    if (value === null) {
      //console.log('set null');

    }else{
      //console.log(value);

    }
    this.familyEditSubject$.next(value)
  }

  storeFamily(data): Observable<Family>{
    return this.http.post<Response>(`${environment.API}families`, data).pipe(
      map( v => {
        this.updateItemsResults(v.data)
        this.setFamilyOnEdit(v.data)

        //snacbarr
        this.openSnackBar('Se creó correctamente','success-snack-bar')
        //////////
        return v.data
      })

    )

  }

  updateFamily(data): Observable<Family>{
    //console.log(data);

    const id = data?.id
    data._method = "put";
    return this.http.post<Response>(`${environment.API}families/${id}`, data).pipe(
      map( v => {
        this.updateItemsResults(v.data)
        this.setFamilyOnEdit(v.data)
        //snacbarr
        this.openSnackBar('Actualizado correctamente','success-snack-bar')
        //////////
        return v.data
      })

    )

  }

  removeMember(data): Observable<Family>{
    //console.log(data);

    const id = data?.id
    return this.http.post<Response>(`${environment.API}family_remove_member/${id}`, data).pipe(
      map( v => {
        this.updateItemsResults(v.data)
        this.setFamilyOnEdit(v.data)
        //snacbarr
        this.openSnackBar('Actualizado correctamente','success-snack-bar')
        //////////
        return v.data
      })

    )

  }


  deleteFamily(id){
    return this.http.delete<Response>(`${environment.API}families/${id}`).pipe(
      map( v => {
        // console.log(v.data);

        this.getFamilies(1, 20, '', 'desc').pipe(take(1)).subscribe()
        //snacbarr
        this.openSnackBar('Eliminado correctamente','success-snack-bar')
        //////////
        return v.data

      })

    )
  }


  getFamily(id) : Observable<Family>{
    return this.http.get<Response>(`${environment.API}families/${id}`).pipe(map(
      res => {
        this.setFamilyOnEdit(res.data)
        const resp = res.data
        return resp;
      }
    )

    )
  }


///listar
  getFamilies(currentPage = 1, perPage = 20, filter='', sort= 'desc', patient_id= '' ) : Observable<ResponsePaginate>{
    return this.http.get<ResponsePaginate>(`${environment.API}families`, {
      params: new HttpParams()
        .set('page', currentPage.toString())
        .set('filter', filter)
        .set('sort', sort)
        .set( 'patient_id', patient_id)
        .set('per_page', perPage.toString())

    }).pipe(map(
      res => {

        this.setFamilyOnEdit(null)
        this.setItems(res)
        const resp = res
        return resp;
      }
    )

    )
  }

  getFilterFamilies(currentPage = 1, perPage = 20, filter='', sort= 'desc', patient_id= '' ) : Observable<ResponsePaginate>{
    return this.http.get<ResponsePaginate>(`${environment.API}families`, {
      params: new HttpParams()
        .set('page', currentPage.toString())
        .set('filter', filter)
        .set('sort', sort)
        .set( 'patient_id', patient_id)
        .set('per_page', perPage.toString())

    }).pipe(map(
      res => {
        const resp = res
        return resp;
      }
    )

    )
  }

  addMember(data){

    let dataSend = {...data};

    if (data?.id) {
      dataSend = data
    }else{
      dataSend.id =  this.familyEditSubject$.value.id
    }

    this.updateFamily(dataSend).subscribe()
  }


  updateMember(data){

  }


  getKinships(currentPage = 1, perPage = 20, filter='', sort= 'asc') : Observable<ResponsePaginate>{
    return this.http.get<ResponsePaginate>(`${environment.API}kinships`, {
      params: new HttpParams()
        .set('page', currentPage.toString())
        .set('filter', filter)
        .set('sort', sort)
        .set('per_page', perPage.toString())

    }).pipe(map(
      res => {


        // this.setFamilyOnEdit(null)
        const resp = res
        return resp;
      }
    )

    )
  }

  openSnackBar(message: string, refClass:string, action: string = '') {
    this.snackBar.open(message, action, {
      duration: 2000,
      horizontalPosition: 'center',
      verticalPosition: 'top',
      panelClass: refClass
    });
  }
}
