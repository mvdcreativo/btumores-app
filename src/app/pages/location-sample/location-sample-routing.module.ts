import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LocationSampleComponent } from './location-sample.component';

const routes: Routes = [{ path: '', component: LocationSampleComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LocationSampleRoutingModule { }
