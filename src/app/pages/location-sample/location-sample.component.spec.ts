import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LocationSampleComponent } from './location-sample.component';

describe('LocationSampleComponent', () => {
  let component: LocationSampleComponent;
  let fixture: ComponentFixture<LocationSampleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LocationSampleComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LocationSampleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
