import { Component, OnInit, ViewChild } from '@angular/core';
import { Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { Observable, Subscription } from 'rxjs';
import { map, take } from 'rxjs/operators';
import { Column } from 'src/app/shared/components/data-table/interfaces/table';
import { ModalReutilComponent } from 'src/app/shared/components/modals/modal-reutil/modal-reutil.component';
import { ResponsePaginate } from 'src/app/shared/interfaces/response';
import { Location } from '../samples/interfaces/storage-sample';
import { TypeLocationSamplesService } from '../type-location-sample/services/type-location-sample.service';
import { LocationSamplesService } from './services/location-sample.service';
import { OptionSelect } from 'src/app/shared/components/dinamic-form/interfaces/fields';


@Component({
  selector: 'mvd-location-sample',
  templateUrl: './location-sample.component.html',
  styleUrls: ['./location-sample.component.scss']
})
export class LocationSampleComponent implements OnInit {


  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  ////COLUMNAS TABLA
  public columns: Column[] = [
    { title: 'Código', col: 'code' },
    { title: 'Tipo', col: 'type_location' },
    { title: 'Cant.Racks', col: 'racks' },
    { title: 'Cant.Cajas x Rack', col: 'boxes' },
    { title: 'Cant.Posiciones x Caja', col: 'positions' },
    { title: 'Descripción', col: 'description' },
    { title: 'Marca/Modelo', col: 'brand_model' },
    { title: 'Capacidad', col: 'capacity' },

  ]

  totalResut: Observable<number>;
  pageDefault = 1;
  perPage: number = 10;
  orden: string = 'desc';
  filter: string = '';
  result: Observable<ResponsePaginate>;
  subscroption: Subscription;
  dataSource: Observable<any[]>;
  typeLocationsOptions: OptionSelect[];


  constructor(
    private locationSampleService: LocationSamplesService,
    public dialog: MatDialog,
    private typeLocationService: TypeLocationSamplesService

  ) {
    this.result = this.locationSampleService.resultItems$

  }

  ngOnInit(): void {

    this.getLocationSamples(this.pageDefault, this.perPage, this.filter, this.orden)
  }

  getTypeLocations(){
    this.typeLocationService.getTypeLocationSamples(1,1000).pipe(take(1)).subscribe(res=>{
      this.typeLocationsOptions = res.data.data.map(v=>{
        return {name: v.name, value: v.id }
      })
    })
  }
  paginatorChange(e: PageEvent) {
    //console.log(e);
    this.getLocationSamples(e.pageIndex + 1, e.pageSize)

  }

  getLocationSamples(currentPage?, perPage?, filter?, sort?) {
    this.subscroption = this.locationSampleService.getLocationSamples(currentPage, perPage, filter, sort).subscribe(next => this.loadData());
  }


  loadData() {

    this.dataSource = this.result.pipe(map(v => {
      const dataTable = v.data.data.map(x => {
        return {
          id: x.id,
          type_location_id: x.type_location_id,
          code: x.code,
          racks: x.racks,
          boxes: x.boxes,
          positions: x.positions,
          description: x.description,
          brand_model: x.brand_model,
          capacity: x.capacity,
          type_location: x.type_location?.name,

        }
      })
      return dataTable;
    }))

    this.totalResut = this.result.pipe(map(v => v.data.total))
    this.getTypeLocations()
  }

  search(filter) {
    this.getLocationSamples(this.pageDefault, this.perPage, filter, this.orden)
  }


  deleteItem(id): Observable<any> {
    return this.locationSampleService.deleteLocationSample(id)
  }

  itemAction(event) {
    //console.log(event);

    if (event.action === "delete") {
      this.deleteItem(event.element.id).pipe(take(1)).subscribe()
    }
    if (event.action === "edit") {
      this.openDialog('Edita Almacén de Muestras',event.element)
    }
  }

  openDialog(title_form = 'Agrega Almacén de Muestras', data?) {


    const dataDielog = { title: title_form, data: this.setFields(data) };

    const dialogRef = this.dialog.open(ModalReutilComponent, {
      width: '550px',
      data: dataDielog
    });

    return dialogRef.afterClosed().subscribe(result => {
      // this.animal = result;
      if (result) {
        if (result.id) {
          //console.log(result);

          this.updateLocationSample(result)

        } else {
          this.storeLocationSample(result)
        }

      }

    });
  }

  updateLocationSample(data) {
    this.locationSampleService.updateLocationSample(data).pipe(take(1)).subscribe()
  }

  storeLocationSample(data) {
    this.locationSampleService.storeLocationSample(data).pipe(take(1)).subscribe(
      res => {
        //console.log(res);
      }
    )
  }

  setFields(elementEdit? : Location) {
    // console.log(elementEdit);
    const fields = [
      { nameControl: 'id', type: 'hidden', value: elementEdit?.id, label: 'Id' },
      { nameControl: 'code', type: 'text', value: elementEdit?.code, validators: [Validators.required], label: 'Código', class:'mvd-col1--1' },
      { nameControl: 'type_location_id', type: 'select', options:this.typeLocationsOptions, value: elementEdit?.type_location_id, validators: [Validators.required], label: 'Tipo de Almacén', class:'mvd-col1--1' },
      { nameControl: 'racks', type: 'text', value: elementEdit?.racks, validators: [Validators.required], label: 'Cantidad de Racks', class:'mvd-col1--1' },
      { nameControl: 'boxes', type: 'text', value: elementEdit?.boxes, validators: [Validators.required], label: 'Cantidad de Cajas x Rack', class:'mvd-col1--1' },
      { nameControl: 'positions', type: 'text', value: elementEdit?.positions, validators: [Validators.required], label: 'Cantidad de Posiciones x Caja', class:'mvd-col1--1' },
      { nameControl: 'description', type: 'text', value: elementEdit?.description, label: 'Descripción', class:'mvd-col1--1' },
      { nameControl: 'brand_model', type: 'text', value: elementEdit?.brand_model, label: 'Marca/Modelo', class:'mvd-col1--1' },
      { nameControl: 'capacity', type: 'text', value: elementEdit?.capacity, label: 'Capacidad', class:'mvd-col1--1' },
    ]

    return fields
  }

  ngOnDestroy(): void {
    this.subscroption.unsubscribe()
  }
}
