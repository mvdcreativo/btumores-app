import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LocationSampleRoutingModule } from './location-sample-routing.module';
import { LocationSampleComponent } from './location-sample.component';
import { SharedModule } from 'src/app/shared/shared.module';


@NgModule({
  declarations: [
    LocationSampleComponent
  ],
  imports: [
    CommonModule,
    LocationSampleRoutingModule,
    SharedModule
  ]
})
export class LocationSampleModule { }
