import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { concatMap, map, take } from 'rxjs/operators';
import { ResponsePaginate, Response } from 'src/app/shared/interfaces/response';
import { UbicationService } from 'src/app/shared/services/ubications/ubication.service';
import { environment } from 'src/environments/environment';
import { Location } from '../../samples/interfaces/storage-sample';


@Injectable({
  providedIn: 'root'
})
export class LocationSamplesService {


  private dataOptionsForms$ : BehaviorSubject<any> = new BehaviorSubject<any>(null)
  public get optionsForms$() {
    return this.dataOptionsForms$
  }

  private locationSampleEditSubject$ : BehaviorSubject<Location> = new BehaviorSubject<Location>(null)
  private urlUtils = "src/app/shared/utils/data/";

  private resultSubject$: BehaviorSubject<ResponsePaginate> = new BehaviorSubject(null)
  public get resultItems$() {
    return this.resultSubject$
  }
  public setItems(value) {
    this.resultSubject$.next(value)
  }



  constructor(
    private http:HttpClient,
    private snackBar: MatSnackBar,
    private ubicationService: UbicationService,
  ) {
    // this.loadData()
  }

  get locationSampleOnEdit():Observable<Location>{
    return this.locationSampleEditSubject$
  }

  setLocationSampleOnEdit(value){
    this.locationSampleEditSubject$.next(value)
  }

  storeLocationSample(data): Observable<Location>{
    return this.http.post<Response>(`${environment.API}locations_samples`, data).pipe(
      map( v => {
        this.getLocationSamples(1, 10, '', 'desc').pipe(take(1)).subscribe()
        //snacbarr
        this.openSnackBar('Se creó correctamente','success-snack-bar')
        //////////
        return v.data
      })

    )

  }

  updateLocationSample(data): Observable<Location>{
    const id = data.id
    data._method = "put";
    return this.http.post<Response>(`${environment.API}locations_samples/${id}`, data).pipe(
      map( v => {
        this.getLocationSamples(1, 10, '', 'desc').pipe(take(1)).subscribe()
        //snacbarr
        this.openSnackBar('Actualizado correctamente','success-snack-bar')
        //////////
        return v.data
      })

    )

  }

  deleteLocationSample(id){
    return this.http.delete<Response>(`${environment.API}locations_samples/${id}`).pipe(
      map( v => {

        this.getLocationSamples(1, 10, '', 'desc').pipe(take(1)).subscribe()
        //snacbarr
        this.openSnackBar('Eliminado correctamente','success-snack-bar')
        //////////
        return v.data

      })

    )
  }


  getLocationSample(id) : Observable<Location>{
    return this.http.get<Response>(`${environment.API}locations_samples/${id}`).pipe(map(
      res => {
        const resp = res.data
        return resp;
      }
    )

    )
  }


///listar
  getLocationSamples(currentPage = 1, perPage = 20, filter='', sort= 'desc') : Observable<ResponsePaginate>{
    return this.http.get<ResponsePaginate>(`${environment.API}locations_samples`, {
      params: new HttpParams()
        .set('page', currentPage.toString())
        .set('filter', filter)
        .set('sort', sort)
        .set('per_page', perPage.toString())

    }).pipe(map(
      res => {

        this.setLocationSampleOnEdit(null)
        this.setItems(res)
        const resp = res
        return resp;
      }
    )

    )
  }



  openSnackBar(message: string, refClass:string, action: string = '') {
    this.snackBar.open(message, action, {
      duration: 2000,
      horizontalPosition: 'center',
      verticalPosition: 'top',
      panelClass: refClass
    });
  }
}
