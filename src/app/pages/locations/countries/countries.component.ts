import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { Observable, Subscription } from 'rxjs';
import { map, take } from 'rxjs/operators';
import { Column } from 'src/app/shared/components/data-table/interfaces/table';
import { ResponsePaginate } from 'src/app/shared/interfaces/response';
import { Country } from 'src/app/shared/interfaces/ubication';
import { LocationService } from '../services/location.service';

@Component({
  selector: 'mvd-countries',
  templateUrl: './countries.component.html',
  styleUrls: ['./countries.component.scss']
})
export class CountriesComponent implements OnInit {


  // displayedColumns: string[] = ['code', 'title', 'user_owner.name', 'address', 'neighborhood.name'];

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  ////COLUMNAS TABLA
  public columns: Column[] = [
    { title: 'id', col: 'id' },
    { title: 'Nombre', col: 'name' },
    { title: 'Código', col: 'code' },
  ]

  dataSource: Observable<any>;
  result: Observable<ResponsePaginate>;
  totalResut: Observable<number>;
  countries: Country[];

  ////paginator
  pageDefault = 1;
  perPage: number = 10;
  orden: string = 'desc';
  filter: string = '';
  subcription: Subscription;

  paginatorChange(e: PageEvent) {
   //console.log(e);
    this.getCities(e.pageIndex + 1, e.pageSize)

  }
  /////////////


  constructor(
    private locationService: LocationService
  ) {
    this.result = this.locationService.resultItems$

  }


  ngOnInit(): void {

    this.getCities(this.pageDefault, this.perPage, this.filter, this.orden)

  }


  getCities(currentPage?, perPage?, filter?, sort?) {
    const crudClient = "countries"
    this.subcription = this.locationService.getItems(currentPage, perPage, filter, sort, crudClient).subscribe(next => this.loadData())
  }

  loadData() {
    this.dataSource = this.result.pipe(map(v => {
      const dataTable = v.data.data.map(x => {
        return {
          id: x.id,
          code: x.code,
          name: x.name,
        }
      })
      return dataTable;
    }))

    this.totalResut = this.result.pipe(map(v => v.data.total))
  }

  search(filter) {
    this.getCities(this.pageDefault, this.perPage, filter, this.orden)
  }

  openDialog(elementEdit?){
    this.locationService.openDialog('countries',elementEdit)
  }
  
  itemAction(event){
    if(event.action === "delete"){
      this.deleteItem(event.element.id).pipe(take(1)).subscribe()
    }
  
    if(event.action === "edit"){
      this.openDialog(event.element)
    }
  }
  
  deleteItem(id):Observable<any>{
    const crudClient = "countries"
    return this.locationService.deleteLocation(id,crudClient)
  }

  ngOnDestroy(): void {
    this.subcription.unsubscribe()
  }

}
