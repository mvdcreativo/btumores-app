import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MedicalInstitutionsComponent } from './medical-institutions.component';

const routes: Routes = [{ path: '', component: MedicalInstitutionsComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MedicalInstitutionsRoutingModule { }
