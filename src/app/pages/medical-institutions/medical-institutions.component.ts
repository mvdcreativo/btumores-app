import { Component, OnInit, ViewChild } from '@angular/core';
import { Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { Observable, Subscription } from 'rxjs';
import { map, take } from 'rxjs/operators';
import { Column } from 'src/app/shared/components/data-table/interfaces/table';
import { ModalReutilComponent } from 'src/app/shared/components/modals/modal-reutil/modal-reutil.component';
import { ResponsePaginate } from 'src/app/shared/interfaces/response';
import { MedicalInstitution } from './interfaces/medicalInstitution';
import { MedicalInstitutionsService } from './services/medical-institutions.service';

@Component({
  selector: 'mvd-medical-institutions',
  templateUrl: './medical-institutions.component.html',
  styleUrls: ['./medical-institutions.component.scss']
})
export class MedicalInstitutionsComponent implements OnInit {



  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  ////COLUMNAS TABLA
  public columns: Column[] = [
    { title: 'Nombre', col: 'name' },

  ]

  totalResut: Observable<number>;
  pageDefault = 1;
  perPage: number = 10;
  orden: string = 'desc';
  filter: string = '';
  result: Observable<ResponsePaginate>;
  subscroption: Subscription;
  dataSource: Observable<any[]>;


  constructor(
    private medicalInstitutionService: MedicalInstitutionsService,
    public dialog: MatDialog,

  ) {
    this.result = this.medicalInstitutionService.resultItems$

  }

  ngOnInit(): void {

    this.getMedicalInstitutions(this.pageDefault, this.perPage, this.filter, this.orden)

  }

  paginatorChange(e: PageEvent) {
    //console.log(e);
    this.getMedicalInstitutions(e.pageIndex + 1, e.pageSize)

  }

  getMedicalInstitutions(currentPage?, perPage?, filter?, sort?) {
    this.subscroption = this.medicalInstitutionService.getMedicalInstitutions(currentPage, perPage, filter, sort).subscribe(next => this.loadData());
  }


  loadData() {

    this.dataSource = this.result.pipe(map(v => {
      const dataTable = v.data.data.map(x => {
        return {
          id: x.id,
          name: x.name,

        }
      })
      return dataTable;
    }))

    this.totalResut = this.result.pipe(map(v => v.data.total))
  }

  search(filter) {
    this.getMedicalInstitutions(this.pageDefault, this.perPage, filter, this.orden)
  }


  deleteItem(id): Observable<any> {
    return this.medicalInstitutionService.deleteMedicalInstitution(id)
  }

  itemAction(event) {
    //console.log(event);

    if (event.action === "delete") {
      this.deleteItem(event.element.id).pipe(take(1)).subscribe()
    }
    if (event.action === "edit") {
      this.openDialog('Edita Institución Médica',event.element)
    }
  }

  openDialog(title_form = 'Agrega Institución Médica', data?) {


    const dataDielog = { title: title_form, data: this.setFields(data) };

    const dialogRef = this.dialog.open(ModalReutilComponent, {
      width: '550px',
      data: dataDielog
    });

    return dialogRef.afterClosed().subscribe(result => {
      // this.animal = result;
      if (result) {
        if (result.id) {
          //console.log(result);

          this.updateMedicalInstitution(result)

        } else {
          this.storeMedicalInstitution(result)
        }

      }

    });
  }

  updateMedicalInstitution(data) {
    this.medicalInstitutionService.updateMedicalInstitution(data).pipe(take(1)).subscribe()
  }

  storeMedicalInstitution(data) {
    this.medicalInstitutionService.storeMedicalInstitution(data).pipe(take(1)).subscribe(
      res => {
        //console.log(res);
      }
    )
  }

  setFields(elementEdit? : MedicalInstitution) {
    // console.log(elementEdit);

    const fields = [
      { nameControl: 'id', type: 'hidden', value: elementEdit?.id, label: 'Id' },
      { nameControl: 'name', type: 'text', value: elementEdit?.name, validators: [Validators.required], label: 'Nombre', class:'mvd-col1--1' },
    ]

    return fields
  }

  ngOnDestroy(): void {
    this.subscroption.unsubscribe()
  }

}
