import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MedicalInstitutionsRoutingModule } from './medical-institutions-routing.module';
import { MedicalInstitutionsComponent } from './medical-institutions.component';
import { SharedModule } from 'src/app/shared/shared.module';


@NgModule({
  declarations: [MedicalInstitutionsComponent],
  imports: [
    CommonModule,
    MedicalInstitutionsRoutingModule,
    SharedModule
  ]
})
export class MedicalInstitutionsModule { }
