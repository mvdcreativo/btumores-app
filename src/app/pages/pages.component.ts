import { Component } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { forkJoin, Observable, Subscription } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';
import { UbicationService } from '../shared/services/ubications/ubication.service';
import { PatientsService } from './patients/services/patients.service';
import { TopographiesService } from './topographies/services/topography.service';
import { Topography } from './topographies/interfaces/topography';
import { TumorLineageService } from './tumor-lineage/services/tumor-lineage.service';
import { TumorLineage } from './tumor-lineage/interfaces/tumor-lineage';
import { TypeSamplesService } from './types-samples/services/types-samples.service';
import { TypeSample } from './types-samples/interfaces/type-sample';

@Component({
  selector: 'mvd-pages',
  templateUrl: './pages.component.html',
  styleUrls: ['./pages.component.scss']
})
export class PagesComponent {

  isHandset$: Observable<boolean> = this.breakpointObserver.observe([Breakpoints.Tablet, Breakpoints.Handset, Breakpoints.Medium, Breakpoints.Small])
    .pipe(
      map(result => result.matches),
      shareReplay()
    );
  subscriptions: Subscription;
  topographies: Observable<Topography[]>;
  tumorLineages: Observable<TumorLineage[]>;
  typeSamples: Observable<TypeSample[]>;

  constructor(
    private breakpointObserver: BreakpointObserver,
    private ubicationService: UbicationService,
    private patientService: PatientsService,
    private topographyService: TopographiesService,
    private tumorLineageService: TumorLineageService,
    private typeSampleService: TypeSamplesService
  ) {
    this.subscriptions = forkJoin(
      this.ubicationService.citiesItems$,
      this.ubicationService.statesItems$,
      this.ubicationService.countriesItems$,
      this.patientService.doctorsItems$,
      this.patientService.medicalInstitutionsItems$,
      this.topographies = this.topographyService.listTopographiesItems$,
      this.tumorLineages = this.tumorLineageService.listTumorLineageItems$,
      this.typeSamples = this.typeSampleService.listTypeSampleItems$


    ).subscribe()
  }


  ngOnDestroy(): void {
    this.subscriptions.unsubscribe()
  }

}
