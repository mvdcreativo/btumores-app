import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { Observable, Subscription } from 'rxjs';
import { map, take } from 'rxjs/operators';
import { Column } from 'src/app/shared/components/data-table/interfaces/table';
import { ModalPreviewComponent } from 'src/app/shared/components/modals/modal-preview/modal-preview.component';
import { ModalReutilComponent } from 'src/app/shared/components/modals/modal-reutil/modal-reutil.component';
import { ResponsePaginate } from 'src/app/shared/interfaces/response';
import { Patient } from '../../interfaces/patient';
import { Attached } from './attached';
import { AttachedFilesService } from './attached-files.services';
import * as fileSaver from 'file-saver';


@Component({
  selector: 'mvd-attached-files',
  templateUrl: './attached-files.component.html',
  styleUrls: ['./attached-files.component.scss'],
})
export class AttachedFilesComponent implements OnInit {
  @Input() patient: Patient;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  ////COLUMNAS TABLA
  public columns: Column[] = [
    { title: 'Nombre', col: 'name' },
    { title: 'Consentimiento', col: 'consentimiento', type: 'consentimiento' },
    { title: 'Fecha de carga', col: 'created_at', pipe: 'dd/MM/yyyy H:mm:ss' },
  ];

  totalResut: Observable<number>;
  pageDefault = 1;
  perPage: number = 10;
  orden: string = 'desc';
  filter: string = '';
  result: Observable<ResponsePaginate>;
  subscroption: Subscription;
  dataSource: Observable<any[]>;
  docUrl:string;
  consentimiento: boolean = false;

  constructor(
    private attachedService: AttachedFilesService,
    public dialog: MatDialog
  ) {
    this.result = this.attachedService.resultItems$;
  }

  ngOnInit(): void {
    // console.log(this.patient);

    this.getAttacheds(
      this.patient.id,
      this.pageDefault,
      this.perPage,
      this.filter,
      this.orden
    );
  }

  ngOnChanges(): void {
    if (this.patient?.documents?.length >= 1) {
      const documentsConsentimiento = this.patient.documents.filter(
        (v) => v.consentimiento
      );
      this.consentimiento =
        documentsConsentimiento.length >= 1 ? true : false;
    }else{
      this.consentimiento = false
    }
  }
  paginatorChange(e: PageEvent) {
    //console.log(e);
    this.getAttacheds(this.patient.id, e.pageIndex + 1, e.pageSize);
  }

  getAttacheds(patient_id, currentPage?, perPage?, filter?, sort?) {
    this.subscroption = this.attachedService
      .getAttacheds(patient_id, currentPage, perPage, filter, sort)
      .subscribe((next) => this.loadData());
  }

  loadData() {
    this.dataSource = this.result.pipe(
      map((v) => {
        const dataTable = v.data.data.map((x) => {
          // console.log(x.consentimiento);
          if (this.patient?.documents?.length >= 1) {
            const documentsConsentimiento = this.patient.documents.filter(
              (v) => v.consentimiento
            );
            this.consentimiento =
              documentsConsentimiento.length >= 1 ? true : false;
          }else{
            this.consentimiento = false
          }

          return {
            id: x.id,
            name: x.name,
            consentimiento: x.consentimiento,
            url_file: x.url_file,
            created_at: x.created_at,
          };
        });
        return dataTable;
      })
    );

    //concentimiento firmado si,no


    this.totalResut = this.result.pipe(map((v) => v.data.total));
  }

  search(filter) {
    this.getAttacheds(
      this.patient.id,
      this.pageDefault,
      this.perPage,
      filter,
      this.orden
    );
  }

  deleteItem(id): Observable<any> {
    return this.attachedService.deleteAttached(id);
  }

  itemAction(event) {
    //console.log(event);

    if (event.action === 'delete') {
      this.deleteItem(event.element.id).pipe(take(1)).subscribe();
    }
    if (event.action === 'edit') {
      this.openDialog('Edita Documento', event.element);
    }
    if (event.action === 'preview') {
      this.openDialogPreview(event.element.url_file);
    }
    if (event.action === 'download') {
      this.downloadFile(event.element);
    }
  }

  openDialog(title_form = 'Agrega Documento', data?) {
    // console.log(data);

    const dataDielog = { title: title_form, data: this.setFields(data) };
    const dialogRef = this.dialog.open(ModalReutilComponent, {
      width: '550px',
      data: dataDielog,
    });

    return dialogRef.afterClosed().subscribe((result) => {
      // this.animal = result;
      if (result) {
        if (result.id) {
          //console.log(result);

          this.updateAttached(result);
        } else {
          // this.storeFeature(result)
        }
      }
    });
  }


  openDialogPreview(data) {
    // console.log(data);

    const dialogRef = this.dialog.open(ModalPreviewComponent, {
      width: '800px',
      height: '90vh',
      data: data,
    });

    return dialogRef.afterClosed().subscribe((result) => {
      // this.animal = result;
      if (result) {
        if (result.id) {
          //console.log(result);

          // this.updateAttached(result);
        } else {
          // this.storeFeature(result)
        }
      }
    });
  }
  updateAttached(data) {
    this.attachedService.updateAttached(data).pipe(take(1)).subscribe();
  }
  setFields(elementEdit?: Attached) {
    // const options = this.groupsFeature.pipe(map(v => { return v.map( o => { return { name: o.name, value: o.id}}) })).subscribe(res => console.log(res))

    const fields = [
      {
        nameControl: 'id',
        type: 'hidden',
        value: elementEdit?.id,
        label: 'Id',
      },
      {
        nameControl: 'name',
        type: 'text',
        value: elementEdit?.name,
        validators: [Validators.required],
        label: 'Nombre',
        class: 'mvd-col1--1',
      },
    ];

    return fields;
  }

  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    this.subscroption.unsubscribe();
  }

  /////UPLOAD
  files = new FormControl([null]);
  files_consentimiento = new FormControl([null]);

  upload(e, consentimiento: boolean = false) {
    //console.log(e.target.files);
    const files = e.target.files;
    let formData = new FormData();
    formData.append('patient_id', this.patient.id.toString());
    formData.append('consentimiento', consentimiento ? '1' : '0');

    for (let i = 0; i < files.length; i++) {
      formData.append('files[]', files[i]);
    }

    this.attachedService
      .storeAttach(formData)
      .subscribe((res) => {
        if (this.patient?.documents?.length >= 1) {
          const documentsConsentimiento = this.patient.documents.filter(
            (v) => v.consentimiento
          );
          this.consentimiento =
            documentsConsentimiento.length >= 1 ? true : false;
        }
      });
  }

  downloadFile(element:Attached){
    this.attachedService.download(element).subscribe((response: any) => { //when you use stricter type checking
			let blob:any = new Blob([response]);
			const url = window.URL.createObjectURL(blob);
			//window.open(url);
			//window.location.href = response.url;
			fileSaver.saveAs(blob , element.name)
		//}), error => console.log('Error downloading the file'),
		}), (error: any) => console.log('Error downloading the file'), //when you use stricter type checking
                 () => console.info('File downloaded successfully');
  }
}
