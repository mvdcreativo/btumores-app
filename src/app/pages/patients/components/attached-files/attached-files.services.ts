import { HttpClient, HttpParams } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { MatSnackBar } from "@angular/material/snack-bar";
import { Router } from "@angular/router";
import { BehaviorSubject, Observable } from "rxjs";
import { map, take } from "rxjs/operators";
import { Response, ResponsePaginate } from "src/app/shared/interfaces/response";
import { environment } from "src/environments/environment";
import { Patient } from "../../interfaces/patient";
import { PatientsService } from "../../services/patients.service";
import { Attached } from "./attached";


@Injectable({
    providedIn: 'root'
})

export class AttachedFilesService {

    private resultSubject$: BehaviorSubject<ResponsePaginate> = new BehaviorSubject(null)
    private attachedEditSubject$: BehaviorSubject<Attached> = new BehaviorSubject<Attached>(null)
    patient: Patient;

    public get resultItems$() {
        return this.resultSubject$
    }


    public setItems(value) {
        this.resultSubject$.next(value)
    }


    constructor(
        private http: HttpClient,
        private snackBar: MatSnackBar,
        private patientService: PatientsService,
        private router: Router,
    ) {
        this.patientService.patientOnEdit.subscribe(res => this.patient = res)
    }

    get attachedOnEdit(): Observable<Attached> {
        return this.attachedEditSubject$
    }

    setAttachedOnEdit(value) {
        this.attachedEditSubject$.next(value)
    }

    storeAttach(data): Observable<Attached> {

        return this.http.post<Response>(`${environment.API}documents`, data).pipe(
            map(v => {
                this.getAttacheds(this.patient?.id, 1, 20, '', 'desc').pipe(take(1)).subscribe(
                  res=> {
                    const attaches = res
                    this.patientService.patientOnEdit.pipe(take(1)).subscribe(res=>{
                      let patient = {...res}
                      patient.documents= attaches.data.data
                      this.patientService.setPatientOnEdit(patient)
                    })
                  }
                )
                return v.data
            })

        )
    }
    getAttached(id): Observable<Attached> {
        return this.http.get<Response>(`${environment.API}documents/${id}`).pipe(map(
            res => {
                this.setAttachedOnEdit(res.data)
                const resp = res.data
                return resp;
            }
        )

        )
    }


    ///listar
    getAttacheds(patient_id = this.patient?.id ,currentPage = 1, perPage = 20, filter = '', sort = 'desc'): Observable<ResponsePaginate> {
        return this.http.get<ResponsePaginate>(`${environment.API}documents`, {
            params: new HttpParams()
                .set('page', currentPage.toString())
                .set('filter', filter)
                .set('sort', sort)
                .set('patient_id', patient_id?.toString())
                .set('per_page', perPage.toString())

        }).pipe(map(
            res => {

                this.setAttachedOnEdit(null)
                this.setItems(res)
              //console.log(res)
                const resp = res
                return resp;
            }
        )

        )
    }

    updateAttached(data): Observable<Attached> {
        return this.http.put<Response>(`${environment.API}documents/${data.id}`, data).pipe(
            map(v => {
                this.getAttacheds(this.patient?.id, 1, 20, '', 'desc').pipe(take(1)).subscribe()
                //snacbarr
                this.openSnackBar('Actualizado correctamente', 'success-snack-bar')
                //////////
                return v.data
            })
        )

    }

    deleteAttached(id) {
        return this.http.delete<Response>(`${environment.API}documents/${id}`).pipe(
            map(v => {
                // console.log(v.data);

                this.getAttacheds(this.patient?.id, 1, 20, '', 'desc').pipe(take(1)).subscribe(
                  res=> {
                    const attaches = res
                    this.patientService.patientOnEdit.pipe(take(1)).subscribe(res=>{
                      let patient = {...res}
                      patient.documents= attaches.data.data
                      this.patientService.setPatientOnEdit(patient)
                    })
                  }
                )
                //snacbarr
                this.openSnackBar('Eliminado correctamente', 'success-snack-bar')
                //////////
                return v.data

            })

        )
    }
    download(data){
      return this.http.get(`${environment.API}download_document/${data.id}`,
        {
          responseType: 'blob',
        }
      )
    }

    openSnackBar(message: string, refClass: string, action: string = '') {
        this.snackBar.open(message, action, {
            duration: 2000,
            horizontalPosition: 'center',
            verticalPosition: 'top',
            panelClass: refClass
        });
    }
}
