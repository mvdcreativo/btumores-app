export interface Attached {
    id?:number;
    name:string;
    name_file:string;
    consentimiento?:boolean;

    url_file:string;
}
