import { DecimalPipe } from '@angular/common';
import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDatepickerInputEvent } from '@angular/material/datepicker';
import moment from 'moment';
import { Observable, Subscription } from 'rxjs';
import { debounce, debounceTime, first, single, take } from 'rxjs/operators';
import { Surgery } from 'src/app/pages/surgeries/interfaces/surgery';
import { Topography } from 'src/app/pages/topographies/interfaces/topography';
import { DataPatient, Patient } from '../../interfaces/patient';
import { PatientsService } from '../../services/patients.service';

@Component({
  selector: 'mvd-basic-data',
  templateUrl: './basic-data.component.html',
  styleUrls: ['./basic-data.component.scss'],
  providers:[DecimalPipe]
})
export class BasicDataComponent implements OnInit {

  @Output() dataBasic: EventEmitter<any> = new EventEmitter()

  form: FormGroup;
  formAntecedents: FormGroup;
  surgeries: Observable<Surgery[]>;
  topographies: Observable<Topography[]>;
  imc: boolean;
  dataSelects: Observable<any>;
  subscriptions: Subscription[]=[];
  patientEdit: Patient;
  formAntecedentsFamily: FormGroup;
  parentesco: any[]=[];

  constructor(
    private fb: FormBuilder,
    private patientService: PatientsService,
    private decimalPipe: DecimalPipe
  ) {

   }

  ngOnInit(): void {
    this.surgeries = this.patientService.getSurgeries();
    this.topographies = this.patientService.getTopographies();
    this.dataSelects = this.patientService.dataSelect()
    this.createForm()

    this.subscriptions.push(
      this.patientService.patientOnEdit.subscribe(res => {
        this.patientEdit = res ;

        this.formAntecedents.patchValue({
          patient_id: this.patientEdit?.id
        })
        this.patientEdit?.patient_data ? this.updateForm(this.patientEdit.patient_data): false

      }),
    )
    this.patientService.patientOnEdit.pipe(first()).subscribe(res => this.parentesco = res?.patient_data?.antecedente_directo_tipo ? res.patient_data.antecedente_directo_tipo.split(','): [])
  }


  createForm(){
    this.form = this.fb.group({
      patient_id: [],
      date_surgery: [],
      topography_id: [],
      type_surgery_id: [],
      imc: [false],
      imc_talla: [''],
      imc_peso: [''],
      imc_imc: [''],
      fumador: [false],
      fumador_activo: [false],
      fumador_cant: [''],
      fumador_periodo:[null],
      alcoholista: [false],
      alcoholista_activo: [false],
      alcoholista_cant: [''],
      alcoholista_periodo: [null],
      drogas: [false],
      drogas_activo: [false],
      drogas_tipo: [''],
      drogas_periodo: [null],
      rt: [false],
      rt_donde: [''],
      rt_date: [''],
      anticonceptivos: [false],
      anticonceptivos_periodo: [''],
      amamantar: [false],
      amamantar_periodo: [''],
      hormonas: [false],
      hormonas_periodo: [''],
      tipo_trh: [''],
      ambientales: [false],
      ambientales_cuales: [''],
      factor_r_especifico: [''],
      mamografia: [false],
      mamografia_frecuencia: [''],
      mamografia_otros: [''],
      mamografia_date_ultima: [''],
      pap: [false],
      pap_frecuencia: [''],
      pap_otros: [''],
      pap_date_ultima: [''],

      fecatest: [false],
      fecatest_frecuencia: [''],
      fecatest_otros: [''],
      fecatest_date_ultima: [''],

      fibrocolonoscopia: [false],
      fibrocolonoscopia_frecuencia: [''],
      fibrocolonoscopia_otros: [''],
      fibrocolonoscopia_date_ultima: [''],

      fibrogastroscopia: [false],
      fibrogastroscopia_frecuencia: [''],
      fibrogastroscopia_otros: [''],
      fibrogastroscopia_date_ultima: [''],

      psa: [false],
      psa_frecuencia: [''],
      psa_otros: [''],
      psa_date_ultima: [''],
      psa_result:[''],

      rm_mamaria: [false],
      rm_mamaria_frecuencia: [''],
      rm_mamaria_otros: [''],
      rm_mamaria_date_ultima: [''],

      edad_menarca: [''],
      edad_primer_emb: [''],
      menopausia_edad: [''],
      menopausia_quirurgica: [false],
      antecedente: [false],
      antecedente_directo: [false],
      antecedente_directo_tipo: [] ,
      antecedente_indirectos: [false],
      antecedente_indirectos_tipo: [''],
      anterior: [false],
      // anterior_topography_id: [null],
      // anterior_edad: [''],
      date_create:[''],
      date_update:['']
    })

    this.formAntecedents = this.fb.group({
      patient_id:[null],
      topography_id:[null],
      edad:[null]
    })
    this.formAntecedentsFamily = this.fb.group({
      topography:[null],
      parentesco:[null]
    })


    this.listenForm()
  }





  updateForm(patientData: DataPatient){

    this.form.patchValue({
      patient_id: patientData.patient_id,
      date_surgery: patientData.date_surgery,
      topography_id: patientData.topography_id,
      type_surgery_id: patientData.type_surgery_id,
      imc: patientData.imc,
      imc_talla: patientData.imc_talla,
      imc_peso: patientData.imc_peso,
      imc_imc: patientData.imc_imc,
      fumador: patientData.fumador,
      fumador_activo: patientData.fumador_activo,
      fumador_cant: patientData.fumador_cant,
      fumador_periodo: patientData.fumador_periodo,
      alcoholista: patientData.alcoholista,
      alcoholista_activo: patientData.alcoholista_activo,
      alcoholista_cant: patientData.alcoholista_cant,
      alcoholista_periodo: patientData.alcoholista_periodo,
      drogas: patientData.drogas,
      drogas_activo: patientData.drogas_activo,
      drogas_tipo: patientData.drogas_tipo,
      drogas_periodo: patientData.drogas_periodo,
      rt: patientData.rt,
      rt_donde: patientData.rt_donde,
      rt_date: patientData.rt_date,
      anticonceptivos: patientData.anticonceptivos,
      anticonceptivos_periodo: patientData.anticonceptivos_periodo,
      amamantar: patientData.amamantar,
      amamantar_periodo: patientData.amamantar_periodo,
      hormonas: patientData.hormonas,
      hormonas_periodo: patientData.hormonas_periodo,
      tipo_trh: patientData.tipo_trh,
      ambientales: patientData.ambientales,
      ambientales_cuales: patientData.ambientales_cuales,
      factor_r_especifico: patientData.factor_r_especifico,
      mamografia: patientData.mamografia,
      mamografia_frecuencia: patientData.mamografia_frecuencia,
      mamografia_otros: patientData.mamografia_otros,
      mamografia_date_ultima: patientData.mamografia_date_ultima,

      pap: patientData.pap,
      pap_frecuencia: patientData.pap_frecuencia,
      pap_otros: patientData.pap_otros,
      pap_date_ultima: patientData.pap_date_ultima,

      fecatest: patientData.fecatest,
      fecatest_frecuencia: patientData.fecatest_frecuencia,
      fecatest_otros: patientData.fecatest_otros,
      fecatest_date_ultima: patientData.fecatest_date_ultima,

      fibrocolonoscopia: patientData.fibrocolonoscopia,
      fibrocolonoscopia_frecuencia: patientData.fibrocolonoscopia_frecuencia,
      fibrocolonoscopia_otros: patientData.fibrocolonoscopia_otros,
      fibrocolonoscopia_date_ultima: patientData.fibrocolonoscopia_date_ultima,

      fibrogastroscopia: patientData.fibrogastroscopia,
      fibrogastroscopia_frecuencia: patientData.fibrogastroscopia_frecuencia,
      fibrogastroscopia_otros: patientData.fibrogastroscopia_otros,
      fibrogastroscopia_date_ultima: patientData.fibrogastroscopia_date_ultima,

      psa: patientData.psa,
      psa_frecuencia: patientData.psa_frecuencia,
      psa_otros: patientData.psa_otros,
      psa_date_ultima: patientData.psa_date_ultima,
      psa_result:patientData.psa_result,

      rm_mamaria: patientData.rm_mamaria,
      rm_mamaria_frecuencia: patientData.rm_mamaria_frecuencia,
      rm_mamaria_otros: patientData.rm_mamaria_otros,
      rm_mamaria_date_ultima: patientData.rm_mamaria_date_ultima,

      edad_menarca: patientData.edad_menarca,
      edad_primer_emb: patientData.edad_primer_emb,
      menopausia_edad: patientData.menopausia_edad,
      menopausia_quirurgica: patientData.menopausia_quirurgica,
      antecedente: patientData.antecedente,
      antecedente_directo: patientData.antecedente_directo,
      antecedente_directo_tipo: patientData?.antecedente_directo_tipo?.split(',') || [],
      antecedente_indirectos: patientData.antecedente_indirectos,
      antecedente_indirectos_tipo: patientData.antecedente_indirectos_tipo,
      anterior: this.patientEdit?.cancer_patient_antecedents ?true:false || patientData.anterior,
      // anterior_topography_id: patientData.anterior_topography_id,
      // anterior_edad: patientData.anterior_edad,
      date_create:'',
      date_update:''
    })




  }

  submitAntecedent(){
    this.patientService.storeCancerAntecedents(this.formAntecedents.value).pipe(take(1)).subscribe()
    this.formAntecedents.reset()
  }
  removeAntecedent(id){
    this.patientService.removeAntecedentCancer(id).pipe(take(1)).subscribe()

  }

  submitParentesco(){
    const topography = this.formAntecedentsFamily.get('topography').value
    const parentesco = this.formAntecedentsFamily.get('parentesco').value
    const actual = this.parentesco
    this.parentesco = [...actual, `${parentesco} (${topography})`];
    this.formAntecedentsFamily.reset()
  }

  removeParentesco(item){
    this.parentesco = this.parentesco.filter(v=> item !== v)
  }
  private calculaImc(){

    const talla = this.form.get('imc_talla').value;
    const peso = this.form.get('imc_peso').value;
    const m2 = talla*talla;
    const imc = this.decimalPipe.transform(peso / m2);
    if (talla && peso && m2 && imc) {
      this.form.get('imc_imc').setValue(imc);
    }
  }

  listenForm(){
    this.form.valueChanges.pipe(debounceTime(500) ).subscribe(value=>{
      this.listenImc(value)
      this.form.get('antecedente_directo_tipo').setValue(this.parentesco)
      this.dataBasic.emit(this.form)
    })
  }

  formatDate(e : MatDatepickerInputEvent<Date>, campo){
    let brt = e.value;
    this.form.get(campo).setValue(moment(brt).format('YYYY-MM-DD HH:mm:ss'))
  }




  listenImc(value){
    // this.imc = value.imc;

    let talla = this.form.get('imc_talla');
    let peso = this.form.get('imc_peso');

    talla.valueChanges.pipe(debounceTime(1000) ).subscribe(value=> this.calculaImc())
    peso.valueChanges.pipe(debounceTime(1000) ).subscribe(value=> this.calculaImc())

  }

  checkValue(value){
    if (value.imc  === false) {
      if(value.imc_talla || value.imc_peso || value.imc_imc ){
        this.form.get('imc_talla').reset()
        this.form.get('imc_peso').reset()
        this.form.get('imc_imc').reset()

      }
    }

    if (value.fumador === false) {
      if(value.fumador_activo || value.fumador_cant ){
        this.form.get('fumador_activo').reset()
        this.form.get('fumador_cant').reset()
      }
    }

    if (value.drogas  === false) {
      if(value.drogas_activo || value.drogas_tipo ){
        this.form.get('drogas_activo').reset()
        this.form.get('drogas_tipo').reset()
      }
    }

    if (value.rt === false) {
      if(value.rt_donde || value.rt_date){
        this.form.get('rt_date').reset()
        this.form.get('rt_donde').reset()
      }
    }

    if (value.anticonceptivos === false) {
      if(value.anticonceptivos_periodo){
        this.form.get('anticonceptivos_periodo').reset()
      }
    }

    if (value.amamantar === false) {
      if(value.amamantar_periodo){
        this.form.get('amamantar_periodo').reset()
      }
    }

    if (value.hormonas === false) {
      if(value.hormonas_periodo){
        this.form.get('hormonas_periodo').reset()
      }
    }

    if (value.ambientales === false) {
      if(value.ambientales_cuales){
        this.form.get('ambientales_cuales').reset()
      }
    }

    if (value.mamografia === false) {
      if(value.mamografia_frecuencia || value.mamografia_otros || value.mamografia_date_ultima){
        this.form.get('mamografia_frecuencia').reset()
        this.form.get('mamografia_otros').reset()
        this.form.get('mamografia_date_ultima').reset()
      }
    }

    if (value.pap === false) {
      if(value.pap_frecuencia || value.pap_otros || value.pap_date_ultima){
        this.form.get('pap_frecuencia').reset()
        this.form.get('pap_otros').reset()
        this.form.get('pap_date_ultima').reset()
      }
    }

    if (value.antecedente === false) {
      if(value.antecedente_directo !== false || value.antecedente_indirectos !== false ){
        this.form.get('antecedente_directo').setValue(false)
        this.form.get('antecedente_directo_tipo').setValue(null)
        this.form.get('antecedente_indirectos').setValue(false)
        this.form.get('antecedente_indirectos_tipo').setValue(null)
      }
    }

    if (value.anterior === false) {
      if(value.anterior_topography_id !== null || value.anterior_edad !== null ){
        this.form.get('anterior_topography_id').setValue(null)
        this.form.get('anterior_edad').setValue(null)
      }
    }
  }



  // onTipoAntecedenteRemoved(item_rm: string) {
  //   const item = this.form.get('antecedente_directo_tipo').value as string[];
  //   this.removeFirst(item, item_rm);

  //   this.form.get('antecedente_directo_tipo').setValue(item) // To trigger change detection
  // }

  // private removeFirst<T>(array: T[], toRemove: T): void {
  //   const index = array.indexOf(toRemove);
  //   if (index !== -1) {
  //     array.splice(index, 1);
  //   }
  // }

ngOnDestroy(): void {
  this.subscriptions.map(v=>v.unsubscribe())
}
}
