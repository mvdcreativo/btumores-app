import { Component, Input, OnInit } from '@angular/core';
import { Patient } from '../../interfaces/patient';

@Component({
  selector: 'mvd-family-patient',
  templateUrl: './family.component.html',
  styleUrls: ['./family.component.scss']
})
export class FamilyComponent implements OnInit {

  @Input()patient: Patient

  constructor() { }

  ngOnInit(): void {
  }

}
