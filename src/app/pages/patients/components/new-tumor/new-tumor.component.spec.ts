import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NewTumorComponent } from './new-tumor.component';

describe('NewTumorComponent', () => {
  let component: NewTumorComponent;
  let fixture: ComponentFixture<NewTumorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NewTumorComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NewTumorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
