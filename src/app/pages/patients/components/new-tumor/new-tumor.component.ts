import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDatepicker, MatDatepickerInputEvent } from '@angular/material/datepicker';
import moment from 'moment';
import { Observable, Subscription } from 'rxjs';
import { take } from 'rxjs/operators';
import { Topography } from 'src/app/pages/topographies/interfaces/topography';
import { Patient } from '../../interfaces/patient';
import { PatientsService } from '../../services/patients.service';
import { NewTumorService } from './service/new-tumor.service';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { MAT_MOMENT_DATE_ADAPTER_OPTIONS, MomentDateAdapter } from '@angular/material-moment-adapter';


export const MY_FORMATS = {
  parse: {
    dateInput: 'YYYY',
  },
  display: {
    dateInput: 'YYYY',
    monthYearLabel: 'YYYY',
    monthYearA11yLabel: 'YYYY',
  },
};

@Component({
  selector: 'mvd-new-tumor',
  templateUrl: './new-tumor.component.html',
  styleUrls: ['./new-tumor.component.scss'],
  providers: [
    {
     provide: MAT_DATE_FORMATS, useValue: MY_FORMATS
    },
   ]
})
export class NewTumorComponent implements OnInit {

  @ViewChild('picker', { static: false })
  private picker!: MatDatepicker<Date>;
  selectYear: any;

  @Input() patient: Patient


  topographies: Observable<Topography[]>;


  form: FormGroup;
  subscriptions: Subscription= new Subscription();

  dataSelects: Observable<any>;

  constructor(
    private patientService: PatientsService,
    private newTumorService: NewTumorService,
    private fb: FormBuilder
  ) {
    this.topographies = this.patientService.getTopographies();
  }

  ngOnInit(): void {
    this.newTumorService.setPatient(this.patient)
    this.createForm()
  }

  ngOnChanges(): void {
    this.newTumorService.setPatient(this.patient)
  }

  createForm(){
    this.form = this.fb.group({
      patient_id: this.patient?.id,
      topography_id: [],
      date: [],
      obs: []
    })
  }

  removeAntecedent(id){
    this.subscriptions.add(this.newTumorService.removeTumor(id).subscribe())

  }

  submitAntecedent(){
    console.log(this.form.value);

    this.subscriptions.add (
      this.newTumorService.storeTumor(this.form.value).subscribe()
    )
    this.form.reset()
    this.form.patchValue({
      patient_id: this.patient.id,
    })
  }

  ngOnDestroy(): void {
    this.subscriptions.unsubscribe()
  }

  chosenYearHandler(ev, input){
    let { _d } = ev;
    this.selectYear = _d;
    console.log(ev);
    let { _i } = ev;
    let date = new Date("1/1/"+ _i.year)
    console.log(date);
    this.formatDate(date,'date')
    this.picker.close()
  }

  formatDate(e : any, campo){
    let date = e;
    this.form.get(campo).setValue(moment(date).format('YYYY-MM-DD HH:mm:ss'))
  }
}
