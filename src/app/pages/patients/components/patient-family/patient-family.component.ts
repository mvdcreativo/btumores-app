import { Component, OnInit, ChangeDetectionStrategy, Input } from '@angular/core';
import { Patient } from '../../interfaces/patient';

@Component({
  selector: 'mvd-patient-family',
  templateUrl: './patient-family.component.html',
  styleUrls: ['./patient-family.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PatientFamilyComponent implements OnInit {

  @Input() patient : Patient
  constructor() { }

  ngOnInit(): void {
  }

}
