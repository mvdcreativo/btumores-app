import { Component, EventEmitter, Input, OnDestroy, OnInit, Output, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl, AbstractControl } from '@angular/forms';
import { Observable, Subscription } from 'rxjs';
import { State, City, Country } from 'src/app/shared/interfaces/ubication';
import { UbicationService } from 'src/app/shared/services/ubications/ubication.service';
import { Patient } from '../../interfaces/patient';
import { PatientsService } from '../../services/patients.service';

import { CustomValidators } from 'ngx-custom-validators';

import _moment from 'moment';
import { MatDatepickerInputEvent } from '@angular/material/datepicker';
import { debounceTime, filter, map, startWith, take } from 'rxjs/operators';
import { MvdValidations } from 'src/app/shared/components/forms/mvd-validations/mvd-validations';
import { LocationService } from 'src/app/pages/locations/services/location.service';
import { MatAutocompleteTrigger } from '@angular/material/autocomplete';
import { MedicalInstitution } from 'src/app/pages/medical-institutions/interfaces/medicalInstitution';
import { Doctor } from 'src/app/pages/doctors/interfaces/doctor';
import { MatDialog } from '@angular/material/dialog';
import { MedicalInstitutionsService } from 'src/app/pages/medical-institutions/services/medical-institutions.service';
import { ModalReutilComponent } from 'src/app/shared/components/modals/modal-reutil/modal-reutil.component';
import { DoctorsService } from 'src/app/pages/doctors/services/doctors.service';
const moment = _moment;



@Component({
  selector: 'mvd-patient-identification',
  templateUrl: './patient-identification.component.html',
  styleUrls: ['./patient-identification.component.scss'],

})
export class PatientIdentificationComponent implements OnInit , OnDestroy{

  @Output() dataIdentification : EventEmitter<any> = new EventEmitter()
  @ViewChild(MatAutocompleteTrigger) autocomplete: MatAutocompleteTrigger;

  form: FormGroup;
  states: State[];
  cities:  City[];
  countries: Observable<Country[]>;
  dataSelects$: Observable<any>;
  medicaInstitutions: MedicalInstitution[];
  doctors: Doctor[];
  age: number;
  patientEdit:Patient;
  subscriptions: Subscription[]=[];
  patient: Patient;
  n_docExists: string;
  lastId: any;
  filteredStates:Observable<State[]>;
  filteredCities: Observable<City[]>;
  filteredMedicaInstitutions: Observable<MedicalInstitution[]>;
  filteredSurgeryInstitution: Observable<MedicalInstitution[]>;
  filteredDoctorsDeriv: Observable<Doctor[]>;
  filteredDoctors: Observable<Doctor[]>;

  constructor(
    private fb: FormBuilder,
    private ubicationService: UbicationService,
    private patientService: PatientsService,
    private locationService: LocationService,
    private medicalInstitutionService: MedicalInstitutionsService,
    private doctorService: DoctorsService,
    public dialog: MatDialog,
  ) {
    this.subscriptions.push(
      this.ubicationService.statesItems$.subscribe(res=>this.states = res),
      this.ubicationService.citiesItems$.subscribe(res=>this.cities = res),
      this.patientService.medicalInstitutionsItems$.subscribe(res=> this.medicaInstitutions = res),
      this.patientService.doctorsItems$.subscribe(res=>this.doctors = res)
      )
    this.countries = this.ubicationService.countriesItems$;
    this.dataSelects$ = this.patientService.dataSelect()
  }

  ngOnInit(): void {
    this.createForm()
    this.subscriptions.push(
      this.patientService.patientOnEdit.subscribe(res => {
        this.patientEdit = res ;
        this.patientEdit ? this.updateForm(this.patientEdit): false

      }),

    )
  }

  createForm(){
    this.form = this.fb.group({
      code:[null, Validators.required, MvdValidations.validateCode(this.patientService)],
      birth:[null, Validators.required],
      type_doc:['CI', Validators.required],
      n_doc:[null, Validators.required, MvdValidations.validateNumDoc(this.patientService)],
      name:[null, Validators.required],
      last_name:[null, Validators.required],
      phone:[null],
      address:[null],
      email:[null, [CustomValidators.email]],
      ashkenasi:[false],
      gender:[null],
      type_patient:[null, Validators.required],
      nationality_id: [null],
      city_id:[null],
      medical_institution_id:[null],
      surgery_institution_id:[null],
      registroH: [null],
      doctor_id:[null],
      derived_by_id:[null],
      breed_id:[null],
      obs:[null],
      country_id:[null],
      state_id:[null],
      age:[null, Validators.required],
      dead: [false],
      date_dead: [null],
      proyect_investigation_id: [null]
    })

    this.form.valueChanges.pipe(debounceTime(500) ).subscribe(value => {
      this.dataIdentification.emit(this.form)
      // console.log(this.form.value);

    })
    this.filteredStates =  this.form.get('state_id').valueChanges
    .pipe(
      startWith(''),
      map(state => {
        // console.log(state);
        return state ? this._filterStates(state) : this.states
      })
    );

    this.filteredCities =  this.form.get('city_id').valueChanges
    .pipe(
      startWith(''),
      map(city => {
        return city ? this._filterCities(city) : this.cities
      })
    );

    this.filteredSurgeryInstitution =  this.form.get('surgery_institution_id').valueChanges
    .pipe(
      startWith(''),
      map(val => {
        return val ? this._filterMedicalInstitutions(val) : this.medicaInstitutions
      })
    );

    this.filteredMedicaInstitutions =  this.form.get('medical_institution_id').valueChanges
    .pipe(
      startWith(''),
      map(val => {
        return val ? this._filterMedicalInstitutions(val) : this.medicaInstitutions
      })
    );

    this.filteredDoctorsDeriv =  this.form.get('derived_by_id').valueChanges
    .pipe(
      startWith(''),
      map(val => {
        return val ? this._filterDoctors(val) : this.doctors
      })
    );

    this.filteredDoctors =  this.form.get('doctor_id').valueChanges
    .pipe(
      startWith(''),
      map(val => {
        return val ? this._filterDoctors(val) : this.doctors
      })
    );

  }

  formatDate(e : MatDatepickerInputEvent<Date>){
    let brt = e.value;
    this.form.get('birth').setValue(moment(brt).format('YYYY-MM-DD HH:mm:ss'))
    if(this.form.get('dead').value){
      this.calculoEdad(this.form.get('birth').value , this.form.get('date_dead').value)
    }else{
      this.calculoEdad(this.form.get('birth').value)
    }


  }
  formatDateDead(e : MatDatepickerInputEvent<Date>){
    let date_dead = e.value;
    this.form.get('date_dead').setValue(moment(date_dead).format('YYYY-MM-DD HH:mm:ss'))

    if(this.form.get('dead').value){
      this.calculoEdad(this.form.get('birth').value , this.form.get('date_dead').value)
    }else{
      this.calculoEdad(this.form.get('birth').value)
    }

  }

  calculoEdad(value, date_dead?){
    const date = moment()
    const birth = moment(value);

    let years;
    if (date_dead) {
      const dead = moment(date_dead);
      years = dead.diff(birth,"years");
    }else{
      years = date.diff(birth,"years");
    }

    this.form.get('age').setValue(years)
  }

  private updateForm(patient){
    // console.log(patient);

    this.form.patchValue({
      country_id: patient?.country_id ? patient?.country_id : patient?.city?.state?.country_id ,
      state_id: patient?.state_id ? patient?.state_id : patient?.city?.state_id,
      code: patient?.code,
      birth: patient?.birth,
      type_doc: patient?.type_doc,
      n_doc: patient?.n_doc,
      name: patient?.name,
      last_name: patient?.last_name,
      phone: patient?.phone,
      address: patient?.address,
      email: patient?.email,
      ashkenasi: patient?.ashkenasi,
      gender: patient?.gender,
      type_patient: patient?.type_patient,
      nationality_id:  patient?.nationality_id,
      medical_institution_id: patient?.medical_institution_id,
      surgery_institution_id: patient?.surgery_institution_id,
      registroH: patient?.registroH,
      doctor_id: patient?.doctor_id,
      derived_by_id: patient?.derived_by_id,
      breed_id: patient?.breed_id,
      obs: patient?.obs,
      city_id: patient?.city_id,
      age:"",
      dead: patient?.dead,
      date_dead: patient?.date_dead,
      proyect_investigation_id: null
    })

    if(this.form.get('dead').value){
      this.calculoEdad(this.form.get('birth').value , this.form.get('date_dead').value)
    }else{
      this.calculoEdad(this.form.get('birth').value)
    }

  }

  public onSelectCountry() {
    this.form.get('state_id').setValue(null, { emitEvent: false });
    this.form.get('city_id').setValue(null, { emitEvent: false });
  }

  public onSelectState() {
    this.form.get('city_id').setValue(null, { emitEvent: false });
  }
  public onSelectCity() {

  }

  private _filterStates(value: string): State[] {
    const filterValue = typeof value === 'string' ? value.toLowerCase() : "";
    if(this.states){
      return this.states.filter(item => item.name.toLowerCase().includes(filterValue))

    }

  }
  getNameStates(id: number) {
    if(id && this.states){
      return this.states.find(item => item.id === id).name;
    }
  }

  private _filterCities(value: string): City[] {
    const filterValue = typeof value === 'string' ? value.toLowerCase() : "";
    if(this.cities){
    return this.cities.filter(item => item.name.toLowerCase().includes(filterValue))
    }

  }

  getNameCities(id: number) {
    if(id && this.cities){
      return this.cities.find(item => item.id === id).name;
    }
  }


  private _filterMedicalInstitutions(value: string): any[] {
    const filterValue = typeof value === 'string' ? value.toLowerCase() : "";
    if(this.medicaInstitutions){
      return this.medicaInstitutions.filter(item => item.name.toLowerCase().includes(filterValue))
    }
  }

  getNameMedicalInst(id: number) {
    if(id && this.medicaInstitutions){
      return this.medicaInstitutions.find(item => item.id === id).name;
    }
  }

  private _filterDoctors (value: string): any[] {
    const filterValue = typeof value === 'string' ? value.toLowerCase() : "";
    if(this.doctors){
      return this.doctors.filter(item => {
        return item.name.toLowerCase().includes(filterValue) || item.last_name.toLowerCase().includes(filterValue)
      })
    }
  }

  getNameDoctor(id: number) {
    if(id && this.doctors){
      const doctor = this.doctors.find(item => item.id === id)
      // console.log(doctor);
      return `${doctor.name} ${doctor.last_name}`
    }
  }



  setCode(e){
  //console.log(e);
    const prefix = e.value.slice(1)
    if(!this.patientEdit){
      this.patientService.lastId().subscribe( res=> {
        this.form.get('code').setValue(`${prefix}-${res+1}`)
      })
    }else{
      this.form.get('code').setValue(`${prefix}-${this.patientEdit.id}`)
    }

  }

  addCity(){
    setTimeout(() => {
      this.autocomplete.closePanel();
    }, 100);

    this.locationService.openDialog('cities',null, 10000)
    this.subscriptions.push(
        this.locationService.resultCitiesItems$.subscribe(res=> {
          if(res?.data?.data.length >= 1){
            this.cities = res?.data?.data
            this.ubicationService.setCitiesItems(res?.data?.data)
            const lastId = Math.max.apply(Math, this.cities.map(function(v) { return v.id; }))
            this.form.get('city_id').setValue(lastId)

          }
        })

      )
  }

  addMedicaInstitutions(control : AbstractControl) {
    const title_form = 'Agrega Institución Médica'
    const fields = [
      { nameControl: 'name', type: 'text', value:"", validators: [Validators.required], label: 'Nombre', class:'mvd-col1--1' },
    ]
    const dataDielog = { title: title_form, data: fields };

    setTimeout(() => {
      const dialogRef = this.dialog.open(ModalReutilComponent, {
        width: '300px',
        data: dataDielog
      });
      return dialogRef.afterClosed().subscribe(result => {
        // this.animal = result;
        if (result) {
            this.medicalInstitutionService.storeMedicalInstitution(result).pipe(take(1)).subscribe(
              res => {
                // console.log(res);
                this.medicaInstitutions = [...this.medicaInstitutions, res]
                this.patientService.setMedicalInstitutionsItems(this.medicaInstitutions)
                // control.setValue(res.name)
                control.setValue(res.id)
              }
            )

        }

      });
    }, 100);

  }


  addDoctor(control : AbstractControl){
    const title_form = 'Agrega Médico'
    const fields = [
      { nameControl: 'name', type: 'text', value:"", validators: [Validators.required], label: 'Nombre', class:'mvd-col1--1' },
      { nameControl: 'last_name', type: 'text', value:"", validators: [Validators.required], label: 'Apellido', class:'mvd-col1--1' },
      { nameControl: 'contact', type: 'text', value:"", label: 'Contacto', class:'mvd-col1--1' },
    ]
    const dataDielog = { title: title_form, data: fields };



    setTimeout(() => {
      const dialogRef = this.dialog.open(ModalReutilComponent, {
        width: '300px',
        data: dataDielog
      });
      return dialogRef.afterClosed().subscribe(result => {
        // this.animal = result;
        if (result) {
            this.doctorService.storeDoctor(result).pipe(take(1)).subscribe(
              res => {
                // console.log(res);
                this.doctors = [...this.doctors, res]
                this.patientService.setDoctorsItems(this.doctors)
                // control.setValue(res.name)
                control.setValue(res.id)
              }
            )

        }

      });
    }, 100);
  }



  ngOnDestroy(){
    this.subscriptions.map(v=> v.unsubscribe() )
  }





}
