import { Component, Input, OnInit } from '@angular/core';
import { Patient } from '../../interfaces/patient';

@Component({
  selector: 'mvd-patient-samples',
  templateUrl: './patient-samples.component.html',
  styleUrls: ['./patient-samples.component.scss']
})
export class PatientSamplesComponent implements OnInit {

  @Input() patient : Patient

  constructor() { }

  ngOnInit(): void {
  }

}
