import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ScorePatientComponent } from './score-patient.component';

describe('ScorePatientComponent', () => {
  let component: ScorePatientComponent;
  let fixture: ComponentFixture<ScorePatientComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ScorePatientComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ScorePatientComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
