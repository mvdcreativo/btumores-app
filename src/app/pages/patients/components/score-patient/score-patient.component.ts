import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Observable, Subscription } from 'rxjs';
import { take } from 'rxjs/operators';
import { Patient } from '../../interfaces/patient';
import { PatientsService } from '../../services/patients.service';
import { ScorePatientService } from './service/score-patient.service';

@Component({
  selector: 'mvd-score-patient',
  templateUrl: './score-patient.component.html',
  styleUrls: ['./score-patient.component.scss']
})
export class ScorePatientComponent implements OnInit {

  @Input() patient: Patient


  form: FormGroup;
  subscriptions: Subscription= new Subscription();
  dataSelects: Observable<any>;

  constructor(
    private patientService: PatientsService,
    private fb: FormBuilder,
    private scoreService: ScorePatientService
  ) { }

  ngOnInit(): void {

    this.scoreService.setPatient(this.patient)
    this.createForm()
  }

  createForm(){
    this.form = this.fb.group({
      patient_id: this.patient?.id,
      score: [null, Validators.required],
      percentage: [0,[Validators.required]],
    })
  }

  removeAntecedent(id){
    this.scoreService.removeScore(id).pipe(take(1)).subscribe()

  }


  submitAntecedent(){
    this.subscriptions.add (
      this.scoreService.storeScore(this.form.value).pipe(take(1)).subscribe()
    )
    this.form.reset()
    this.form.patchValue({
      patient_id: this.patient.id,
    })
  }

  ngOnDestroy(): void {
    this.subscriptions.unsubscribe();
  }
}
