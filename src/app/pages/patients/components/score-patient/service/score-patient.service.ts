import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { map } from 'rxjs/operators';
import { Response } from 'src/app/shared/interfaces/response';
import { environment } from 'src/environments/environment';
import { Patient } from '../../../interfaces/patient';
import { PatientsService } from '../../../services/patients.service';

@Injectable({
  providedIn: 'root'
})
export class ScorePatientService {

  patient: Patient
  constructor(
    private http: HttpClient,
    private patientService: PatientsService,
    private snackBar: MatSnackBar,

  ) { }

  setPatient(patient){
    this.patient = patient
  }

  storeScore(data){
    return this.http.post<Response>(`${environment.API}patient_scores`, data).pipe(
      map( v => {
        console.log(this.patient.patient_scores);

        this.patient.patient_scores = [...this.patient.patient_scores, v.data]
        this.patientService.setPatientOnEdit(this.patient);

        //snacbarr
        this.openSnackBar('Se creó correctamente','success-snack-bar')
        //////////
        return v.data
      })

    )
  }

  removeScore(id){
    return this.http.delete<Response>(`${environment.API}patient_scores/${id}`).pipe(
      map(res => {
          const dataDeleted = this.patient.patient_scores.filter(x=> x.id !== res.data.id)

          this.patient.patient_scores = dataDeleted
          this.patientService.setPatientOnEdit(this.patient);
          const resp = res.data
          return resp;
        }
    ))
  }


  openSnackBar(message: string, refClass:string, action: string = '') {
    this.snackBar.open(message, action, {
      duration: 2000,
      horizontalPosition: 'center',
      verticalPosition: 'top',
      panelClass: refClass
    });
  }
}
