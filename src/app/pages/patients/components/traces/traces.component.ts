import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { date } from 'ngx-custom-validators/src/app/date/validator';
import { Observable, Subscription } from 'rxjs';
import { map, take } from 'rxjs/operators';
import { AuthService } from 'src/app/auth/auth.service';
import { PatientsService } from 'src/app/pages/patients/services/patients.service';
import { EvolutionsService } from 'src/app/pages/evolutions/services/evolutions.service';
import { User } from 'src/app/pages/users/interfaces/user';
import { UsersService } from 'src/app/pages/users/services/users.service';
import { Column } from 'src/app/shared/components/data-table/interfaces/table';
import { ModalReutilComponent } from 'src/app/shared/components/modals/modal-reutil/modal-reutil.component';
import { OptionsSelect } from 'src/app/shared/interfaces/data-selects';
import { ResponsePaginate } from 'src/app/shared/interfaces/response';
import { Patient } from '../../interfaces/patient';
import { TracePatient } from '../../interfaces/tracePatient';
import { TracePatientService } from '../../services/trace-patient.service';

@Component({
  selector: 'mvd-traces',
  templateUrl: './traces.component.html',
  styleUrls: ['./traces.component.scss']
})
export class TracesComponent implements OnInit {

  @Input()patient : Patient;

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  ////COLUMNAS TABLA
  public columns: Column[] = [
    { title: 'Fecha', col: 'created_at', pipe: "dd/MM/yyyy H:mm:ss", class: 'w-15-100' },
    { title: 'Técnico/Profesional', col: 'user_name', class: 'w-15-100' },
    { title: 'Evento/Evolución', col: 'evolution_name', class: 'w-20-100'},
    { title: 'Observaciones', col: 'obs', class: 'w-30-100', type: 'longText'},
    { title: 'Vencimiento', col: 'date_limit', pipe: "dd/MM/yyyy", class: 'w-10-100' },
    { title: 'Fin', col: 'end', type: 'boolean', class: 'w-5-100'}
  ]

  totalResut: Observable<number>;
  pageDefault = 1;
  perPage: number = 10;
  orden: string = 'desc';
  filter: string = '';
  result: Observable<ResponsePaginate>;
  subscroption: Subscription;
  dataSource: Observable<any[]>;
  evolutionsOptions: OptionsSelect[];
  userOptions: { name: string; value: any; }[];
  user: User;
  users: User[];


  constructor(
    private tracePatientService: TracePatientService,
    public dialog: MatDialog,
    private evolutionsServices: EvolutionsService,
    private usersServices: UsersService,
    private authService: AuthService,

  ) {
    this.result = this.tracePatientService.resultItems$

  }

  ngOnInit(): void {

    this.getTraces(this.patient?.id, this.pageDefault, this.perPage, this.filter, this.orden)
    this.getEvolutions()
    this.getUsers()
    console.log(this.patient);


  }

  paginatorChange(e: PageEvent) {
    //console.log(e);
    this.getTraces(this.patient?.id, e.pageIndex + 1, e.pageSize)

  }

  getTraces(patient_id, currentPage?, perPage?, filter?, sort?) {
    this.subscroption = this.tracePatientService.getTraces(currentPage, perPage, filter, sort, patient_id).subscribe(next => this.loadData());
  }

  getEvolutions() {
    this.evolutionsServices.getEvolutions(1,1000).pipe(map(
      v=> {

        const dataSelect = v.data.data.map(x=> {
          return {name: x.name, value: x.id}
        })

        return dataSelect;
      }
    )).subscribe(res=> this.evolutionsOptions =res);

    console.log(this.evolutionsOptions);

  }

  getUsers() {
    this.usersServices.getUsers(1,1000).pipe(map(
      v=> {
        this.users = v.data.data;
        const dataSelect = v.data.data.map(x=> {
          return {name: `${x.name} ${x.last_name}`, value: x.id}
        })

        return dataSelect;
      }
    )).subscribe(res=> this.userOptions =res);

  }


  loadData() {

    this.dataSource = this.result.pipe(map(v => {
      const dataTable = v.data.data.map(x => {
        return {
          id: x.id,
          patient_id: x.patient_id,
          evolution_id: x.evolution_id,
          evolution_name: x.evolution?.name,
          end: x.evolution?.end,
          user_name: x.user_name,
          user_id: x.user_id,
          obs: x.obs,
          date_limit: x.date_limit,
          created_at: x.created_at,
        }
      })
      return dataTable;
    }))

    this.totalResut = this.result.pipe(map(v => v.data.total))

  }

  search(filter) {
    this.getTraces(this.patient?.id, this.pageDefault, this.perPage, filter, this.orden)
  }


  // deleteItem(id): Observable<any> {
  //   return this.tracePatientService.deleteTrace(id)
  // }

  itemAction(event) {
    //console.log(event);

    if (event.action === "delete") {
      // this.deleteItem(event.element.id).pipe(take(1)).subscribe()
    }
    if (event.action === "edit") {
      this.openDialog('Edita Traza',event.element)
    }
  }

  openDialog(title_form = 'Agrega Traza', data?) {
    console.log(this.patient);

    this.user = this.authService.currentUser.value
    const dataDielog = { title: title_form, data: this.setFields(data) };

    const dialogRef = this.dialog.open(ModalReutilComponent, {
      width: '500px',
      data: dataDielog
    });

    return dialogRef.afterClosed().subscribe(result => {
      // this.animal = result;
      if (result) {
        if (result.id) {
          //console.log(result);

          this.updateTrace(result)

        } else {
          this.storeTracePatient(result)
        }

      }

    });
  }

  updateTrace(data) {
    const user = this.users.filter(x => x.id === data.user_id)[0]
    data.user_name = user ? `${user.name} ${user.last_name}` : data.user_name;
    this.tracePatientService.updateTrace(data).pipe(take(1)).subscribe(
      res=>{
        this.getTraces(this.patient?.id, this.pageDefault, this.perPage, this.filter, this.orden)
      }
    )
  }

  storeTracePatient(data) {
    const user = this.users.filter(x => x.id === data.user_id)[0]
    data.user_name = user ? `${user.name} ${user.last_name}` : data.user_name;
    this.tracePatientService.storeTracePatient(data).pipe(take(1)).subscribe(
      res => {
        this.getTraces(this.patient?.id, this.pageDefault, this.perPage, this.filter, this.orden)
      }
    )
  }

  setFields(elementEdit? : TracePatient) {
    // console.log(elementEdit);

    const fields = [
      { nameControl: 'id', type: 'hidden', value: elementEdit?.id, label: 'Id' },
      { nameControl: 'patient_id', type: 'hidden', value: elementEdit?.patient_id || this.patient?.id, label: 'Id' },
      { nameControl: 'user_name', type: 'hidden', value: elementEdit?.user_name , label: 'Técnico/Profesional', class:"mvd-col1--1" },
      { nameControl: 'user_id', type: 'select', value: elementEdit?.user_id || this.user?.id , options: this.userOptions, label: 'Técnico/Profesional', class:"mvd-col1--1" },
      { nameControl: 'evolution_id', type: 'select', value: elementEdit?.evolution_id, options: this.evolutionsOptions, label: 'Evolución', class:"mvd-col1--1" },
      { nameControl: 'date_limit', type: 'date', value: elementEdit?.date_limit, label: 'Fecha límite' },
      { nameControl: 'obs', type: 'textarea', value: elementEdit?.obs, label: 'Comentarios / Observaciones', class:"mvd-col1--1" },

    ]

    return fields
  }

  ngOnDestroy(): void {
    this.subscroption.unsubscribe()
  }




}
