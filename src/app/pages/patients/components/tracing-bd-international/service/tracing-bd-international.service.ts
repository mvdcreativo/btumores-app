import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { InternationalBd, Patient } from '../../../interfaces/patient';
import { PatientsService } from '../../../services/patients.service';
import { Response, ResponsePaginate } from 'src/app/shared/interfaces/response';
import { BehaviorSubject, Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class TracingBdInternationalService {

  bdInternationalsSubject$ : BehaviorSubject<InternationalBd[]> = new BehaviorSubject([])

  patient: Patient


  constructor(
    private http: HttpClient,
    private patientService: PatientsService,
    private snackBar: MatSnackBar,

  ) { }

  setPatient(patient){
    this.patient = patient
  }

  setInternationalsBds(value : InternationalBd[]){
    this.bdInternationalsSubject$.next(value)
  }

  get internationalsBds$(): Observable<InternationalBd[]>{

    if (this.bdInternationalsSubject$.value.length) {
      return this.bdInternationalsSubject$.asObservable()
    }else{
      this.getBdsInternationals().subscribe()
      return this.bdInternationalsSubject$.asObservable()
    }
  }

  storeTracingBdInternational(data){
    return this.http.post<Response>(`${environment.API}patient_international_bds`, data).pipe(
      map( v => {

        this.patient.international_bds= v.data
        this.patientService.setPatientOnEdit(this.patient);

        //snacbarr
        this.openSnackBar('Se creó correctamente','success-snack-bar')
        //////////
        return v.data
      })

    )
  }

  removeTracingBdInternational(id){
    const patient_id = {patient_id : this.patient.id};
    return this.http.post<Response>(`${environment.API}patient_international_bds/${id}`, patient_id).pipe(
      map(res => {

          this.patient.international_bds = res.data
          this.patientService.setPatientOnEdit(this.patient);
          this.openSnackBar('Se eliminó correctamente','success-snack-bar')

          return res.data;
        }
    ))
  }


  storeBdInternational(data){
    return this.http.post<Response>(`${environment.API}international_bds`, data).pipe(
      map( v => {
        let bds = this.bdInternationalsSubject$.getValue();
        if (v.data) {
          bds = [...bds, v.data]
        }
        this.setInternationalsBds(bds);
        //snacbarr
        this.openSnackBar('Se creó correctamente','success-snack-bar')
        //////////
        return v.data
      })

    )
  }

  getBdsInternationals(): Observable<InternationalBd[]>{
    return this.http.get<ResponsePaginate>(`${environment.API}international_bds`, {
      params: new HttpParams()
        .set('page', 1)
        .set('sort', 'asc')
        .set('per_page', 1000)
    }).pipe(
      map( v => {
        if (v.data.data.length) {
          this.setInternationalsBds(v.data.data);
        }else{
          this.setInternationalsBds([]);
        }
        console.log(v);

        return v.data.data
      })

    )
  }

  openSnackBar(message: string, refClass:string, action: string = '') {
    this.snackBar.open(message, action, {
      duration: 2000,
      horizontalPosition: 'center',
      verticalPosition: 'top',
      panelClass: refClass
    });
  }
}
