import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TracingBdInternationalComponent } from './tracing-bd-international.component';

describe('TracingBdInternationalComponent', () => {
  let component: TracingBdInternationalComponent;
  let fixture: ComponentFixture<TracingBdInternationalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TracingBdInternationalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TracingBdInternationalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
