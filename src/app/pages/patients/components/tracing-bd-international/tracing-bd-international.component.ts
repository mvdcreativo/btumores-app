import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDatepickerInputEvent } from '@angular/material/datepicker';
import moment from 'moment';
import { Observable, Subscription } from 'rxjs';
import { take } from 'rxjs/operators';
import { ItemOption } from 'src/app/shared/components/input-select-add-item/Interfaces/item-option';
import { InternationalBd, Patient } from '../../interfaces/patient';
import { TracingBdInternationalService } from './service/tracing-bd-international.service';

@Component({
  selector: 'mvd-tracing-bd-international',
  templateUrl: './tracing-bd-international.component.html',
  styleUrls: ['./tracing-bd-international.component.scss']
})
export class TracingBdInternationalComponent implements OnInit {

  @Input() patient: Patient;

  bds:InternationalBd[];
  options: ItemOption[] = [];
  form: FormGroup
  subscriptions: Subscription= new Subscription();
  valueSelect: string;

  constructor(
    private tracingBdInterService : TracingBdInternationalService,
    private fb: FormBuilder
  ) {
  }

  ngOnInit(): void {
    this.formatOptionsSelect()
    this.tracingBdInterService.setPatient(this.patient)
    this.createForm()
  }

  createForm(){
    this.form = this.fb.group({
      patient_id:[this.patient?.id],
      international_bd_id: [],
      date: [],
      obs: []
    })
  }

  submitTrace(){
    this.subscriptions.add (
      this.tracingBdInterService.storeTracingBdInternational(this.form.value).subscribe()
    )
    this.form.reset()
    this.valueSelect = ''

    this.form.patchValue({
      patient_id: this.patient.id,
    })
  }

  removeTrace(id){
    this.tracingBdInterService.removeTracingBdInternational(id).pipe(take(1)).subscribe()

  }

  ngOnDestroy(): void {
    this.subscriptions.unsubscribe();
  }


  formatDate(e : MatDatepickerInputEvent<Date>, campo){
    let brt = e.value;
    this.form.get(campo).setValue(moment(brt).format('YYYY-MM-DD HH:mm:ss'))
  }

  formatOptionsSelect(){
    this.subscriptions.add(
      this.tracingBdInterService.internationalsBds$.subscribe(items=>{
        this.bds = items,
        this.options = items.map(v=> ( {value: v.id, text: v.name,} ) )
      })
    )
  }

  addBd(value){
    this.subscriptions.add(this.tracingBdInterService.storeBdInternational(value).subscribe())
  }

  changeBd(value){
    this.form.get('international_bd_id').setValue(value)
    this.valueSelect = value

  }
}
