import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TracingOncofertilidadComponent } from './tracing-oncofertilidad.component';

describe('TracingOncofertilidadComponent', () => {
  let component: TracingOncofertilidadComponent;
  let fixture: ComponentFixture<TracingOncofertilidadComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TracingOncofertilidadComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TracingOncofertilidadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
