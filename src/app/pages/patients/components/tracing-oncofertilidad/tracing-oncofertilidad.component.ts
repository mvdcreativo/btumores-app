import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { MatDatepickerInputEvent } from '@angular/material/datepicker';
import moment from 'moment';
import { Subscription, Observable } from 'rxjs';
import { debounceTime } from 'rxjs/operators';
import { Patient } from '../../interfaces/patient';
import { TracingPatient } from '../../interfaces/tracePatient';
import { TracingService } from '../../services/tracing.service';

@Component({
  selector: 'mvd-tracing-oncofertilidad',
  templateUrl: './tracing-oncofertilidad.component.html',
  styleUrls: ['./tracing-oncofertilidad.component.scss']
})
export class TracingOncofertilidadComponent implements OnInit {
  @Output() changeTracing : EventEmitter<any> = new EventEmitter()
  @Input() patient: Patient


  form: FormGroup;
  subscriptions: Subscription = new Subscription();
  dataSelects: Observable<any>;

  constructor(
    private fb:FormBuilder,
    private tracingService: TracingService
  ) {


  }

  ngOnInit(): void {
    this.createForm()
    this.loadData()
  }



  createForm(){
    this.form = this.fb.group({
      id: [],
      patient_id:[this.patient.id],
      onc: [false],
      onc_date: [],
      onc_obs: [],

    })
    this.subscriptions.add(
      this.form.valueChanges.pipe(debounceTime(500)).subscribe(value => this.changeTracing.emit(value))
    )
  }


  private updateForm(tracing: TracingPatient){
    if (tracing && this.form) {
      this.form.patchValue({
        patient_id: this.patient.id,
        id: tracing?.id,
        onc: tracing?.onc,
        onc_date: tracing?.onc_date,
        onc_obs: tracing?.onc_obs,

      })

      this.changeTracing.emit(this.form.value)

    }

  }

  formatDate(e : MatDatepickerInputEvent<Date>, campo){
    let brt = e.value;
    this.form.get(campo).setValue(moment(brt).format('YYYY-MM-DD HH:mm:ss'))
  }


  loadData(){
    this.subscriptions.add(this.tracingService.tracePatientOnEdit(this.patient.id).subscribe(tracing => this.updateForm(tracing)))
  }

  ngOnDestroy(): void {
    this.subscriptions.unsubscribe();
  }
}
