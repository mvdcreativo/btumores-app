import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { Patient, Questionary } from '../../../interfaces/patient';
import { PatientsService } from '../../../services/patients.service';
import { Response, ResponsePaginate } from 'src/app/shared/interfaces/response';
import { BehaviorSubject, Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class TracingQuestionaryService {

  questionariesSubject$ : BehaviorSubject<Questionary[]> = new BehaviorSubject([])

  patient: Patient


  constructor(
    private http: HttpClient,
    private patientService: PatientsService,
    private snackBar: MatSnackBar,

  ) { }

  setPatient(patient){
    this.patient = patient
  }

  setQuestionaries(value : Questionary[]){
    this.questionariesSubject$.next(value)
  }

  get questionaries$(): Observable<Questionary[]>{

    if (this.questionariesSubject$.value.length) {
      return this.questionariesSubject$.asObservable()
    }else{
      this.getQuestionaries().subscribe()
      return this.questionariesSubject$.asObservable()
    }
  }

  storeTracingQuestionary(data){
    return this.http.post<Response>(`${environment.API}patient_questionary`, data).pipe(
      map( v => {

        this.patient.questionnaires= v.data
        this.patientService.setPatientOnEdit(this.patient);

        //snacbarr
        this.openSnackBar('Se creó correctamente','success-snack-bar')
        //////////
        return v.data
      })

    )
  }

  removeTracingQuestionary(id){
    const patient_id = {patient_id : this.patient.id};
    return this.http.post<Response>(`${environment.API}patient_questionary/${id}`, patient_id).pipe(
      map(res => {

          this.patient.questionnaires = res.data
          this.patientService.setPatientOnEdit(this.patient);
          this.openSnackBar('Se eliminó correctamente','success-snack-bar')

          return res.data;
        }
    ))
  }


  storeQuestionary(data){
    return this.http.post<Response>(`${environment.API}questionnaires`, data).pipe(
      map( v => {
        let bds = this.questionariesSubject$.getValue();
        if (v.data) {
          bds = [...bds, v.data]
        }
        this.setQuestionaries(bds);
        //snacbarr
        this.openSnackBar('Se creó correctamente','success-snack-bar')
        //////////
        return v.data
      })

    )
  }

  getQuestionaries(): Observable<Questionary[]>{
    return this.http.get<ResponsePaginate>(`${environment.API}questionnaires`, {
      params: new HttpParams()
        .set('page', 1)
        .set('sort', 'asc')
        .set('per_page', 1000)
    }).pipe(
      map( v => {
        if (v.data.data.length) {
          this.setQuestionaries(v.data.data);
        }else{
          this.setQuestionaries([]);
        }
        console.log(v);

        return v.data.data
      })

    )
  }

  openSnackBar(message: string, refClass:string, action: string = '') {
    this.snackBar.open(message, action, {
      duration: 2000,
      horizontalPosition: 'center',
      verticalPosition: 'top',
      panelClass: refClass
    });
  }
}
