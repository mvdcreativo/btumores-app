import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TracingQuestionaryComponent } from './tracing-questionary.component';

describe('TracingQuestionaryComponent', () => {
  let component: TracingQuestionaryComponent;
  let fixture: ComponentFixture<TracingQuestionaryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TracingQuestionaryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TracingQuestionaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
