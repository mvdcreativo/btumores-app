import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDatepickerInputEvent } from '@angular/material/datepicker';
import moment from 'moment';
import { Subscription } from 'rxjs';
import { take } from 'rxjs/operators';
import { ItemOption } from 'src/app/shared/components/input-select-add-item/Interfaces/item-option';
import { Patient, Questionary } from '../../interfaces/patient';
import { TracingQuestionaryService } from './service/tracing-questionary.service';

@Component({
  selector: 'mvd-tracing-questionary',
  templateUrl: './tracing-questionary.component.html',
  styleUrls: ['./tracing-questionary.component.scss']
})
export class TracingQuestionaryComponent implements OnInit {

  @Input() patient: Patient;

  questionaries:Questionary[];
  options: ItemOption[] = [];
  form: FormGroup
  subscriptions: Subscription [] = [];
  valueSelect: any = null;

  constructor(
    private tracingQuestionariesService : TracingQuestionaryService,
    private fb: FormBuilder
  ) {
  }

  ngOnInit(): void {
    this.formatOptionsSelect()
    this.tracingQuestionariesService.setPatient(this.patient)
    this.createForm()
  }

  createForm(){
    this.form = this.fb.group({
      patient_id:[this.patient?.id],
      questionnaire_id: [],
      date: [],
      obs: []
    })
  }

  submitTrace(){
    this.subscriptions.push (
      this.tracingQuestionariesService.storeTracingQuestionary(this.form.value).pipe(take(1)).subscribe()
    )
    this.form.reset()
    this.valueSelect = '';
    this.form.patchValue({
      patient_id: this.patient.id,
    })
  }

  removeTrace(id){
    this.tracingQuestionariesService.removeTracingQuestionary(id).pipe(take(1)).subscribe()

  }

  ngOnDestroy(): void {
    this.subscriptions.map(s=>s.unsubscribe())
  }


  formatDate(e : MatDatepickerInputEvent<Date>, campo){
    let brt = e.value;
    this.form.get(campo).setValue(moment(brt).format('YYYY-MM-DD HH:mm:ss'))
  }

  formatOptionsSelect(){
    this.tracingQuestionariesService.questionaries$.subscribe(items=>{
      this.questionaries = items,
      this.options = items.map(v=> ( {value: v.id, text: v.name,} ) )
    })
  }

  add(value){
    this.tracingQuestionariesService.storeQuestionary(value).subscribe()
  }

  change(value){
    this.form.get('questionnaire_id').setValue(value);
    this.valueSelect = value
  }
}
