import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TracingQuimioprofilaxisComponent } from './tracing-quimioprofilaxis.component';

describe('TracingQuimioprofilaxisComponent', () => {
  let component: TracingQuimioprofilaxisComponent;
  let fixture: ComponentFixture<TracingQuimioprofilaxisComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TracingQuimioprofilaxisComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TracingQuimioprofilaxisComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
