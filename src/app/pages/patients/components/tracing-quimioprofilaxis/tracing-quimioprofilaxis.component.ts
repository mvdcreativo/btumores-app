import { NgIf } from '@angular/common';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { MatDatepickerInputEvent } from '@angular/material/datepicker';
import moment from 'moment';
import { Subscription, Observable } from 'rxjs';
import { debounceTime } from 'rxjs/operators';
import { Patient } from '../../interfaces/patient';
import { TracingPatient } from '../../interfaces/tracePatient';
import { TracingService } from '../../services/tracing.service';

@Component({
  selector: 'mvd-tracing-quimioprofilaxis',
  templateUrl: './tracing-quimioprofilaxis.component.html',
  styleUrls: ['./tracing-quimioprofilaxis.component.scss']
})
export class TracingQuimioprofilaxisComponent implements OnInit {

  @Output() changeTracing : EventEmitter<any> = new EventEmitter()
  @Input() patient: Patient


  form: FormGroup;
  subscriptions: Subscription= new Subscription();
  dataSelects: Observable<any>;

  constructor(
    private fb:FormBuilder,
    private tracingService: TracingService
  ) {


  }

  ngOnInit(): void {
    this.loadData()
    this.createForm()
  }
  ngOnChanges(): void {

  }

  createForm(){
    this.form = this.fb.group({
      id: [],
      patient_id:[this.patient.id],
      qui: [false],
      qui_date: [],
      qui_obs: [],

    })
    this.subscriptions.add(this.form.valueChanges.pipe(debounceTime(500)).subscribe(value => this.changeTracing.emit(value)))
  }

  private updateForm(tracing: TracingPatient){
    if (tracing && this.form) {
      this.form.patchValue({
        patient_id: this.patient.id,
        id: tracing?.id,
        qui: tracing?.qui,
        qui_date: tracing?.qui_date,
        qui_obs: tracing?.qui_obs,

      })

      this.changeTracing.emit(this.form.value)

    }

  }

  formatDate(e : MatDatepickerInputEvent<Date>, campo){
    let brt = e.value;
    this.form.get(campo).setValue(moment(brt).format('YYYY-MM-DD HH:mm:ss'))
  }


  loadData(){
    this.subscriptions.add(this.tracingService.tracePatientOnEdit(this.patient.id).subscribe(tracing => this.updateForm(tracing)))
  }

  ngOnDestroy(): void {
    this.subscriptions.unsubscribe();
  }
}
