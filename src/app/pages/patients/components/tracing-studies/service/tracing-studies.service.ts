import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { Study, Patient } from '../../../interfaces/patient';
import { PatientsService } from '../../../services/patients.service';
import { Response, ResponsePaginate } from 'src/app/shared/interfaces/response';
import { BehaviorSubject, Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class TracingStudiesService {

  studiesSubject$ : BehaviorSubject<Study[]> = new BehaviorSubject([])

  patient: Patient


  constructor(
    private http: HttpClient,
    private patientService: PatientsService,
    private snackBar: MatSnackBar,

  ) { }

  setPatient(patient){
    this.patient = patient
  }

  setStudies(value : Study[]){
    this.studiesSubject$.next(value)
  }

  get studies$(): Observable<Study[]>{

    if (this.studiesSubject$.value.length) {
      return this.studiesSubject$.asObservable()
    }else{
      this.getStudies().subscribe()
      return this.studiesSubject$.asObservable()
    }
  }

  storeTracingStudy(data){
    return this.http.post<Response>(`${environment.API}patient_study`, data).pipe(
      map( v => {

        this.patient.studies= v.data
        this.patientService.setPatientOnEdit(this.patient);

        //snacbarr
        this.openSnackBar('Se creó correctamente','success-snack-bar')
        //////////
        return v.data
      })

    )
  }

  removeTracingStudy(id){
    const patient_id = {patient_id : this.patient.id};
    return this.http.post<Response>(`${environment.API}patient_study/${id}`, patient_id).pipe(
      map(res => {

          this.patient.studies = res.data
          this.patientService.setPatientOnEdit(this.patient);
          this.openSnackBar('Se eliminó correctamente','success-snack-bar')

          return res.data;
        }
    ))
  }


  storeStudy(data){
    return this.http.post<Response>(`${environment.API}studies`, data).pipe(
      map( v => {
        let bds = this.studiesSubject$.getValue();
        if (v.data) {
          bds = [...bds, v.data]
        }
        this.setStudies(bds);
        //snacbarr
        this.openSnackBar('Se creó correctamente','success-snack-bar')
        //////////
        return v.data
      })

    )
  }

  getStudies(): Observable<Study[]>{
    return this.http.get<ResponsePaginate>(`${environment.API}studies`, {
      params: new HttpParams()
        .set('page', 1)
        .set('sort', 'asc')
        .set('per_page', 1000)
    }).pipe(
      map( v => {
        if (v.data.data.length) {
          this.setStudies(v.data.data);
        }else{
          this.setStudies([]);
        }
        console.log(v);

        return v.data.data
      })

    )
  }

  openSnackBar(message: string, refClass:string, action: string = '') {
    this.snackBar.open(message, action, {
      duration: 2000,
      horizontalPosition: 'center',
      verticalPosition: 'top',
      panelClass: refClass
    });
  }
}
