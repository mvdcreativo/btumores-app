import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TracingStudiesComponent } from './tracing-studies.component';

describe('TracingStudiesComponent', () => {
  let component: TracingStudiesComponent;
  let fixture: ComponentFixture<TracingStudiesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TracingStudiesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TracingStudiesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
