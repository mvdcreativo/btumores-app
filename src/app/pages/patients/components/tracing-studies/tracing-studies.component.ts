import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDatepickerInputEvent } from '@angular/material/datepicker';
import moment from 'moment';
import { Observable, Subscription } from 'rxjs';
import { take } from 'rxjs/operators';
import { ItemOption } from 'src/app/shared/components/input-select-add-item/Interfaces/item-option';
import { PatientStudy, Patient, Study } from '../../interfaces/patient';
import { TracingStudiesService } from './service/tracing-studies.service';

@Component({
  selector: 'mvd-tracing-studies',
  templateUrl: './tracing-studies.component.html',
  styleUrls: ['./tracing-studies.component.scss']
})
export class TracingStudiesComponent implements OnInit {

  @Input() patient: Patient;

  studies:Study[];
  options: ItemOption[] = [];
  form: FormGroup
  subscriptions: Subscription= new Subscription();

  valueSelect: any;

  constructor(
    private tracingStudiesService : TracingStudiesService,
    private fb: FormBuilder
  ) {
  }

  ngOnInit(): void {
    this.formatOptionsSelect()
    this.tracingStudiesService.setPatient(this.patient)
    this.createForm()
  }

  createForm(){
    this.form = this.fb.group({
      patient_id:[this.patient?.id],
      study_id: [],
      date: [],
      obs: []
    })
  }

  submitTrace(){
    this.subscriptions.add (
      this.tracingStudiesService.storeTracingStudy(this.form.value).subscribe()
    )
    this.form.reset()
    this.valueSelect = ''
    this.form.patchValue({
      patient_id: this.patient.id,
    })
  }

  removeTrace(id){
    this.subscriptions.add(this.tracingStudiesService.removeTracingStudy(id).subscribe())

  }

  ngOnDestroy(): void {
    this.subscriptions.unsubscribe();
  }


  formatDate(e : MatDatepickerInputEvent<Date>, campo){
    let brt = e.value;
    this.form.get(campo).setValue(moment(brt).format('YYYY-MM-DD HH:mm:ss'))
  }

  formatOptionsSelect(){
    this.subscriptions.add (
      this.tracingStudiesService.studies$.subscribe(items=>{
        this.studies = items,
        this.options = items.map(v=> ( {value: v.id, text: v.name,} ) )
      })
    )
  }

  add(value){
    this.subscriptions.add(this.tracingStudiesService.storeStudy(value).subscribe())
  }

  change(value){
    this.form.get('study_id').setValue(value)
    this.valueSelect = value
  }
}
