import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TracingSurgeryComponent } from './tracing-surgery.component';

describe('TracingSurgeryComponent', () => {
  let component: TracingSurgeryComponent;
  let fixture: ComponentFixture<TracingSurgeryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TracingSurgeryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TracingSurgeryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
