import { Component, EventEmitter, Input, OnInit, Output, SimpleChanges } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDatepickerInputEvent } from '@angular/material/datepicker';
import moment from 'moment';
import { Observable } from 'rxjs';
import { Subscription } from 'rxjs/internal/Subscription';
import { debounceTime } from 'rxjs/operators';
import { Patient } from '../../interfaces/patient';
import { TracingPatient } from '../../interfaces/tracePatient';
import { TracingService } from '../../services/tracing.service';


@Component({
  selector: 'mvd-tracing',
  templateUrl: './tracing-surgery.component.html',
  styleUrls: ['./tracing-surgery.component.scss']
})
export class TracingSurgeryComponent implements OnInit {


  @Output() changeTracing : EventEmitter<any> = new EventEmitter()
  @Input() patient: Patient


  form: FormGroup;
  subscriptions: Subscription = new Subscription();
  dataSelects: Observable<any>;

  constructor(
    private fb:FormBuilder,
    private tracingService: TracingService
  ) {


  }

  ngOnInit(): void {

  }
  ngOnChanges(changes: SimpleChanges): void {
  }


  ngAfterViewInit(): void {
    this.loadData()
    this.createForm()

  }

  createForm(){
    this.form = this.fb.group({
      patient_id:[this.patient.id],
      id: [],
      mas_uni: [false],
      mas_uni_date: [],
      mas_uni_obs: [],

      mas_bi: [false],
      mas_bi_date: [],
      mas_bi_obs: [],

      oof: [false],
      oof_date: [],
      oof_obs: [],

      hist: [false],
      hist_date: [],
      hist_obs: [],

      gast: [false],
      gast_date: [],
      gast_obs: [],

    })
    this.subscriptions.add(
      this.form.valueChanges.pipe(debounceTime(500)).subscribe(value => this.changeTracing.emit(value))
    )
  }

  private updateForm(tracing: TracingPatient){
    if (tracing && this.form) {
      this.form.patchValue({
        patient_id: this.patient.id,
        id: tracing?.id,
        mas_uni: tracing?.mas_uni,
        mas_uni_date: tracing?.mas_uni_date,
        mas_uni_obs: tracing?.mas_uni_obs,

        mas_bi: tracing?.mas_bi,
        mas_bi_date: tracing?.mas_bi_date,
        mas_bi_obs: tracing?.mas_bi_obs,

        oof: tracing?.oof,
        oof_date: tracing?.oof_date,
        oof_obs: tracing?.oof_obs,

        hist: tracing?.hist,
        hist_date: tracing?.hist_date,
        hist_obs: tracing?.hist_obs,

        gast: tracing?.gast,
        gast_date: tracing?.gast_date,
        gast_obs: tracing?.gast_obs,

      })

      this.changeTracing.emit(this.form.value)
    }
  }

  formatDate(e : MatDatepickerInputEvent<Date>, campo){
    let brt = e.value;
    this.form.get(campo).setValue(moment(brt).format('YYYY-MM-DD HH:mm:ss'))
  }


  loadData(){
    this.subscriptions.add(
      this.tracingService.tracePatientOnEdit(this.patient.id).subscribe(tracing => this.updateForm(tracing))
    )
  }

  ngOnDestroy(): void {
    this.subscriptions.unsubscribe()
  }
}


