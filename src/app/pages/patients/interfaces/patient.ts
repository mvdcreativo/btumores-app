import { City, Country } from "src/app/shared/interfaces/ubication";
import { Topography } from "../../topographies/interfaces/topography";
import { Attached } from "../components/attached-files/attached";

export interface Patient {
  id?: number;
  code:string;
  birth:string;
  type_doc:string;
  n_doc:string;
  name:string;
  last_name:string;
  phone?:string;
  address?:string;
  email?:string;
  ashkenasi?:boolean;
  gender?:string;
  type_patient?:string;
  country_id?:number;
  state_id?:number;
  city_id?:number;
  city?: City;
  nationality_id?:number;
  nationality?: Country;
  medical_institution_id:number;
  surgery_institution_id:number;
  registroH:string;
  doctor_id?:number;
  breed_id?:number;
  obs?:string;
  proyect_investigation_id?:number;
  patient_data?: DataPatient;
  documents?: Attached[];
  cancer_patient_antecedents: CancerPatientAntecedent[];
  new_tumors: NewTumor[]
  patient_scores: PatientScores[];
  international_bds: PatientInternationalBd[];
  studies: PatientStudy[];
  questionnaires: PatientQuestionary[];
  dead?:boolean;
  date_dead?:string;
  obs_dead?:string;
}

export interface PatientStudy {
  id:number;
  name: number;
  pivot: {
    id:number;
    study_id: number;
    patient_id: number;
    date: string;
    obs: string;
  }
}

export interface PatientQuestionary {
  id:number;
  name: number;
  pivot: {
    id:number;
    questionnaire_id: number;
    patient_id: number;
    date: string;
    obs: string;
  }
}


export interface PatientInternationalBd {
  id:number;
  name: number;
  pivot: {
    id:number;
    international_bd_id: number;
    patient_id: number;
    date: string;
    obs: string;
  }
}

export interface InternationalBd{
  id:number;
  name:string;
  date:string;
  obs:string;
}

export interface Study{
  id:number;
  name:string;
  date:string;
  obs:string;
}

export interface Questionary{
  id:number;
  name:string;
  date:string;
  obs:string;
}
export interface NewTumor {
  id:number;
  patient_id: number;
  topography_id: number;
  topography: Topography;
  date: string;
  obs: string;
}

export interface PatientScores {
  patient_id: number;
  id: number;
  score: string;
  percentage: number;
}
export interface CancerPatientAntecedent{
  id?:number;
  patient_id: number;
  topography_id: number;
  topographies: Topography[];
  topography?: Topography
  edad: number;
}

export interface DataPatient{
    id:number;
    patient_id:number;
    date_surgery:string;
    topography_id:number;
    type_surgery_id:number;
    imc:boolean;
    imc_talla:any;
    imc_peso:any;
    imc_imc:any;
    fumador:boolean;
    fumador_activo:boolean;
    fumador_cant:string;
    fumador_periodo?:string;
    alcoholista:boolean;
    alcoholista_activo:boolean;
    alcoholista_cant:string;
    alcoholista_periodo:string;
    drogas:boolean;
    drogas_activo:boolean;
    drogas_tipo:string;
    drogas_periodo:string;
    rt:boolean;
    rt_donde:string;
    rt_date:string;
    anticonceptivos:boolean;
    anticonceptivos_periodo:any;
    amamantar:boolean;
    amamantar_periodo:any;
    hormonas:boolean;
    hormonas_periodo:any;
    tipo_trh:string;
    ambientales:boolean;
    ambientales_cuales:string;
    factor_r_especifico:string;
    mamografia:boolean;
    mamografia_frecuencia:any;
    mamografia_otros:string;
    mamografia_date_ultima:string;
    pap:boolean;
    pap_frecuencia:any;
    pap_otros:string;
    pap_date_ultima:string;
    fecatest:boolean;
    fecatest_frecuencia:any;
    fecatest_otros:string;
    fecatest_date_ultima:string;
    fibrocolonoscopia:boolean;
    fibrocolonoscopia_frecuencia:any;
    fibrocolonoscopia_otros:string;
    fibrocolonoscopia_date_ultima:string;
    fibrogastroscopia:boolean;
    fibrogastroscopia_frecuencia:any;
    fibrogastroscopia_otros:string;
    fibrogastroscopia_date_ultima:string;
    psa:boolean;
    psa_frecuencia:any;
    psa_otros:string;
    psa_date_ultima:string;
    psa_result:string;
    rm_mamaria:boolean;
    rm_mamaria_frecuencia:any;
    rm_mamaria_otros:string;
    rm_mamaria_date_ultima:string;
    edad_menarca:number;
    edad_primer_emb:number;
    menopausia_edad:string;
    menopausia_quirurgica:boolean;
    antecedente:boolean;
    antecedente_directo:boolean;
    antecedente_directo_tipo: string;
    antecedente_indirectos:boolean;
    antecedente_indirectos_tipo:any;
    anterior:boolean;
    anterior_topography_id:number;
    anterior_edad:string;
}
