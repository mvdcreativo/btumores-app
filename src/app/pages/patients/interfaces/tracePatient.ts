import { Evolution } from "../../evolutions/interfaces/evolution";


export interface TracePatient {
  id: number;
  patient_id:number;
  evolution_id: number;
  user_name:string;
  obs:string;
  date_limit:any;
  user_id:number;
  evolution?: Evolution
}

export interface TracingPatient {
  id: number;
  patient_id:number;
  mas_uni: boolean,
  mas_uni_date: string,
  mas_uni_obs: string,
  mas_bi: boolean,
  mas_bi_date: string,
  mas_bi_obs: string,
  oof: boolean,
  oof_date: string,
  oof_obs: string,
  hist: boolean,
  hist_date: string,
  hist_obs: string,
  gast: boolean,
  gast_date: string,
  gast_obs: string,
  qui: boolean,
  qui_date: string,
  qui_obs: string,
  onc: boolean,
  onc_date: string,
  onc_obs: string,
}
