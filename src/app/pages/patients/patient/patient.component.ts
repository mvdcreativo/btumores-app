import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import { take } from 'rxjs/operators';
import { ModalConfirmNoChangesComponent } from 'src/app/shared/components/modals/modal-confirm-no-changes/modal-confirm-no-changes.component';
import { FamilyService } from '../../families/services/family.service';
import { DataPatient, Patient } from '../interfaces/patient';
import { TracingPatient } from '../interfaces/tracePatient';
import { PatientsService } from '../services/patients.service';
import { TracePatientService } from '../services/trace-patient.service';
import { TracingService } from '../services/tracing.service';

@Component({
  selector: 'mvd-patient',
  templateUrl: './patient.component.html',
  styleUrls: ['./patient.component.scss'],
})
export class PatientComponent implements OnInit, OnDestroy {
  dataFormIdentification: FormGroup;
  dataBasicForm: FormGroup;
  patientEdit: Observable<Patient>;
  tipoPaciente: any;
  countChanges: number = 0;
  urlReturn: any;
  subscriptions: Subscription[] = [];
  consentimiento: boolean;
  statusEvolution: any;
  patientId: number;
  tracingData: TracingPatient;
  hidden = false;
  patient: Patient;

  toggleBadgeVisibility() {
    this.hidden = !this.hidden;
  }
  constructor(
    private patientService: PatientsService,
    private activatedRoute: ActivatedRoute,
    private tracesPatientService: TracePatientService,
    private tracingService: TracingService,
    private familyService: FamilyService,
    private router: Router,
    public dialog: MatDialog
  ) {
    this.subscriptions.push(
      this.patientService.patientOnEdit.subscribe((res) => {

        if (res?.documents?.length >= 1) {
          const documentsConsentimiento = res.documents.filter(
            (v) => v.consentimiento
          );
          this.consentimiento =
            documentsConsentimiento.length >= 1 ? true : false;
        }else{
          this.consentimiento = false
        }
        if (res) {
          this.patient = res;
          this.patientId = res.id;
          this.subscriptions.push(
            this.tracesPatientService.resultItems$.subscribe((res) => {
              this.statusEvolution = res?.data?.data[0]?.evolution?.name;
            }),
            this.tracesPatientService
              .getTraces(1, 1, '', 'desc', this.patientId)
              .subscribe()
          );
        }
      })
    );
  }

  ngOnInit(): void {
    this.patientEdit = this.patientService.patientOnEdit;
    this.activatedRoute.paramMap.subscribe((params: Params) => {
      if (params.params.id) {
        this.subscriptions.push(
          this.patientService
            .getPatient(params.params.id)
            .subscribe((res) => {})
        );
      }

      if (!params.params.id) {
        this.patientService.setPatientOnEdit(null);
        this.familyService.setFamilyOnEdit(null)

      }
    });
  }

  ngOnChanges(): void {

  }

  back() {
    this.countChanges >= 2
      ? this.confirmBack()
      : this.router.navigate(['/pacientes']);
  }
  confirmBack() {
    const dialogRef = this.dialog.open(ModalConfirmNoChangesComponent, {
      width: '250px',
      data: {},
    });

    return dialogRef.afterClosed().subscribe((result) => {
      // this.animal = result;
      if (result) {
        if (result.value) {
          this.router.navigate(['/pacientes']);
        }
      }
    });
  }

  onSubmit() {
    if (this.dataFormIdentification?.value) {
      let patient = { ...this.dataFormIdentification.value };

      if (this.dataBasicForm) {
        const dataBasic = this.dataBasicForm.value;
        Array.isArray(dataBasic?.antecedente_directo_tipo)
          ? (dataBasic.antecedente_directo_tipo =
              dataBasic?.antecedente_directo_tipo?.join(','))
          : (dataBasic.antecedente_directo_tipo = '');
        patient.patient_data = dataBasic;
      }

      this.subscriptions.push(
        this.patientEdit.subscribe((patientOnEdit) => {
          if (!patientOnEdit) {
            this.dataFormIdentification.valid
              ? this.patientService.storePatient(patient).subscribe((res) => {
                if (this.tracingData?.patient_id) {
                  this.tracingService.storeTracingPatient(this.tracingData)
                }
                this.router.navigate(['/pacientes/paciente', res.id]);
                  this.countChanges = 0;
                })
              : this.dataFormIdentification.markAllAsTouched();
          } else {
            this.dataFormIdentification.valid
              ? this.patientService
                  .updatePatient(patient)
                  .subscribe((res) => {
                    if (this.tracingData?.patient_id) {
                      if(this.tracingData?.id){
                        this.tracingService.updateTracingPatient(this.tracingData)
                      }else{
                        this.tracingService.storeTracingPatient(this.tracingData)
                      }
                    }
                    this.countChanges = 0
                  })
              : this.dataFormIdentification.markAllAsTouched();
          }
        })
      );
    }

  }

  changeTracing(value){

    this.tracingData= { ...this.tracingData, ...value }

  }

  dataIdentification(e) {
    this.dataFormIdentification = e;
    this.countChanges++;
  }

  dataBasic(e) {
    this.dataBasicForm = e;
    this.countChanges++;
  }

  ngOnDestroy() {
    this.subscriptions.map((v) => v.unsubscribe());
  }
}
