import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { Router } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import { map, take } from 'rxjs/operators';
import { Column } from 'src/app/shared/components/data-table/interfaces/table';
import { ResponsePaginate } from 'src/app/shared/interfaces/response';
import { Patient } from "./interfaces/patient";
import { PatientsService } from "./services/patients.service";

@Component({
  selector: 'mvd-patients',
  templateUrl: './patients.component.html',
  styleUrls: ['./patients.component.scss']
})
export class PatientsComponent implements OnInit {
  // displayedColumns: string[] = ['code', 'title', 'user_owner.name', 'address', 'neighborhood.name'];

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
    ////COLUMNAS TABLA
    public columns: Column[] = [
      { title: 'Código', col: 'code' },
      { title: 'Nombre', col: 'name' },
      { title: 'Tipo de Paciente ', col: 'type_patient' },
      { title: 'Doc. de Identidad', col: 'n_doc' },
    ]

  dataSource:Observable<any[]>;
  patients: Patient[];

  ////paginator
  totalResut: Observable<number>;
  pageDefault = 1;
  perPage: number = 10;
  orden: string = 'desc';
  filter: string = '';
  result: Observable<ResponsePaginate>;
  subscroption: Subscription;


  paginatorChange(e: PageEvent) {
   //console.log(e);
    this.getPatients(e.pageIndex + 1, e.pageSize)

  }
  /////////////


  constructor(
    private patientService: PatientsService,
    private router: Router
  ) {
    this.result = this.patientService.resultItems$

  }
  
  
  ngOnInit(): void {
    
    this.getPatients(this.pageDefault, this.perPage, this.filter, this.orden)

  }





  getPatients(currentPage?, perPage?, filter?, sort?) {
    this.subscroption = this.patientService.getPatients(currentPage, perPage, filter, sort).subscribe(next => this.loadData());
  }

  loadData() {

    this.dataSource = this.result.pipe(map(v => {
      const dataTable = v.data.data.map(x => {
        return {
          id: x.id,
          code: x.code,
          name: `${x.name} ${x.last_name}`,
          type_patient: x.type_patient,
          n_doc: x.n_doc,
        }
      })
      return dataTable;
    }))

    this.totalResut = this.result.pipe(map(v => v.data.total))
  }

  search(filter){
    this.getPatients(this.pageDefault, this.perPage, filter, this.orden)
  }

  deleteItem(id):Observable<any>{
    return this.patientService.deletePatient(id)
  }

  itemAction(event){
   //console.log(event);
    
    if(event.action === "delete"){
      this.deleteItem(event.element.id).pipe(take(1)).subscribe( )
    }
  
    if(event.action === "edit"){
      this.router.navigate(['/pacientes/paciente', event.element.id])    }
    
    }



    ngOnDestroy(): void {
      //Called once, before the instance is destroyed.
      //Add 'implements OnDestroy' to the class.
      this.subscroption.unsubscribe()
    }
}
