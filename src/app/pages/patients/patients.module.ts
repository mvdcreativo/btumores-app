import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PatientsRoutingModule } from './patients-routing.module';
import { PatientsComponent } from './patients.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { PatientComponent } from './patient/patient.component';
import { BasicDataComponent } from './components/basic-data/basic-data.component';
import { AttachedFilesComponent } from './components/attached-files/attached-files.component';
import { PatientIdentificationComponent } from './components/patient-identification/patient-identification.component';
import { SamplesModule } from '../samples/samples.module';
import { PatientSamplesComponent } from './components/patient-samples/patient-samples.component';
import { PatientFamilyComponent } from './components/patient-family/patient-family.component';
import { FamiliesModule } from '../families/families.module';
import { TracesComponent } from './components/traces/traces.component';
import { FamilyComponent } from './components/family/family.component';
import { TracingSurgeryComponent } from './components/tracing-surgeries/tracing-surgery.component';
import { NewTumorComponent } from './components/new-tumor/new-tumor.component';
import { ScorePatientComponent } from './components/score-patient/score-patient.component';
import { TracingQuimioprofilaxisComponent } from './components/tracing-quimioprofilaxis/tracing-quimioprofilaxis.component';
import { TracingOncofertilidadComponent } from './components/tracing-oncofertilidad/tracing-oncofertilidad.component';
import { TracingBdInternationalComponent } from './components/tracing-bd-international/tracing-bd-international.component';
import { TracingStudiesComponent } from './components/tracing-studies/tracing-studies.component';
import { TracingQuestionaryComponent } from './components/tracing-questionary/tracing-questionary.component';
import { StudyRequetsModule } from '../study-requests/study-request.module';


@NgModule({
  declarations: [
    PatientsComponent,
    PatientComponent,
    BasicDataComponent,
    AttachedFilesComponent,
    PatientIdentificationComponent,
    PatientSamplesComponent,
    PatientFamilyComponent,
    TracesComponent,
    FamilyComponent,
    TracingSurgeryComponent,
    NewTumorComponent,
    ScorePatientComponent,
    TracingQuimioprofilaxisComponent,
    TracingOncofertilidadComponent,
    TracingBdInternationalComponent,
    TracingStudiesComponent,
    TracingQuestionaryComponent
  ],
  imports: [
    CommonModule,
    PatientsRoutingModule,
    SamplesModule,
    FamiliesModule,
    SharedModule,
    StudyRequetsModule
  ]
})
export class PatientsModule { }
