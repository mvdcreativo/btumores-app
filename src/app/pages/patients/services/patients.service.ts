import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { ResponsePaginate, Response } from 'src/app/shared/interfaces/response';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from "src/environments/environment";
import { CancerPatientAntecedent, Patient } from '../interfaces/patient';
import { MedicalInstitution } from '../../medical-institutions/interfaces/medicalInstitution';
import { map, take, catchError, mergeMap, concatMap } from 'rxjs/operators';
import { MatSnackBar } from '@angular/material/snack-bar';
import { UbicationService } from 'src/app/shared/services/ubications/ubication.service';
import { MedicalInstitutionsService } from '../../medical-institutions/services/medical-institutions.service';
import { DoctorsService } from "src/app/pages/doctors/services/doctors.service";
import { Doctor } from '../../doctors/interfaces/doctor';
import { SurgeriesService } from '../../surgeries/services/surgeries.service';
import { Surgery } from '../../surgeries/interfaces/surgery';
import { Topography } from '../../topographies/interfaces/topography';
import { TopographiesService } from '../../topographies/services/topography.service';
import { Router } from '@angular/router';
import { FamilyService } from '../../families/services/family.service';

@Injectable({
  providedIn: 'root'
})
export class PatientsService {
  private dataOptionsForms$ : BehaviorSubject<any> = new BehaviorSubject<any>(null)
  public get optionsForms$() {
    return this.dataOptionsForms$
  }

  private patientEditSubject$ : BehaviorSubject<Patient> = new BehaviorSubject<Patient>(null)
  private urlUtils = "src/app/shared/utils/data/";

  private resultSubject$: BehaviorSubject<ResponsePaginate> = new BehaviorSubject(null)
  public get resultItems$() {
    return this.resultSubject$
  }
  public setItems(value) {
    this.resultSubject$.next(value)
  }

  private patient : Patient


  constructor(
    private http:HttpClient,
    private snackBar: MatSnackBar,
    private ubicationService: UbicationService,
    private medicalInstitutionsService: MedicalInstitutionsService,
    private doctorsService: DoctorsService,
    private surgeryService: SurgeriesService,
    private topographyService: TopographiesService,
    private familyService:FamilyService,
    private router: Router,
  ) {
    this.patientOnEdit.subscribe( res=> { this.patient = res })
  }

  get patientOnEdit():Observable<Patient>{
    return this.patientEditSubject$.asObservable()
  }

  setPatientOnEdit(value){
    this.familyService.setFamilyOnEdit(null)
    this.patientEditSubject$.next(value)
  }

  loadData(){
    const data = of(
      this.ubicationService.statesItems$,
      this.ubicationService.citiesItems$,
      this.ubicationService.countriesItems$,
      this.dataSelect()
    )

    const dataMerge = data.pipe(
      concatMap(v=>v)
    );

    dataMerge.subscribe(
      res => console.log(res)
    );

  }

  storePatient(data): Observable<Patient>{
    return this.http.post<Response>(`${environment.API}${environment.routesCRUD.patients}`, data).pipe(
      map( v => {
        this.setPatientOnEdit(v.data)
        //snacbarr
        this.openSnackBar('Se creó correctamente','success-snack-bar')
        //////////
        return v.data
      })

    )

  }

  updatePatient(data): Observable<Patient>{
    const id = this.patientEditSubject$.value.id
    data._method = "put";
    return this.http.post<Response>(`${environment.API}${environment.routesCRUD.patients}/${id}`, data).pipe(
      map( v => {
        this.setPatientOnEdit(v.data)
        //snacbarr
        this.openSnackBar('Actualizado correctamente','success-snack-bar')
        //////////
        return v.data
      })

    )

  }

  deletePatient(id){
    return this.http.delete<Response>(`${environment.API}${environment.routesCRUD.patients}/${id}`).pipe(
      map( v => {
        // console.log(v.data);

        this.getPatients(1, 20, '', 'desc').pipe(take(1)).subscribe()
        //snacbarr
        this.openSnackBar('Eliminado correctamente','success-snack-bar')
        //////////
        return v.data

      })

    )
  }


  getPatient(id) : Observable<Patient>{
    return this.http.get<Response>(`${environment.API}${environment.routesCRUD.patients}/${id}`).pipe(map(
      res => {
        this.familyService.setFamilyOnEdit(null)

        this.setPatientOnEdit(res.data)
        const resp = res.data
        return resp;
      }
    )

    )
  }


///listar
  getPatients(currentPage = 1, perPage = 20, filter='', sort= 'desc') : Observable<ResponsePaginate>{
    return this.http.get<ResponsePaginate>(`${environment.API}${environment.routesCRUD.patients}`, {
      params: new HttpParams()
        .set('page', currentPage.toString())
        .set('filter', filter)
        .set('sort', sort)
        .set('per_page', perPage.toString())

    }).pipe(map(
      res => {
        this.familyService.setFamilyOnEdit(null)

        this.setPatientOnEdit(null)
        this.setItems(res)
      //console.log(res)
        const resp = res
        return resp;
      }
    )

    )
  }


  filterPatients(currentPage = 1, perPage = 20, filter='', sort= 'desc') : Observable<ResponsePaginate>{
    return this.http.get<ResponsePaginate>(`${environment.API}${environment.routesCRUD.patients}`, {
      params: new HttpParams()
        .set('page', currentPage.toString())
        .set('filter', filter)
        .set('sort', sort)
        .set('per_page', perPage.toString())

    }).pipe(map(
      res => {

      //console.log(res)
        const resp = res
        return resp;
      }
    )

    )
  }

  checkExist(campo,value=""){
    return this.http.get<string>(`${environment.API}check_patient_exist`, {
      params: new HttpParams()
      .set(campo, value)
      .set('not_include_patient_id', this.patient?.id.toString())
    })
  }


  storeCancerAntecedents(data){
    return this.http.post<Response>(`${environment.API}cancer_patient_antecedent`, data).pipe(
      map( v => {
        this.patient.cancer_patient_antecedents = [...this.patient.cancer_patient_antecedents, v.data]
        this.setPatientOnEdit(this.patient)

        //snacbarr
        this.openSnackBar('Se creó correctamente','success-snack-bar')
        //////////
        return v.data
      })

    )
  }

  removeAntecedentCancer(id){
    return this.http.delete<Response>(`${environment.API}cancer_patient_antecedent/${id}`).pipe(
      map(res => {
          const dataDeleted = this.patient.cancer_patient_antecedents.filter(x=> x.id !== res.data.id)
          this.patient.cancer_patient_antecedents = dataDeleted
          this.setPatientOnEdit(this.patient)
          const resp = res.data
          return resp;
        }
    ))
  }

  openSnackBar(message: string, refClass:string, action: string = '') {
    this.snackBar.open(message, action, {
      duration: 2000,
      horizontalPosition: 'center',
      verticalPosition: 'top',
      panelClass: refClass
    });
  }


  dataSelect():Observable<any>{
    return this.http.get<any>('assets/data/data_selects.json').pipe(
      map(v=> v)
    )
  }


  //Medical Institutions
  private resultMedicalInstitutionsSubject$: BehaviorSubject<MedicalInstitution[]> = new BehaviorSubject([])

  public get medicalInstitutionsItems$() {
    if (!this.resultMedicalInstitutionsSubject$.getValue().length) {
      this.getMedicalInstitutions().pipe(take(1)).subscribe()
    }
    return this.resultMedicalInstitutionsSubject$.asObservable()
  }
  public setMedicalInstitutionsItems(value) {
    this.resultMedicalInstitutionsSubject$.next(value)
  }

  getMedicalInstitutions():Observable<MedicalInstitution[]>{
    return this.medicalInstitutionsService.getMedicalInstitutions(1,10000).pipe(map(
      v=> {
        this.setMedicalInstitutionsItems(v.data.data)
        return v.data.data
      }
    ))
  }


  //Doctores
  private resultDoctorsSubject$: BehaviorSubject<Doctor[]> = new BehaviorSubject([])

  public get doctorsItems$() {
    if (!this.resultDoctorsSubject$.getValue().length) {
      this.getDoctors().pipe(take(1)).subscribe()
    }
    return this.resultDoctorsSubject$.asObservable()
  }
  public setDoctorsItems(value) {
    this.resultDoctorsSubject$.next(value)
  }
  getDoctors():Observable<Doctor[]>{
    return this.doctorsService.getDoctors(1,10000).pipe(map(
      v=> {
        this.setDoctorsItems(v.data.data)
        return v.data.data
      }
    ))
  }


  getSurgeries(): Observable<Surgery[]>{
    return this.surgeryService.getSurgeries(1,10000).pipe(map(
      v=>v.data.data
    ))
  }

  getTopographies(): Observable<Topography[]>{
    return this.topographyService.getTopographies(1,10000).pipe(map(
      v=>v.data.data
    ))
  }

  lastId(){
    return this.http.get<Response>(`${environment.API}last_id`).pipe(map(
      v=> v?.data?.id ? v.data.id : 0
    ))
  }


}
