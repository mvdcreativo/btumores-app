import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { map, take } from 'rxjs/operators';
import { ResponsePaginate, Response } from 'src/app/shared/interfaces/response';
import { environment } from 'src/environments/environment';
import { TracePatient } from '../interfaces/tracePatient';

@Injectable({
  providedIn: 'root',
})
export class TracePatientService {
  private tracePatientEditSubject$: BehaviorSubject<TracePatient> = new BehaviorSubject<TracePatient>(null);
  private urlUtils = 'src/app/shared/utils/data/';

  private resultSubject$: BehaviorSubject<ResponsePaginate> = new BehaviorSubject(null);
  private statusPatient$: BehaviorSubject<string> = new BehaviorSubject("");
  public get resultItems$() {
    return this.resultSubject$;
  }
  public setItems(value : ResponsePaginate) {

    this.resultSubject$.next(value);
  }

  constructor(private http: HttpClient, private snackBar: MatSnackBar) {}

  get tracePatientOnEdit(): Observable<TracePatient> {
    return this.tracePatientEditSubject$;
  }

  get currentStatusPatient$():Observable<string>{
    return this.statusPatient$
  }

  setTracePatientOnEdit(value) {
    this.tracePatientEditSubject$.next(value);
  }


  ///listar
  getTraces(
    currentPage = 1,
    perPage = 10,
    filter = '',
    sort = 'desc',
    patient_id:number = null
  ): Observable<ResponsePaginate> {
    return this.http
      .get<ResponsePaginate>(`${environment.API}traces_patients`, {
        params: new HttpParams()
          .set('page', currentPage.toString())
          .set('filter', filter)
          .set('sort', sort)
          .set('patient_id', patient_id.toString())
          .set('per_page', perPage.toString()),
      })
      .pipe(
        map((res) => {
          //console.log(res);

          this.statusPatient$.next(res.data.data[0]?.stage?.name)
          this.setTracePatientOnEdit(null);
          this.setItems(res);
          const resp = res;
          return resp;
        })
      );
  }

  getPatient(id) : Observable<TracePatient>{
    return this.http.get<Response>(`${environment.API}traces_patients/${id}`).pipe(map(
      res => {
        this.setTracePatientOnEdit(res.data)
        const resp = res.data
        return resp;
      }
    )

    )
  }

  storeTracePatient(data): Observable<TracePatient> {
    return this.http
      .post<Response>(`${environment.API}traces_patients`, data)
      .pipe(
        map((v) => {
          this.setTracePatientOnEdit(v.data);
          //snacbarr
          this.openSnackBar('Se creó correctamente', 'success-snack-bar');
          //////////
          return v.data;
        })
      );
  }

  openSnackBar(message: string, refClass: string, action: string = '') {
    this.snackBar.open(message, action, {
      duration: 2000,
      horizontalPosition: 'center',
      verticalPosition: 'top',
      panelClass: refClass,
    });
  }

  updateTrace(data): Observable<TracePatient>{
    data._method = "put";
    return this.http
    .post<Response>(`${environment.API}traces_patients/${data.id}`, data)
    .pipe(
      map((v) => {
        this.setTracePatientOnEdit(v.data);
        //snacbarr
        this.openSnackBar('Se creó correctamente', 'success-snack-bar');
        //////////
        return v.data;
      })
    );
  }
}
