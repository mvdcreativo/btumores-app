import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { TracingPatient } from '../interfaces/tracePatient';
import { Response } from 'src/app/shared/interfaces/response';
import { PatientsService } from './patients.service';
import { Patient } from '../interfaces/patient';


@Injectable({
  providedIn: 'root'
})
export class TracingService {

  private tracingPatientEditSubject$: BehaviorSubject<TracingPatient> = new BehaviorSubject<TracingPatient>(null);

  constructor(
    private http: HttpClient,
    private snackBar: MatSnackBar,
  ) {
  }

  tracePatientOnEdit(patient_id?): Observable<TracingPatient> {

    if (patient_id) {
      return this.getTracing(patient_id)
    }

    if (this.tracingPatientEditSubject$.getValue()) {
      return this.tracingPatientEditSubject$.asObservable();
    }
  }

  setTracingPatientOnEdit(value) {
    this.tracingPatientEditSubject$.next(value);
  }

  storeTracingPatient(data: TracingPatient) {
    const dataSend = data;
    this.http
      .post<Response>(`${environment.API}tracings`, dataSend)
      .pipe(
        map((v) => {
          this.setTracingPatientOnEdit(v.data);

          return v.data;
        })
      ).subscribe()
  }

  updateTracingPatient(data: TracingPatient) {
    const dataSend = data;
    dataSend['_method'] = 'PUT';
    this.http
      .post<Response>(`${environment.API}tracings/${data.id}`, dataSend)
      .pipe(
        map((v) => {
          this.setTracingPatientOnEdit(v.data);

          return v.data;
        })
      ).subscribe()
  }

  getTracing(patient_id) : Observable<TracingPatient>{
    return this.http.get<Response>(`${environment.API}tracings/${patient_id}`).pipe(map(
      res => {
        this.setTracingPatientOnEdit(res.data)
        const resp = res.data
        return resp;
      }
    )

    )
  }

  openSnackBar(message: string, refClass: string, action: string = '') {
    this.snackBar.open(message, action, {
      duration: 2000,
      horizontalPosition: 'center',
      verticalPosition: 'top',
      panelClass: refClass,
    });
  }
}
