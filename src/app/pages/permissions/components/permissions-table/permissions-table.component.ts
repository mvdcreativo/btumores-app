import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { Router } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import { map, take } from 'rxjs/operators';
import { Column } from 'src/app/shared/components/data-table/interfaces/table';
import { ResponsePaginate } from 'src/app/shared/interfaces/response';
import { Permission } from '../../interfaces/permission';
import { PermissionsService } from '../../services/permissions.service';

@Component({
  selector: 'mvd-permissions-table',
  templateUrl: './permissions-table.component.html',
  styleUrls: ['./permissions-table.component.scss']
})
export class PermissionsTableComponent implements OnInit {

  // displayedColumns: string[] = ['code', 'title', 'permission_owner.name', 'address', 'neighborhood.name'];

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  ////COLUMNAS TABLA
  public columns: Column[] = [
    { title: 'Código', col: 'id' },
    { title: 'Nombre', col: 'name' },
    { title: 'Descripción', col: 'description' },
  ]

  dataSource: Observable<any[]>;
  permissions: Permission[];

  ////paginator
  totalResut: Observable<number>;
  pageDefault = 1;
  perPage: number = 10;
  orden: string = 'desc';
  filter: string = '';
  result: Observable<ResponsePaginate>;
  subscroption: Subscription;
  

  paginatorChange(e: PageEvent) {
  //console.log(e);
    this.getPermissions(e.pageIndex + 1, e.pageSize)

  }
  /////////////


  constructor(
    private permissionService: PermissionsService,
    public router: Router,
  ) {
    this.result = this.permissionService.resultItems$

  }


  ngOnInit(): void {
    this.getPermissions(this.pageDefault, this.perPage, this.filter, this.orden)
    
  }


  add(){
    this.router.navigate(['/permisos/permiso'] , { queryParams: { urlReturn: this.router.url} })
  }


  getPermissions(currentPage?, perPage?, filter?, sort?) {
    this.subscroption = this.permissionService.getPermissions(currentPage, perPage, filter, sort).subscribe(next => this.loadData());
  }

  loadData() {

    this.dataSource = this.result.pipe(map(v => {
      const dataTable = v.data.data.map(x => {
        return {
          id: x.id,
          name: x.name,
          description: x.description
        }
      })
      return dataTable;
    }))

    this.totalResut = this.result.pipe(map(v => v.data.total))
  }

  search(filter) {
    this.getPermissions(this.pageDefault, this.perPage, filter, this.orden )
  }

  deleteItem(id): Observable<any> {
    return this.permissionService.deletePermission(id)
  }

  itemAction(event) {
  //console.log(event);

    if (event.action === "delete") {
      this.deleteItem(event.element.id).pipe(take(1)).subscribe(res => console.log(res))
    }

    if (event.action === "edit") {
      this.router.navigate(['/permisos/permiso', event.element.id], {queryParams:{urlReturn : this.router.url}})
    }

  }



  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    this.subscroption.unsubscribe()
  }

}
