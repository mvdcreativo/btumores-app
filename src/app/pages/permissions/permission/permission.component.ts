import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import { take } from 'rxjs/operators';
import { MvdValidations } from 'src/app/shared/components/forms/mvd-validations/mvd-validations';
import { ConfirmComponent } from 'src/app/shared/components/modals/confirm/confirm.component';
import { ModalConfirmNoChangesComponent } from 'src/app/shared/components/modals/modal-confirm-no-changes/modal-confirm-no-changes.component';
import { Permission } from '../interfaces/permission';
import { PermissionsService } from '../services/permissions.service';

@Component({
  selector: 'mvd-permission',
  templateUrl: 'permission.component.html',
  styleUrls: ['permission.component.scss']
})
export class PermissionComponent implements OnInit {
  permissionEdit: Permission;
  form: FormGroup;
  urlReturn: string;
  changes : boolean = false;
  subscriptions: Subscription[]=[];
  permissionId: number;

  constructor(
    private permissionService: PermissionsService,
    private activatedRoute: ActivatedRoute,
    private fb: FormBuilder,
    private router: Router,
    public dialog: MatDialog,
  ) { 
    this.createForm()
  }

  ngOnInit(): void {
    this.subscriptions.push(
      this.permissionService.permissionOnEdit.subscribe(res=> {
        this.permissionEdit = res
        this.permissionEdit ? this.updateForm(this.permissionEdit): false
      })
    )

    this.activatedRoute.paramMap.subscribe(
      (params:Params) => {
        if(params.params.id){
         this.permissionService.getPermission(params.params.id).pipe(take(1)).subscribe()
        }
        
        if(!params.params.id){
          this.permissionEdit = null
        }
      }
    )
    this.activatedRoute.queryParamMap.subscribe(
      (res:Params) => {
        this.urlReturn = res?.params?.urlReturn
        this.permissionId = res?.params?.id
      //console.log(res);
        
      }
    )

  }

  createForm(){
    this.form = this.fb.group({
      name:[null, Validators.required],
      description:[null, Validators.required],
      
    })
  }
  private updateForm(permission:Permission){
    this.form.setValue({
      name: permission?.name,
      description: permission?.description,
    })
  }


  
  onSubmit(){
    if(this.permissionEdit){
      this.subscriptions.push(
        this.permissionService.updatePermission(this.form.value).subscribe(res=> res ? this.back() : false )
      )
    }else{
      this.subscriptions.push(
        this.permissionService.storePermission(this.form.value).subscribe(res=> res ? this.back() : false )
      )
    }
  }

  back(){
    this.urlReturn ? this.router.navigate([this.urlReturn]) : this.router.navigate(['/usuarios'])
    
  }

  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    this.subscriptions.map(v=>v.unsubscribe())
  }
}
