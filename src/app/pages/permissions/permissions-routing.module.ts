import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PermissionComponent } from './permission/permission.component';
import { PermissionsComponent } from './permissions.component';

const routes: Routes = [
  { path: '', component: PermissionsComponent },
  { path: 'permiso', component: PermissionComponent },
  { path: 'permiso/:id', component: PermissionComponent },
  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PermissionsRoutingModule { }
