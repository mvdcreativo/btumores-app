import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PermissionsRoutingModule } from './permissions-routing.module';
import { PermissionsComponent } from './permissions.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { PermissionsTableComponent } from './components/permissions-table/permissions-table.component';
import { PermissionComponent } from './permission/permission.component';


@NgModule({
  declarations: [
    PermissionsComponent,
    PermissionComponent,
    PermissionsTableComponent
  ],
  imports: [
    CommonModule,
    PermissionsRoutingModule,
    SharedModule
  ]
})
export class PermissionsModule { }
