import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { concatMap, map, take } from 'rxjs/operators';
import { ResponsePaginate, Response } from 'src/app/shared/interfaces/response';
import { UbicationService } from 'src/app/shared/services/ubications/ubication.service';
import { environment } from 'src/environments/environment';
import { Permission } from '../interfaces/permission'

@Injectable({
  providedIn: 'root'
})
export class PermissionsService {

  permission: Permission;

  private dataOptionsForms$ : BehaviorSubject<any> = new BehaviorSubject<any>(null)

  public get optionsForms$() {
    return this.dataOptionsForms$
  }

  private permissionEditSubject$ : BehaviorSubject<Permission> = new BehaviorSubject<Permission>(null)
  private urlUtils = "src/app/shared/utils/data/";

  private resultSubject$: BehaviorSubject<ResponsePaginate> = new BehaviorSubject(null)
  public get resultItems$() {
    return this.resultSubject$
  }
  public setItems(value) {
    this.resultSubject$.next(value)
  }



  constructor(
    private http:HttpClient,
    private snackBar: MatSnackBar,
    private ubicationService: UbicationService,
  ) {
    // this.loadData()
    this.permissionOnEdit.subscribe( res=> { this.permission = res })
  }

  get permissionOnEdit():Observable<Permission>{
    return this.permissionEditSubject$
  }

  setPermissionOnEdit(value){
    this.permissionEditSubject$.next(value)
  }


  storePermission(data): Observable<Permission>{
    return this.http.post<Response>(`${environment.API}admin/permissions`, data).pipe(
      map( v => {
        this.setPermissionOnEdit(v.data)
        //snacbarr
        this.openSnackBar('Se creó correctamente','success-snack-bar')
        //////////
        return v.data
      })

    )

  }

  updatePermission(data): Observable<Permission>{
    const id = this.permissionEditSubject$.value.id
    data._method = "put";
    return this.http.post<Response>(`${environment.API}admin/permissions/${id}`, data).pipe(
      map( v => {
        this.setPermissionOnEdit(v.data)
        //snacbarr
        this.openSnackBar('Actualizado correctamente','success-snack-bar')
        //////////
        return v.data
      })

    )

  }

  deletePermission(id){
    return this.http.delete<Response>(`${environment.API}admin/permissions/${id}`).pipe(
      map( v => {
        // console.log(v.data);

        this.getPermissions(1, 20, '', 'desc').pipe(take(1)).subscribe()
        //snacbarr
        this.openSnackBar('Eliminado correctamente','success-snack-bar')
        //////////
        return v.data

      })

    )
  }


  getPermission(id) : Observable<Permission>{
    return this.http.get<Response>(`${environment.API}admin/permissions/${id}`).pipe(map(
      res => {
        this.setPermissionOnEdit(res.data)
        const resp = res.data
        return resp;
      }
    )

    )
  }


///listar
  getPermissions(currentPage = 1, perPage = 20, filter='', sort= 'desc') : Observable<ResponsePaginate>{
    return this.http.get<ResponsePaginate>(`${environment.API}admin/permissions`, {
      params: new HttpParams()
        .set('page', currentPage.toString())
        .set('filter', filter)
        .set('sort', sort)
        .set('per_page', perPage.toString())

    }).pipe(map(
      res => {
      //console.log(res);

        this.setPermissionOnEdit(null)
        this.setItems(res)
        const resp = res
        return resp;
      }
    )

    )
  }



  openSnackBar(message: string, refClass:string, action: string = '') {
    this.snackBar.open(message, action, {
      duration: 2000,
      horizontalPosition: 'center',
      verticalPosition: 'top',
      panelClass: refClass
    });
  }

  // checkEmailExist(email){
  //   return this.http.get<string>(`${environment.API}check_email_exist`, {
  //     params: new HttpParams()
  //     .set('email', email)
  //     .set('email_exclude', this.permission?.email)
  //   })
  // }
}
