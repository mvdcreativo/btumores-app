import { Permission } from "../../permissions/interfaces/permission";

export interface Role {
    id?: number;
    name?: string;
    description?: any;
    guard_name?:any;
    permissions?: Permission[];

}
