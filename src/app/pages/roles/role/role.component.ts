import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import { map, take } from 'rxjs/operators';
import { MvdValidations } from 'src/app/shared/components/forms/mvd-validations/mvd-validations';
import { ConfirmComponent } from 'src/app/shared/components/modals/confirm/confirm.component';
import { ModalConfirmNoChangesComponent } from 'src/app/shared/components/modals/modal-confirm-no-changes/modal-confirm-no-changes.component';
import { ResponsePaginate } from 'src/app/shared/interfaces/response';
import { Permission } from '../../permissions/interfaces/permission';
import { PermissionsService } from '../../permissions/services/permissions.service';
import { Role } from '../interfaces/role';
import { RolesService } from '../services/roles.service';

@Component({
  selector: 'mvd-role',
  templateUrl: './role.component.html',
  styleUrls: ['./role.component.scss']
})
export class RoleComponent implements OnInit {
  roleEdit: Role;
  form: FormGroup;
  urlReturn: string;
  changes: boolean = false;
  subscriptions: Subscription[] = [];
  roleId: number;
  permissions: any;

  constructor(
    private roleService: RolesService,
    private permissionService: PermissionsService,
    private activatedRoute: ActivatedRoute,
    private fb: FormBuilder,
    private router: Router,
    public dialog: MatDialog,
  ) {
    this.createForm()
  }

  ngOnInit(): void {
    
    this.activatedRoute.paramMap.subscribe(
      (params: Params) => {
        if (params.params.id) {
          this.subscriptions.push(
            this.roleService.getRole(params.params.id).subscribe(res=>{
              this.getPermissions()
              
            })
          )

        }

        if (!params.params.id) {
          this.roleEdit = null
          this.getPermissions()
        }
      }
    )

    this.activatedRoute.queryParamMap.subscribe(
      (res: Params) => {
        this.urlReturn = res?.params?.urlReturn
        this.roleId = res?.params?.id

      }
    )
  }

  createForm() {
    this.form = this.fb.group({
      name: [null, Validators.required, MvdValidations.validateRoleExist(this.roleService)],
      description: [null, Validators.required],
      permissions: new FormArray([])
    })
  }
  private updateForm(role: Role) {
    this.form.patchValue({
      name: role?.name,
      description: role?.description,
      permissions: this.buildPermissionFormArray()
    })
  }

  getPermissions() {
    this.subscriptions.push(
      this.permissionService.getPermissions(1, 1000).pipe(map(v => v.data.data)).subscribe(
        res => {
        //console.log(res);
          this.permissions = res;
          this.permissions = res.map((v, i) => {
            this.permissions[i]['group'] = v.name.split('.', 1)[0]
            return v;
          })
          this.subscriptions.push(
            this.roleService.roleOnEdit.subscribe(res => {
              this.roleEdit = res
              this.roleEdit ? this.updateForm(this.roleEdit) : this.buildPermissionFormArray()
            })
          )
          // console.log(this.permissions);
          
          
        })
    )
  }


  buildPermissionFormArray() {
    this.permissions.forEach((v) =>{


      if(this.roleEdit?.permissions.length >=1){
        // console.log('tine permissos');
        
        const searchPermission = this.roleEdit.permissions.find( f => f.name === v.name )
        searchPermission ? this.control.push(new FormControl(true)) : this.control.push(new FormControl(false))
      }else{
      //console.log('no tine permissos');

        this.control.push(new FormControl(false))
      }

    })
  }
  get control() {
    return this.form.get('permissions') as FormArray;
  }


  onSubmit() {
    let roleData = Object.assign({}, this.form.value)
    let submitPermissions = roleData.permissions.map((v,i)=>{
        v ? v = this.permissions[i].name : v = false
        return v
    }).filter(x=> x!== false)

    roleData.permissions= submitPermissions;


    if (this.roleEdit) {
      this.subscriptions.push(
        this.roleService.updateRole(roleData).subscribe(res => res ? this.back() : false)
      )
    } else {
      this.subscriptions.push(
        this.roleService.storeRole(roleData).subscribe(res => res ? this.back() : false)

      )
    }
  }

  back() {
    this.urlReturn ? this.router.navigate([this.urlReturn]) : this.router.navigate(['/roles'])

  }

  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    this.subscriptions.map(v => v.unsubscribe())
  }



  traslateTitle(name) {
    let title;

    switch (name) {
      case 'user':
        title = 'Usuarios'
        break;

      case 'permission':
        title = 'Permisos'
        break;

      case 'sample':
        title = 'Muestras'
        break;

      case 'ubication':
        title = 'Geolocalización'
        break;

      case 'patient':
        title = 'Pacientes'
        break;

      case 'role':
        title = 'Roles'
        break;

      default:
        title = name
        break;
    }

    return title
  }
}
