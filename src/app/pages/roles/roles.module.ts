import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RolesRoutingModule } from './roles-routing.module';
import { RolesComponent } from './roles.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { RolesTableComponent } from './components/roles-table/roles-table.component';
import { RoleComponent } from './role/role.component';


@NgModule({
  declarations: [
    RolesComponent,
    RoleComponent,
    RolesTableComponent
  ],
  imports: [
    CommonModule,
    RolesRoutingModule,
    SharedModule
  ]
})
export class RolesModule { }
