import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { concatMap, map, take } from 'rxjs/operators';
import { ResponsePaginate, Response } from 'src/app/shared/interfaces/response';
import { UbicationService } from 'src/app/shared/services/ubications/ubication.service';
import { environment } from 'src/environments/environment';
import { Role } from '../interfaces/role'

@Injectable({
  providedIn: 'root'
})
export class RolesService {

  role: Role;

  private dataOptionsForms$ : BehaviorSubject<any> = new BehaviorSubject<any>(null)

  public get optionsForms$() {
    return this.dataOptionsForms$
  }

  private roleEditSubject$ : BehaviorSubject<Role> = new BehaviorSubject<Role>(null)
  private urlUtils = "src/app/shared/utils/data/";

  private resultSubject$: BehaviorSubject<ResponsePaginate> = new BehaviorSubject(null)
  public get resultItems$() {
    return this.resultSubject$
  }
  public setItems(value) {
    this.resultSubject$.next(value)
  }



  constructor(
    private http:HttpClient,
    private snackBar: MatSnackBar,
    private ubicationService: UbicationService,
  ) {
    // this.loadData()
    this.roleOnEdit.subscribe( res=> { this.role = res })
  }

  get roleOnEdit():Observable<Role>{
    return this.roleEditSubject$
  }

  setRoleOnEdit(value){
    this.roleEditSubject$.next(value)
  }


  storeRole(data): Observable<Role>{
    return this.http.post<Response>(`${environment.API}admin/roles`, data).pipe(
      map( v => {
        this.setRoleOnEdit(v.data)
        //snacbarr
        this.openSnackBar('Se creó correctamente','success-snack-bar')
        //////////
        return v.data
      })

    )

  }

  updateRole(data): Observable<Role>{
    const id = this.roleEditSubject$.value.id
    data._method = "put";
    return this.http.post<Response>(`${environment.API}admin/roles/${id}`, data).pipe(
      map( v => {
        this.setRoleOnEdit(v.data)
        //snacbarr
        this.openSnackBar('Actualizado correctamente','success-snack-bar')
        //////////
        return v.data
      })

    )

  }

  deleteRole(id){
    return this.http.delete<Response>(`${environment.API}admin/roles/${id}`).pipe(
      map( v => {
        // console.log(v.data);

        this.getRoles(1, 20, '', 'desc').pipe(take(1)).subscribe()
        //snacbarr
        this.openSnackBar('Eliminado correctamente','success-snack-bar')
        //////////
        return v.data

      })

    )
  }


  getRole(id) : Observable<Role>{
    return this.http.get<Response>(`${environment.API}admin/roles/${id}`).pipe(map(
      res => {
        this.setRoleOnEdit(res.data)
        const resp = res.data
        return resp;
      }
    )

    )
  }


///listar
  getRoles(currentPage = 1, perPage = 20, filter='', sort= 'desc') : Observable<ResponsePaginate>{
    return this.http.get<ResponsePaginate>(`${environment.API}admin/roles`, {
      params: new HttpParams()
        .set('page', currentPage.toString())
        .set('filter', filter)
        .set('sort', sort)
        .set('per_page', perPage.toString())

    }).pipe(map(
      res => {
      //console.log(res);

        this.setRoleOnEdit(null)
        this.setItems(res)
        const resp = res
        return resp;
      }
    )

    )
  }



  openSnackBar(message: string, refClass:string, action: string = '') {
    this.snackBar.open(message, action, {
      duration: 2000,
      horizontalPosition: 'center',
      verticalPosition: 'top',
      panelClass: refClass
    });
  }

  checkRoleExist(role){
    return this.http.get<string>(`${environment.API}check_role_exist`, {
      params: new HttpParams()
      .set('role', role)
      .set('role_exclude', this.role?.name)
    })
  }
}
