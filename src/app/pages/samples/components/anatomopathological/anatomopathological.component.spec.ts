import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AnatomopathologicalComponent } from './anatomopathological.component';

describe('AnatomopathologicalComponent', () => {
  let component: AnatomopathologicalComponent;
  let fixture: ComponentFixture<AnatomopathologicalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AnatomopathologicalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AnatomopathologicalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
