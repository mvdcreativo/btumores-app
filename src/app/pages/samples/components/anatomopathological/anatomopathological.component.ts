import { Component, OnInit, ChangeDetectionStrategy, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDatepickerInputEvent } from '@angular/material/datepicker';
import moment from 'moment';
import { Observable, Subscription } from 'rxjs';
import { debounceTime, map } from 'rxjs/operators';
import { Estadio } from 'src/app/pages/estadio/interfaces/estadio';
import { EstadiosService } from 'src/app/pages/estadio/services/estadios.service';
import { PatientsService } from 'src/app/pages/patients/services/patients.service';
import { Topography } from 'src/app/pages/topographies/interfaces/topography';
import { TopographiesService } from 'src/app/pages/topographies/services/topography.service';
import { TumorLineage } from 'src/app/pages/tumor-lineage/interfaces/tumor-lineage';
import { TumorLineageService } from 'src/app/pages/tumor-lineage/services/tumor-lineage.service';
import { ItemOption } from 'src/app/shared/components/input-select-add-item/Interfaces/item-option';
import { Sample } from '../../interfaces/sample';
import { SamplesService } from '../../services/samples.service';

@Component({
  selector: 'mvd-anatomopathological',
  templateUrl: './anatomopathological.component.html',
  styleUrls: ['./anatomopathological.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AnatomopathologicalComponent implements OnInit {

  @Output() anatomopatologicalData : EventEmitter<any> = new EventEmitter()

  form: FormGroup;
  subscriptions: Subscription[]=[];
  sampleEdit: Sample;
  estadios: Observable<Estadio[]>;
  tumorLineages: Observable<TumorLineage[]>;
  topographies:Topography[];
  dataSelects$: Observable<any>;
  optionsTopographies: ItemOption[] = [];
  valueSelect: string

  constructor(
    private fb:FormBuilder,
    private sampleService:SamplesService,
    private estadioService: EstadiosService,
    private tumorLineageService: TumorLineageService,
    private topographyService: TopographiesService,
    private patientService: PatientsService,
  ) {

  }

  ngOnInit(): void {
    this.loadData()
    this.createForm()
    this.dataSelects$ = this.patientService.dataSelect()

    this.subscriptions.push(
      this.sampleService.sampleOnEdit.subscribe(res => {
        this.sampleEdit = res ;
        this.sampleEdit ? this.updateForm(this.sampleEdit): false

      }),

    )
  }

  createForm(){
    this.form = this.fb.group({
      estadio_id:[],
      anatomia:[],
      anatomia_date:[],
      biopsia:[],
      reseccion_q:[],
      con_cancer:[],
      operacion:[],
      isquemia_min:[],
      isquemia_seg:[],
      tacos_cant:[],
      laminas_cant:[],
      necrosis_tumoral_cant:[],
      sample_id:[],
      tumor_lineage_id:[null],
      linfocito_intra_tumoral:[null],
      tnm_t:[null],
      tnm_n:[null],
      tnm_m:[null],

      topography_id:[null],
    })

    this.form.valueChanges.pipe(debounceTime(500) ).subscribe(value => {
      this.anatomopatologicalData.emit(this.form)
    })
  }

  private updateForm(sample:Sample){

    this.form.patchValue({
      estadio_id: sample.sample_data_anatomo?.estadio_id,
      anatomia: sample.sample_data_anatomo?.anatomia,
      anatomia_date: sample.sample_data_anatomo?.anatomia_date,
      biopsia: sample.sample_data_anatomo?.biopsia,
      reseccion_q: sample.sample_data_anatomo?.reseccion_q,
      con_cancer: sample.sample_data_anatomo?.con_cancer,
      operacion: sample.sample_data_anatomo?.operacion,
      isquemia_min: sample.sample_data_anatomo?.isquemia_min,
      isquemia_seg: sample.sample_data_anatomo?.isquemia_seg,
      tacos_cant: sample.sample_data_anatomo?.tacos_cant,
      laminas_cant: sample.sample_data_anatomo?.laminas_cant,
      necrosis_tumoral_cant: sample.sample_data_anatomo?.necrosis_tumoral_cant,
      sample_id: sample.sample_data_anatomo?.sample_id,
      linfocito_intra_tumoral: sample?.sample_data_anatomo?.linfocito_intra_tumoral,
      tumor_lineage_id: sample?.tumor_lineage_id,
      tnm_t:sample.sample_data_anatomo?.tnm_t,
      tnm_n:sample.sample_data_anatomo?.tnm_n,
      tnm_m:sample.sample_data_anatomo?.tnm_m,
      topography_id: sample?.topography_id,
    })
    this.anatomopatologicalData.emit(this.form)
  }



  ngOnDestroy(){
    this.subscriptions.map(v=> v.unsubscribe() )
  }

  formatDate(e : MatDatepickerInputEvent<Date>, campo){
    let brt = e.value;
    this.form.get(campo).setValue(moment(brt).format('YYYY-MM-DD HH:mm:ss'))
  }

  loadData() {
    this.estadios = this.estadioService.getEstadios(1,1000).pipe(map(v=>v.data.data))
    this.tumorLineages = this.tumorLineageService.listTumorLineageItems$
    this.formatOptionsSelectTopograpy()
  }

  changeTopography(value){
    this.form.get('topography_id').setValue(value)
    this.valueSelect = value

  }

  formatOptionsSelectTopograpy(){
    this.topographyService.listTopographiesItems$.subscribe(items=>{
      this.topographies = items,
      this.optionsTopographies = items.map(v=> ( {value: v.id, text: `${v.name} - CIE 10 ${v.cie10}`,} ) )
    })
  }

}
