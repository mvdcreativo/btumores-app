import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IdentificationSampleComponent } from './identification-sample.component';

describe('IdentificationSampleComponent', () => {
  let component: IdentificationSampleComponent;
  let fixture: ComponentFixture<IdentificationSampleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IdentificationSampleComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IdentificationSampleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
