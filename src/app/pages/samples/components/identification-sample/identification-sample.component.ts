import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Observable, Subscription } from 'rxjs';
import { debounceTime, map } from 'rxjs/operators';
import { Patient } from 'src/app/pages/patients/interfaces/patient';
import { PatientsService } from 'src/app/pages/patients/services/patients.service';
import { Tnm } from 'src/app/pages/tnm/interfaces/tnm';
import { Topography } from 'src/app/pages/topographies/interfaces/topography';
import { TopographiesService } from 'src/app/pages/topographies/services/topography.service';
import { TumorLineage } from 'src/app/pages/tumor-lineage/interfaces/tumor-lineage';
import { TumorLineageService } from 'src/app/pages/tumor-lineage/services/tumor-lineage.service';
import { TypeSample } from 'src/app/pages/types-samples/interfaces/type-sample';
import { TypeSamplesService } from 'src/app/pages/types-samples/services/types-samples.service';
import { MvdValidations } from 'src/app/shared/components/forms/mvd-validations/mvd-validations';
import { ResponsePaginate } from 'src/app/shared/interfaces/response';
import { Sample } from '../../interfaces/sample';
import { SamplesService } from '../../services/samples.service';

@Component({
  selector: 'mvd-identification-sample',
  templateUrl: './identification-sample.component.html',
  styleUrls: ['./identification-sample.component.scss']
})
export class IdentificationSampleComponent implements OnInit {

  @Input() patient_id: number
  @Output() sampleIdentification : EventEmitter<any> = new EventEmitter()

  form: FormGroup;
  dataSelects: Observable<any>;
  patientEdit:Patient;
  subscriptions: Subscription[]=[];
  patient: Patient;
  tnms: Observable<Tnm[]>;
  tumorLineages: Observable<TumorLineage[]>;
  topographies: Observable<Topography[]>;
  typesSamples: Observable<TypeSample[]>;
  sampleEdit: Sample;

  constructor(
    private fb: FormBuilder,
    private patientService: PatientsService,
    private typeSampleService: TypeSamplesService,
    private sampleService: SamplesService

  ) {

    this.dataSelects = this.patientService.dataSelect()
  }

  ngOnInit(): void {
    this.loadData()

    this.createForm()
    this.subscriptions.push(
      this.sampleService.sampleOnEdit.subscribe(res => {
        this.sampleEdit = res ;

        this.sampleEdit ? this.updateForm(this.sampleEdit): this.setCode()


      }),

    )
  }


  private createForm(){
    this.form = this.fb.group({
      id:[],
      code:[null, Validators.required, MvdValidations.validateCode(this.sampleService)],
      patient_id:[this.patient_id, Validators.required],
      type_sample_id:[null],
    })

    this.form.valueChanges.pipe(debounceTime(500) ).subscribe(value => {
      this.sampleIdentification.emit(this.form)
    })

  }

  private updateForm(sample){

    this.form.setValue({
      id: sample.id,
      code: sample?.code,
      patient_id: sample?.patient_id,
      type_sample_id: sample?.type_sample_id,
    })

    this.sampleIdentification.emit(this.form)
  }

  setCode(){

    if(!this.sampleEdit?.code){
      this.subscriptions.push(
        this.sampleService.genCode().subscribe( res=> {
          //const prefix = e.value.slice(1)
          this.form.get('code').setValue(res)
        })

      )
      //genera el codigo de muestras. Recibe como argumentos (numero, longitud del relleno, caracter de relleno)
      // function genCode(num, padlen, padchar?) {

      //   var pad_char = typeof padchar !== 'undefined' ? padchar : '0';
      //   var pad = new Array(1 + padlen).join(pad_char);
      //   return "#HM"+(pad + num).slice(-pad.length);
      // }

    }

  }

  ngOnDestroy(){
    this.subscriptions.map(v=> v.unsubscribe() )
  }



  loadData() {
    this.typesSamples = this.typeSampleService.listTypeSampleItems$
  }
}
