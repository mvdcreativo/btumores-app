import { Component, OnInit, ChangeDetectionStrategy, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDatepickerInputEvent } from '@angular/material/datepicker';
import moment from 'moment';
import { Observable, Subscription } from 'rxjs';
import { debounceTime } from 'rxjs/operators';
import { PatientsService } from 'src/app/pages/patients/services/patients.service';
import { Sample } from '../../interfaces/sample';
import { SamplesService } from '../../services/samples.service';

@Component({
  selector: 'mvd-sample-data',
  templateUrl: './sample-data.component.html',
  styleUrls: ['./sample-data.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SampleDataComponent implements OnInit {

  @Output() sampleData : EventEmitter<any> = new EventEmitter()

  form: FormGroup;
  subscriptions: Subscription[]=[];
  dataSelects: Observable<any>;
  sampleEdit: Sample;

  constructor(
    private fb:FormBuilder,
    private patientService: PatientsService,
    private sampleService: SamplesService
  ) {
    this.createForm()
  }

  ngOnInit(): void {

    this.loadData()
    this.createForm()
    this.subscriptions.push(
      this.sampleService.sampleOnEdit.subscribe(res => {
        this.sampleEdit = res ;
        this.sampleEdit ? this.updateForm(this.sampleEdit): false

      }),

    )
  }

  createForm(){
    this.form = this.fb.group({
      trat_q: [false],
      trat_q_date: [],
      trat_q_criterio: [],
      trat_q_plan: [],
      radio_t: [false],
      radio_t_date: [],
      radio_t_criterio: [],
      radio_t_plan: [],
      quimio: [false],
      quimio_date: [],
      quimio_criterio: [],
      quimio_plan: [],
      homo: [false],
      homo_date: [],
      homo_criterio: [],
      homo_plan: [],
      terapia_bio: [false],
      terapia_bio_date: [],
      terapia_bio_criterio: [],
      terapia_bio_plan: [],
      inmuno_terapia: [false],
      inmuno_terapia_date: [],
      inmuno_terapia_criterio: [],
      inmuno_terapia_plan: [],
      otros: [false],
      otros_date: [],
      otros_criterio: [],
      otros_plan: [],
      sample_id:[]
    })

    this.form.valueChanges.pipe(debounceTime(500) ).subscribe(value => {
      this.sampleData.emit(this.form)
    })
  }

  private updateForm(sample: Sample){
    console.log(sample);

    this.form.patchValue({
      trat_q: sample.sample_data?.trat_q,
      trat_q_date: sample.sample_data?.trat_q_date,
      trat_q_criterio: sample.sample_data?.trat_q_criterio,
      trat_q_plan: sample.sample_data?.trat_q_plan,
      radio_t: sample.sample_data?.radio_t,
      radio_t_date: sample.sample_data?.radio_t_date,
      radio_t_criterio: sample.sample_data?.radio_t_criterio,
      radio_t_plan: sample.sample_data?.radio_t_plan,
      quimio: sample.sample_data?.quimio,
      quimio_date: sample.sample_data?.quimio_date,
      quimio_criterio: sample.sample_data?.quimio_criterio,
      quimio_plan: sample.sample_data?.quimio_plan,
      homo: sample.sample_data?.homo,
      homo_date: sample.sample_data?.homo_date,
      homo_criterio: sample.sample_data?.homo_criterio,
      homo_plan: sample.sample_data?.homo_plan,
      terapia_bio: sample.sample_data?.terapia_bio,
      terapia_bio_date: sample.sample_data?.terapia_bio_date,
      terapia_bio_criterio: sample.sample_data?.terapia_bio_criterio,
      terapia_bio_plan: sample.sample_data?.terapia_bio_plan,
      inmuno_terapia: sample.sample_data?.inmuno_terapia,
      inmuno_terapia_date: sample.sample_data?.inmuno_terapia_date,
      inmuno_terapia_criterio: sample.sample_data?.inmuno_terapia_criterio,
      inmuno_terapia_plan: sample.sample_data?.inmuno_terapia_plan,
      otros: sample.sample_data?.otros,
      otros_date: sample.sample_data?.otros_date,
      otros_criterio: sample.sample_data?.otros_criterio,
      otros_plan: sample.sample_data?.otros_plan,
      sample_id: sample.sample_data?.sample_id,
    })

    this.sampleData.emit(this.form)

  }

  ngOnDestroy(){
    this.subscriptions.map(v=> v.unsubscribe() )
  }

  formatDate(e : MatDatepickerInputEvent<Date>, campo){
    let brt = e.value;
    this.form.get(campo).setValue(moment(brt).format('YYYY-MM-DD HH:mm:ss'))
  }

  loadData(){
    this.dataSelects = this.patientService.dataSelect()

  }
}
