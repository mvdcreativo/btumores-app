import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { Observable, Subscription } from 'rxjs';
import { map } from 'rxjs/operators';
import { Patient } from 'src/app/pages/patients/interfaces/patient';
import { Column } from 'src/app/shared/components/data-table/interfaces/table';
import { OptionsSelect } from 'src/app/shared/interfaces/data-selects';
import { ResponsePaginate } from 'src/app/shared/interfaces/response';
import { Sample } from '../../../interfaces/sample';
import { StorageSample } from '../../../interfaces/storage-sample';
import { StorageSampleService } from '../../../services/storage-sample.service';

@Component({
  selector: 'mvd-related-samples',
  templateUrl: './related-samples.component.html',
  styleUrls: ['./related-samples.component.scss']
})
export class RelatedSamplesComponent implements OnInit {
  @Input() sampleOnEdit : Sample;

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  ////COLUMNAS TABLA
  public columns: Column[] = [
    { title: 'Fecha', col: 'created_at', pipe: "dd/MM/yyyy", class: 'w-10-100' },
    { title: 'Código', col: 'sample_code', class: 'w-15-100' },
    { title: 'Paciente', col: 'patient', class: 'w-30-100'},
    { title: 'Ubicación', col: 'location', class: 'w-30-100'},
    { title: 'Rack', col: 'rack', class: 'w-5-100' },
    { title: 'Caja', col: 'box', class: 'w-5-100' },
    { title: 'Posición', col: 'position', class: 'w-5-100'}
  ]

  totalResut: Observable<number>;
  pageDefault = 1;
  perPage: number = 10;
  orden: string = 'desc';
  filter: string = '';
  result: Observable<ResponsePaginate>;
  subscroption: Subscription;
  dataSource: Observable<any[]>;
  stagesOptions: OptionsSelect[];
  userOptions: { name: string; value: any; }[];
  patient: Patient



  constructor(
    public dialog: MatDialog,
    public storageSampleService: StorageSampleService
  ) {
    this.result = this.storageSampleService.resultItems$

  }

  ngOnInit(): void {

    this.patient = this.sampleOnEdit?.patient
    this.getStorageSamples(this.patient?.id, this.pageDefault, this.perPage, this.filter, this.orden)

  }

  paginatorChange(e: PageEvent) {
    //console.log(e);
    this.getStorageSamples(this.patient?.id, e.pageIndex + 1, e.pageSize)

  }

  getStorageSamples(patient_id, currentPage?, perPage?, filter?, sort?) {
    this.subscroption = this.storageSampleService.getStorageSamples(patient_id, currentPage, perPage, filter, sort).subscribe(next => this.loadData());
  }



  loadData() {

    this.dataSource = this.result.pipe(map(v => {
      const dataTable = v.data.data
      .filter(v=> v.sample_id !== this.sampleOnEdit.id)
      .map(x => {
        return {
          id: x.id,
          sample_id: x.sample_id,
          location_id: x.location_id,
          location: `${x.location?.code} - ${x.location?.description}`,
          patient: `${this.patient.code} - ${this.patient.name} ${this.patient.last_name}`,
          sample_code: x.sample?.code,
          rack: x.rack,
          box: x.box,
          position: x.position,
          created_at: x.created_at,
        }
      })
      return dataTable;
    }))

    this.totalResut = this.result.pipe(map(v => v.data.total))

  }

  // deleteItem(id): Observable<any> {
  //   return this.storageSampleSampleService.deleteStorageSample(id)
  // }

  itemAction(event) {
    //console.log(event);

    if (event.action === "delete") {
      // this.deleteItem(event.element.id).pipe(take(1)).subscribe()
    }
    if (event.action === "edit") {
    }
  }


  ngOnDestroy(): void {
    this.subscroption.unsubscribe()
  }




}
