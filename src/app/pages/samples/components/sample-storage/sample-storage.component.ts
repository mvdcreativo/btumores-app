import { Component, EventEmitter, OnDestroy, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CustomValidators } from 'ngx-custom-validators';
import { Observable, Subscription } from 'rxjs';
import { filter, map, take } from 'rxjs/operators';
import { Patient } from 'src/app/pages/patients/interfaces/patient';
import { Sample } from '../../interfaces/sample';
import { Location } from '../../interfaces/storage-sample';
import { SamplesService } from '../../services/samples.service';
import { StorageSampleService } from '../../services/storage-sample.service';

@Component({
  selector: 'mvd-sample-storage',
  templateUrl: './sample-storage.component.html',
  styleUrls: ['./sample-storage.component.scss'],
})
export class SampleStorageComponent implements OnInit, OnDestroy {

  public form: FormGroup;
  locations: Location[];
  subscriptions: Subscription[] = [];
  racks: any[] = [];
  boxes: any[] = [];
  positions: any[] = [];
  locationSelected :string = ''
  rackSelected:string = ''
  boxSelected:string = ''
  positionSelected:string = ''
  availables$: Observable<any>;
  codeSearch: string;
  sampleOnEdit: Observable<Sample>;
  edit :boolean = false
  patient: Patient;

  constructor(
    private fb: FormBuilder,
    private storageSampleService: StorageSampleService,
    private samplesServices: SamplesService,
  ) {
    this.createForm();
    this.availables$ = this.storageSampleService.availablesPositions$
    this.sampleOnEdit = this.samplesServices.sampleOnEdit
  }

  ngOnInit(): void {
    this.getLocations();

    this.subscriptions.push(
      this.samplesServices.sampleOnEdit.subscribe(res => {
        this.patient = res.patient
        const itemSplit = res.position ? res.position.split(/[L,R,B,P]/) : "";
        // console.log(itemSplit);

        // acc[select] = new Set([...acc[select], +itemSplit[positionSplitCode]]);
        // res.position
      })
    )
  }

  createForm() {
    this.form = this.fb.group({
      location_id: [null, [Validators.required, CustomValidators.number]],
      rack: [{ value: null, disabled: true }, CustomValidators.number],
      box: [{ value: null, disabled: true }, CustomValidators.number],
      position: [{ value: null, disabled: true }, CustomValidators.number],
    });

    if (this.form) {
      this.locationsChanges();

    }
  }
  reLocation(){
    this.edit = true
  }
  getLocations() {
    this.subscriptions.push(
      this.storageSampleService
        .getLocations()
        .subscribe((res) => (this.locations = res.data.data))
    );
  }

  locationsChanges() {
    this.form.get('location_id').valueChanges.subscribe((value) => {
      this.storageSampleService.totalLocationlPositions = this.locations
        .filter((x) => x.id === value)
        .map((v) => {
          let racks = Array(v.racks);
          let boxes = Array(v.boxes);
          let positions = Array(v.positions);

          let combinaciones = [];
          for (let r = 1; r <= racks.length; r++) {
            for (let b = 1; b <= boxes.length; b++) {
              for (let p = 1; p <= positions.length; p++) {
                combinaciones.push(`L${v.id}R${r}B${b}P${p}`);
              }
            }
          }
          // console.log(combinaciones);
          return combinaciones;
        })[0];
      this.locationSelected = `L${value}`
      this.codeSearch = `${this.locationSelected}`
      this.subscriptions.push(
        this.storageSampleService.getAvailablePositions(this.codeSearch).subscribe(res=> {
          this.racks=[]
          this.boxes=[]
          this.positions=[]
          this.form.get('rack').disable()
          this.form.get('rack').setValue(null)
          this.form.get('box').disable()
          this.form.get('box').setValue(null)
          this.form.get('position').disable()
          this.form.get('position').setValue(null)
          this.setRacks()
        })
      )
    })


  }

  racksChanges(e){
      // console.log(value);
      this.boxes=[]
      this.positions=[]
      this.form.get('box').disable()
      this.form.get('box').setValue(null)
      this.form.get('position').disable()
      this.form.get('position').setValue(null)

      this.rackSelected = `R${e.value}`
      this.codeSearch = `${this.locationSelected}${this.rackSelected}`
      this.subscriptions.push(
        this.storageSampleService.getAvailablePositions(this.codeSearch).subscribe(res=> {
          this.setBoxes()
        })
      )


  }

  boxesChanges(e){
    this.positions=[]
    this.form.get('position').disable()
    this.form.get('position').setValue(null)
    this.boxSelected = `B${e.value}`
    this.codeSearch = `${this.locationSelected}${this.rackSelected}${this.boxSelected}`
    this.subscriptions.push(
      this.storageSampleService.getAvailablePositions(this.codeSearch).subscribe(res=> {
        this.setPositions()
     })
    )


  }

  positionChange(e){
    this.positionSelected = `P${e.value}`
    this.codeSearch = `${this.locationSelected}${this.rackSelected}${this.boxSelected}${this.positionSelected}`
    const data = { position : this.codeSearch };
    this.samplesServices.updateSample(data).subscribe(res=> {
      if(res){
        this.subscriptions.push(
          this.storageSampleService.getStorageSamples(this.patient.id, 1, 10).subscribe()

        )
      }
    })
  }


  setRacks() {
    // console.log(value);
    this.availables$.pipe(take(1)).subscribe(
      res => {
        // console.log(res);

        this.racks = this.codeToStorage(res,"racks");
        // console.log(this.racks);

        this.racks.length === 0
          ? this.form.get('rack').disable()
          : this.form.get('rack').enable();

    })
  }

  setBoxes() {
    this.availables$.pipe(take(1)).subscribe(
      res => {
        this.boxes = this.codeToStorage(res,"boxes");
        this.boxes.length === 0
          ? this.form.get('box').disable()
          : this.form.get('box').enable();
    })
  }

  setPositions() {
    this.availables$.pipe(take(1)).subscribe(
      res => {
        this.positions = this.codeToStorage(res,"positions");
        this.positions.length === 0
          ? this.form.get('position').disable()
          : this.form.get('position').enable();
      })
  }

  codeToStorage(value, select) {
    // console.log(value);

    let positionSplitCode;

    switch (select) {
      case 'racks':
        positionSplitCode = 1;
        break;
      case 'boxes':
        positionSplitCode = 2;
        break;
      case 'positions':
        positionSplitCode = 3;
        break;

      default:positionSplitCode = 0
        break;
    }
    const res = value.reduce(
      (acc, item) => {
        const itemSplit = item.split(/[R,B,P]/);
        acc[select] = new Set([...acc[select], +itemSplit[positionSplitCode]]);
        // acc.boxes = new Set([...acc.boxes, +itemSplit[2]])
        // acc.positions = new Set([...acc.positions, +itemSplit[3]])
        return acc;
      },
      { racks: [], boxes: [], positions: [] }
    );

    return [...res[select]];
  }



  ngOnDestroy() {
    this.subscriptions.map((v) => v.unsubscribe());
  }
}
