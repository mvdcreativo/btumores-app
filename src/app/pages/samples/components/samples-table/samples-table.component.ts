import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { Router } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import { map, take } from 'rxjs/operators';
import { Patient } from 'src/app/pages/patients/interfaces/patient';
import { Column } from 'src/app/shared/components/data-table/interfaces/table';
import { ResponsePaginate } from 'src/app/shared/interfaces/response';
import { Sample } from '../../interfaces/sample';
import { SamplesService } from '../../services/samples.service';

@Component({
  selector: 'mvd-samples-table',
  templateUrl: './samples-table.component.html',
  styleUrls: ['./samples-table.component.scss']
})
export class SamplesTableComponent implements OnInit {

  @Input() patient: Patient
  // displayedColumns: string[] = ['code', 'title', 'user_owner.name', 'address', 'neighborhood.name'];

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  ////COLUMNAS TABLA
  public columns: Column[] = [
    { title: 'Código', col: 'code' },
    { title: 'Tipo de Muestra', col: 'type_sample_name' },
    { title: 'Topografia', col: 'topography_name' },
    { title: 'Etapa Actual', col: 'trace_now' },
  ]

  dataSource: Observable<any[]>;
  samples: Sample[];

  ////paginator
  totalResut: Observable<number>;
  pageDefault = 1;
  perPage: number = 10;
  orden: string = 'desc';
  filter: string = '';
  result: Observable<ResponsePaginate>;
  subscriptions: Subscription[]=[];


  paginatorChange(e: PageEvent) {
  //console.log(e);
    this.getSamples(e.pageIndex + 1, e.pageSize)

  }
  /////////////


  constructor(
    private sampleService: SamplesService,
    public router: Router,
  ) {
    this.result = this.sampleService.resultItems$

  }


  ngOnInit(): void {
    this.getSamples(this.pageDefault, this.perPage, this.filter, this.orden, this.patient?.id)

  }


  add(){
    this.router.navigate(['/muestras/muestra'] , { queryParams: { urlReturn: this.router.url, patient_id: this.patient?.id } })
  }


  getSamples(currentPage?, perPage?, filter?, sort?, patient_id?) {
    this.subscriptions.push(
      this.sampleService.getSamples(currentPage, perPage, filter, sort, patient_id).subscribe(next => this.loadData())
    )
  }

  loadData() {

    this.dataSource = this.result.pipe(map(v => {
      console.log(v.data.data);

      const dataTable = v.data.data.map(x => {
        console.log(x?.traces_sample[0]?.stage?.name);

        return {
          id: x.id,
          code: x.code,
          patient_id: x?.patient_id,
          type_sample_name: x?.type_sample?.name,
          type_sample_id: x?.type_sample_id,
          tumor_lineage_id: x?.tumor_lineage_id,
          tumor_lineage_name: x?.tumor_lineage?.name,
          tnm_id: x?.tnm_id,
          topography_id: x?.topography_id,
          topography_name: x?.topography?.name,
          trace_now: x?.traces_sample[0]?.stage?.name,
          traces_sample: x?.traces_sample
        }
      })
      return dataTable;
    }))

    this.totalResut = this.result.pipe(map(v => v.data.total))
  }

  search(filter) {
    this.getSamples(this.pageDefault, this.perPage, filter, this.orden, this.patient?.id )
  }

  deleteItem(id): Observable<any> {
    return this.sampleService.deleteSample(id)
  }

  cloneItem(id): Observable<Sample>{
    return this.sampleService.cloneSample(id)
  }

  itemAction(event) {
  //console.log(event);

    if (event.action === "delete") {
      this.deleteItem(event.element.id).pipe(take(1)).subscribe(res => console.log(res))
    }

    if (event.action === "edit") {
      this.router.navigate(['/muestras/muestra', event.element.id], {queryParams:{urlReturn : this.router.url, patient_id: this.patient?.id}})
    }

    if (event.action === "clone") {
      this.subscriptions.push(
        this.cloneItem(event.element.id).pipe().subscribe(res => {
          this.router.navigate(['/muestras/muestra', res.id], {queryParams:{urlReturn : this.router.url, patient_id: this.patient?.id}})
        })
      )
    }

  }



  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    this.subscriptions.map(v=>v.unsubscribe())
    this.sampleService.setSampleOnEdit(null)
  }

}
