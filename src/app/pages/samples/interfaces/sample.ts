import { Patient } from "../../patients/interfaces/patient";
import { Topography } from "../../topographies/interfaces/topography";
import { StorageSample } from "./storage-sample";

export interface Sample {
    id: number;
    code:string;
    patient_id:number;
    type_sample_id:number;
    tumor_lineage_id:number;
    tnm_id:number;
    topography_id:number;
    patient: Patient;
    type_sample: any; //TypeSample;
    tumor_lineage: any;//TumorLineage;
    tnm:any;//Tnm;
    topography: Topography;
    sample_data: SampleData;
    sample_data_anatomo: SampleDataAnatomo
    position?: string;
    storage_sample?: StorageSample;
    last_stage_status?: boolean

}

export interface SampleData {
    sample_id:number;
    trat_q?:boolean;
    trat_q_date?:string;
    trat_q_criterio?:string;
    trat_q_plan?:string;
    radio_t?:boolean;
    radio_t_date?:string;
    radio_t_criterio?:string;
    radio_t_plan?:string;
    quimio?:boolean;
    quimio_date?:string;
    quimio_criterio?:string;
    quimio_plan?:string;
    homo?:boolean;
    homo_date?:string;
    homo_criterio?:string;
    homo_plan?:string;
    terapia_bio?:boolean;
    terapia_bio_date?:string;
    terapia_bio_criterio?:string;
    terapia_bio_plan?:string;
    inmuno_terapia?:boolean;
    inmuno_terapia_date?:string;
    inmuno_terapia_criterio?:string;
    inmuno_terapia_plan?:string;
    otros?:boolean;
    otros_date?:string;
    otros_criterio?:string;
    otros_plan?:string;
}

export interface SampleDataAnatomo {
    sample_id: number;
    estadio_id?: number;
    anatomia?: boolean;
    anatomia_date?: string;
    biopsia?: boolean;
    reseccion_q?: boolean;
    con_cancer?: boolean;
    type_surgery_id?: number;
    isquemia_min?: number;
    isquemia_seg?: number;
    tacos_cant?: number;
    laminas_cant?: number;
    necrosis_tumoral_cant?: number;
    operacion?: string;
    linfocito_intra_tumoral?:string;
    tnm_t?:string;
    tnm_n?:string;
    tnm_m?:string;

}
