export interface StorageSample {
  id?: number;
  code: string;
  sample_id: number;
  location_id: number;
  rack: number;
  box: number;
  position: number;
  created_at?:string;
  updated_at?:string;
  location: Location;

}

export interface Location {
  id?: number;
  type_location_id: number;
  code: string;
  racks: number;
  boxes: number;
  positions: number;
  description?: string;
  brand_model?: string;
  capacity?: string;
  created_at?:string;
  updated_at?:string;
}

export interface TypeLocation {
  id?: number;
  name: string;
  created_at?:string;
  updated_at?:string;
}

export interface Code {
  code:string
}
