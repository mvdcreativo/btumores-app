

export interface TraceSample {
  id: number;
  sample_id:number;
  stage_id: number;
  user_name:string;
  obs:string;
  date_limit:any;
  user_id:number;
}
