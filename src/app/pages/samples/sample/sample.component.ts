import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import { take } from 'rxjs/operators';
import { ConfirmComponent } from 'src/app/shared/components/modals/confirm/confirm.component';
import { ModalConfirmNoChangesComponent } from 'src/app/shared/components/modals/modal-confirm-no-changes/modal-confirm-no-changes.component';
import { Sample } from '../interfaces/sample';
import { SamplesService } from '../services/samples.service';
import { TraceSampleService } from '../services/trace-sample.service';

@Component({
  selector: 'mvd-sample',
  templateUrl: './sample.component.html',
  styleUrls: ['./sample.component.scss']
})
export class SampleComponent implements OnInit {
  sampleEdit: Sample;
  sampleData: FormGroup;
  identificationData: FormGroup;
  anatomopathologicalData: FormGroup;
  urlReturn: string;
  patient_id: number;
  changes : boolean = false;
  countChanges: number = 0;
  subscriptions: Subscription[]=[];
  status_sample: Observable<any>;

  constructor(
    private sampleService: SamplesService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    public dialog: MatDialog,
    private traceSampleService: TraceSampleService

  ) { }

  ngOnInit(): void {
    this.status_sample = this.traceSampleService.currentStatusSample$

    this.subscriptions.push(
      this.sampleService.sampleOnEdit.subscribe(res=>{
        this.sampleEdit = res


      })
    )

    this.activatedRoute.paramMap.subscribe(
      (params:Params) => {
        if(params.params.id){
        //console.log(params.params.id);

         this.sampleService.getSample(params.params.id).pipe(take(1)).subscribe()
        }

        if(!params.params.id){
          this.sampleEdit = null
        }
      }
    )
    this.activatedRoute.queryParamMap.subscribe(
      (res:Params) => {
        this.urlReturn = res?.params?.urlReturn
        this.patient_id = res?.params?.patient_id
      //console.log(res);

      }
    )

  }

  back(){
    this.countChanges >= 7 ? this.confirmBack() : this.router.navigate([this.urlReturn])

  }
  confirmBack() {

    const dialogRef = this.dialog.open(ModalConfirmNoChangesComponent, {
      width: '250px',
      data: {}
    });

    return dialogRef.afterClosed().subscribe(result => {
      // this.animal = result;
      if(result){
        if (result.value) {
            this.router.navigate([this.urlReturn])
        }
      }

    });
  }

  onSubmit(){


    if(this.identificationData?.value){
      let sample = {...this.identificationData.value};

      if (this.sampleData) {
        const dataBasic = this.sampleData.value;
        sample.sample_data = dataBasic;
      }
      if (this.anatomopathologicalData) {
        const dataAnatomo = this.anatomopathologicalData.value;
        sample.sample_data_anatomo = dataAnatomo;
        sample.tumor_lineage_id = dataAnatomo.tumor_lineage_id
        sample.tnm_id = dataAnatomo.tnm_id
        sample.topography_id = dataAnatomo.topography_id
      }
    //console.log(sample);

      if(!this.sampleEdit){
        this.identificationData.valid ? this.sampleService.storeSample(sample).subscribe(
          res=>{
            this.countChanges = 0
          }
        )
        : this.identificationData.markAllAsTouched()
      }else{
        this.identificationData.valid ? this.sampleService.updateSample(sample).subscribe(res=>{
          this.countChanges = 0
          this.subscriptions.push(

          )
        })
        : this.identificationData.markAllAsTouched();

      }
    }
  }

  sampleDataEvent(e){

    this.sampleData = e;
    this.countChanges ++
  }

  identificationDataEvent(e){

    this.identificationData = e
    this.countChanges ++;

  }
  anatomopathologicalDataEvent(e){

    this.anatomopathologicalData = e
    this.countChanges ++;
  }
  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    this.subscriptions.map(v=>v.unsubscribe())
    this.sampleService.setSampleOnEdit(null)
  }
}
