import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SampleComponent } from './sample/sample.component';
import { SamplesComponent } from './samples.component';

const routes: Routes = [
  { path: '', component: SamplesComponent },
  { path: 'muestra', component: SampleComponent },
  { path: 'muestra/:id', component: SampleComponent },
   
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SamplesRoutingModule { }
