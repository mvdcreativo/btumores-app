import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SamplesRoutingModule } from './samples-routing.module';
import { SamplesComponent } from './samples.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { SampleComponent } from './sample/sample.component';
import { IdentificationSampleComponent } from './components/identification-sample/identification-sample.component';
import { SamplesTableComponent } from './components/samples-table/samples-table.component';
import { AnatomopathologicalComponent } from './components/anatomopathological/anatomopathological.component';
import { SampleDataComponent } from './components/sample-data/sample-data.component';
import { TracesComponent } from './components/traces/traces.component';
import { SampleStorageComponent } from './components/sample-storage/sample-storage.component';
import { RelatedSamplesComponent } from './components/sample-storage/related-samples/related-samples.component';


@NgModule({
  declarations: [
    SamplesComponent,
    SampleComponent,
    IdentificationSampleComponent,
    SamplesTableComponent,
    AnatomopathologicalComponent,
    SampleDataComponent,
    TracesComponent,
    SampleStorageComponent,
    RelatedSamplesComponent
  ],
  imports: [
    CommonModule,
    SamplesRoutingModule,
    SharedModule
  ],
  exports:[
    SamplesTableComponent
  ]
})
export class SamplesModule { }
