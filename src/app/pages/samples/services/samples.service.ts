import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { concatMap, map, take } from 'rxjs/operators';
import { ResponsePaginate, Response } from 'src/app/shared/interfaces/response';
import { UbicationService } from 'src/app/shared/services/ubications/ubication.service';
import { environment } from 'src/environments/environment';
import { Sample } from '../interfaces/sample'
import { TraceSampleService } from './trace-sample.service';

@Injectable({
  providedIn: 'root'
})
export class SamplesService {

  sample: Sample;

  private dataOptionsForms$ : BehaviorSubject<any> = new BehaviorSubject<any>(null)
  public get optionsForms$() {
    return this.dataOptionsForms$
  }

  private sampleEditSubject$ : BehaviorSubject<Sample> = new BehaviorSubject<Sample>(null)
  private urlUtils = "src/app/shared/utils/data/";

  private resultSubject$: BehaviorSubject<ResponsePaginate> = new BehaviorSubject(null)
  public get resultItems$() {
    return this.resultSubject$
  }
  public setItems(value) {
    this.resultSubject$.next(value)
  }



  constructor(
    private http:HttpClient,
    private snackBar: MatSnackBar,
    private traceSampleService: TraceSampleService
  ) {
    this.sampleOnEdit.subscribe( res=> { this.sample = res })
  }

  get sampleOnEdit():Observable<Sample>{
    return this.sampleEditSubject$.asObservable()
  }

  setSampleOnEdit(value){
    this.sampleEditSubject$.next(value)
  }


  storeSample(data): Observable<Sample>{
    return this.http.post<Response>(`${environment.API}samples`, data).pipe(
      map( v => {
        this.setSampleOnEdit(v.data)
        //snacbarr
        this.openSnackBar('Se creó correctamente','success-snack-bar')
        //////////
        return v.data
      })

    )

  }

  updateSample(data): Observable<Sample>{
    const id = this.sampleEditSubject$.value.id
    data._method = "put";
    return this.http.post<Response>(`${environment.API}samples/${id}`, data).pipe(
      map( v => {
        this.setSampleOnEdit(v.data)
        //snacbarr
        this.openSnackBar('Actualizado correctamente','success-snack-bar')
        //////////
        return v.data
      })

    )

  }

  deleteSample(id){
    return this.http.delete<Response>(`${environment.API}samples/${id}`).pipe(
      map( v => {
        // console.log(v.data);

        this.getSamples(1, 20, '', 'desc').pipe(take(1)).subscribe()
        //snacbarr
        this.openSnackBar('Eliminado correctamente','success-snack-bar')
        //////////
        return v.data

      })

    )
  }

  cloneSample(id){
    return this.http.post<Response>(`${environment.API}clone_sample/${id}`,'').pipe(
      map( v => {
        // console.log(v.data);

        // this.getSamples(1, 20, '', 'desc').pipe(take(1)).subscribe()
        //snacbarr
        this.openSnackBar('Eliminado correctamente','success-snack-bar')
        //////////
        return v.data

      })

    )
  }


  getSample(id) : Observable<Sample>{
    return this.http.get<Response>(`${environment.API}samples/${id}`).pipe(map(
      res => {
        this.setSampleOnEdit(res.data)
        console.log(res.data);

        this.traceSampleService.setTraceSampleStatus(res.data?.traces_sample?.stage?.name)
        const resp = res.data
        return resp;
      }
    )

    )
  }


///listar
  getSamples(currentPage = 1, perPage = 20, filter='', sort= 'desc', patient_id= '') : Observable<ResponsePaginate>{
    return this.http.get<ResponsePaginate>(`${environment.API}samples`, {
      params: new HttpParams()
        .set('page', currentPage.toString())
        .set('filter', filter)
        .set('sort', sort)
        .set( 'patient_id', patient_id)
        .set('per_page', perPage.toString())

    }).pipe(map(
      res => {
      //console.log(res);

        this.setSampleOnEdit(null)
        this.setItems(res)
        const resp = res
        return resp;
      }
    )

    )
  }



  openSnackBar(message: string, refClass:string, action: string = '') {
    this.snackBar.open(message, action, {
      duration: 2000,
      horizontalPosition: 'center',
      verticalPosition: 'top',
      panelClass: refClass
    });
  }

  genCode(){
    return this.http.get<Response>(`${environment.API}gen_code_sample`).pipe(map(
      v=> v?.data ? v.data : 0
    ))
  }

  checkExist(campo, value){
    return this.http.get<string>(`${environment.API}code_sample_exist`, {
      params: new HttpParams()
      .set(campo, value)
      .set('not_include_id', this.sample?.id.toString())
    })
  }


}
