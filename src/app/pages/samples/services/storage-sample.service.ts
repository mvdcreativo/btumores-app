import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { ResponsePaginate } from 'src/app/shared/interfaces/response';
import { environment } from 'src/environments/environment';
import { Code } from '../interfaces/storage-sample';

@Injectable({
  providedIn: 'root'
})
export class StorageSampleService {

  private availablesPositionsSubject$ : BehaviorSubject<any> = new BehaviorSubject<any>(null)

  public get availablesPositions$() {
    return this.availablesPositionsSubject$
  }
  public setAvailables(value) {
    this.availablesPositionsSubject$.next(value)
  }

  private resultSubject$: BehaviorSubject<ResponsePaginate> = new BehaviorSubject(null)
  public get resultItems$() {
    return this.resultSubject$
  }
  public setItems(value) {
    this.resultSubject$.next(value)
  }


  totalLocationlPositions: string[] = []
  constructor(
    private http:HttpClient,
    private snackBar: MatSnackBar,
  ) { }


  public getLocations(currentPage = 1, perPage = 1000, filter='', sort= 'desc') : Observable<ResponsePaginate>{
    return this.http.get<ResponsePaginate>(`${environment.API}locations_samples`, {
      params: new HttpParams()
        .set('page', currentPage.toString())
        .set('filter', filter)
        .set('sort', sort)
        .set('per_page', perPage.toString())

    }).pipe(map(
      res => {
      //console.log(res);

        // this.setSampleOnEdit(null)
        // this.setItems(res)
        const resp = res
        return resp;
      }
    ))
  }

  public getStorageSamples(patient_id , currentPage = 1, perPage = 1000, filter='', sort= 'desc') : Observable<ResponsePaginate>{
    return this.http.get<ResponsePaginate>(`${environment.API}storage_samples`, {
      params: new HttpParams()
        .set('page', currentPage.toString())
        .set('filter', filter)
        .set('patient_id', patient_id)
        .set('sort', sort)
        .set('per_page', perPage.toString())

    }).pipe(map(
      res => {
      console.log(res);

        // this.setSampleOnEdit(null)
        this.setItems(res)
        const resp = res
        return resp;
      }
    ))
  }

  getAvailablePositions(code){
    return this.http.get<Code[]>(`${environment.API}not_available_positions`, {
      params: new HttpParams()
        .set('code', code)
    }).pipe(map(
      res => {
        const codes = res.map(v=>v.code);
        const availables= this.totalLocationlPositions.filter(el => {
          if(el.startsWith(code) && !codes.includes(el)){
            return true
          }
        });
        this.setAvailables(availables)
        return codes;
      }
    ))
  }


}
