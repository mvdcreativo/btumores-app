import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { map, take } from 'rxjs/operators';
import { ResponsePaginate, Response } from 'src/app/shared/interfaces/response';
import { environment } from 'src/environments/environment';
import { TraceSample } from '../interfaces/traceSample';

@Injectable({
  providedIn: 'root',
})
export class TraceSampleService {
  private traceSampleEditSubject$: BehaviorSubject<TraceSample> = new BehaviorSubject<TraceSample>(null);
  private urlUtils = 'src/app/shared/utils/data/';

  private resultSubject$: BehaviorSubject<ResponsePaginate> = new BehaviorSubject(null);
  private statusSample$: BehaviorSubject<string> = new BehaviorSubject("");
  public get resultItems$() {
    return this.resultSubject$;
  }
  public setItems(value : ResponsePaginate) {

    this.resultSubject$.next(value);
  }

  constructor(
    private http: HttpClient,
    private snackBar: MatSnackBar
    ) {}

  get traceSampleOnEdit(): Observable<TraceSample> {
    return this.traceSampleEditSubject$;
  }

  get currentStatusSample$():Observable<string>{
    return this.statusSample$
  }

  setTraceSampleStatus(value) {
    this.statusSample$.next(value);
  }
  setTraceSampleOnEdit(value) {
    this.traceSampleEditSubject$.next(value);
  }


  ///listar
  getTraces(
    currentPage = 1,
    perPage = 10,
    filter = '',
    sort = 'desc',
    sample_id = ''
  ): Observable<ResponsePaginate> {
    return this.http
      .get<ResponsePaginate>(`${environment.API}traces_samples`, {
        params: new HttpParams()
          .set('page', currentPage.toString())
          .set('filter', filter)
          .set('sort', sort)
          .set('sample_id', sample_id)
          .set('per_page', perPage.toString()),
      })
      .pipe(
        map((res) => {
          //console.log(res);

          this.statusSample$.next(res.data.data[0]?.stage?.name)
          this.setTraceSampleOnEdit(null);
          this.setItems(res);
          const resp = res;
          return resp;
        })
      );
  }

  getSample(id) : Observable<TraceSample>{
    return this.http.get<Response>(`${environment.API}traces_samples/${id}`).pipe(map(
      res => {
        this.setTraceSampleOnEdit(res.data)
        const resp = res.data
        return resp;
      }
    )

    )
  }

  storeTraceSample(data): Observable<TraceSample> {
    return this.http
      .post<Response>(`${environment.API}traces_samples`, data)
      .pipe(
        map((v) => {
          this.setTraceSampleOnEdit(v.data);
          //snacbarr
          this.openSnackBar('Se creó correctamente', 'success-snack-bar');
          //////////
          return v.data;
        })
      );
  }

  openSnackBar(message: string, refClass: string, action: string = '') {
    this.snackBar.open(message, action, {
      duration: 2000,
      horizontalPosition: 'center',
      verticalPosition: 'top',
      panelClass: refClass,
    });
  }

  updateTrace(data): Observable<TraceSample>{
    data._method = "put";
    return this.http
    .post<Response>(`${environment.API}traces_samples/${data.id}`, data)
    .pipe(
      map((v) => {
        this.setTraceSampleOnEdit(v.data);
        //snacbarr
        this.openSnackBar('Se creó correctamente', 'success-snack-bar');
        //////////
        return v.data;
      })
    );
  }
}
