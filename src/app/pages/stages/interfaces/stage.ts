export interface Stage {
    id?: number;
    name:string;
    end:boolean;
    active:boolean;
}
