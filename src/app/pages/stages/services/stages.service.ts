import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { concatMap, map, take } from 'rxjs/operators';
import { ResponsePaginate, Response } from 'src/app/shared/interfaces/response';
import { UbicationService } from 'src/app/shared/services/ubications/ubication.service';
import { environment } from 'src/environments/environment';
import { Stage } from '../interfaces/stage'

@Injectable({
  providedIn: 'root'
})
export class StagesService {


  private dataOptionsForms$ : BehaviorSubject<any> = new BehaviorSubject<any>(null)
  public get optionsForms$() {
    return this.dataOptionsForms$
  }

  private stageEditSubject$ : BehaviorSubject<Stage> = new BehaviorSubject<Stage>(null)
  private urlUtils = "src/app/shared/utils/data/";

  private resultSubject$: BehaviorSubject<ResponsePaginate> = new BehaviorSubject(null)
  public get resultItems$() {
    return this.resultSubject$
  }
  public setItems(value) {
    this.resultSubject$.next(value)
  }



  constructor(
    private http:HttpClient,
    private snackBar: MatSnackBar,
    private ubicationService: UbicationService,
  ) {
    // this.loadData()
  }

  get stageOnEdit():Observable<Stage>{
    return this.stageEditSubject$
  }

  setStageOnEdit(value){
    this.stageEditSubject$.next(value)
  }

  loadData(){
    const data = of(
      this.ubicationService.getStates(),
      this.ubicationService.getCities(),
      this.ubicationService.getCountries(),
    )

    const dataMerge = data.pipe(
      concatMap(v=>v)
    );

    dataMerge.subscribe(
      res => console.log(res)
    );

  }

  storeStage(data): Observable<Stage>{
    return this.http.post<Response>(`${environment.API}stages`, data).pipe(
      map( v => {
        this.getStages(1, 20, '', 'desc').pipe(take(1)).subscribe()
        //snacbarr
        this.openSnackBar('Se creó correctamente','success-snack-bar')
        //////////
        return v.data
      })

    )

  }

  updateStage(data): Observable<Stage>{
    data._method = "put";
    return this.http.post<Response>(`${environment.API}stages/${data.id}`, data).pipe(
      map( v => {
        this.getStages(1, 20, '', 'desc').pipe(take(1)).subscribe()
        //snacbarr
        this.openSnackBar('Actualizado correctamente','success-snack-bar')
        //////////
        return v.data
      })

    )

  }

  deleteStage(id){
    return this.http.delete<Response>(`${environment.API}stages/${id}`).pipe(
      map( v => {

        this.getStages(1, 20, '', 'desc').pipe(take(1)).subscribe()
        //snacbarr
        this.openSnackBar('Eliminado correctamente','success-snack-bar')
        //////////
        return v.data

      })

    )
  }


  getStage(id) : Observable<Stage>{
    return this.http.get<Response>(`${environment.API}stages/${id}`).pipe(map(
      res => {
        this.setStageOnEdit(res.data)
        const resp = res.data
        return resp;
      }
    )

    )
  }


///listar
  getStages(currentPage = 1, perPage = 20, filter='', sort= 'desc') : Observable<ResponsePaginate>{
    return this.http.get<ResponsePaginate>(`${environment.API}stages`, {
      params: new HttpParams()
        .set('page', currentPage.toString())
        .set('filter', filter)
        .set('sort', sort)
        .set('per_page', perPage.toString())

    }).pipe(map(
      res => {

        this.setStageOnEdit(null)
        this.setItems(res)
        const resp = res
        return resp;
      }
    )

    )
  }



  openSnackBar(message: string, refClass:string, action: string = '') {
    this.snackBar.open(message, action, {
      duration: 2000,
      horizontalPosition: 'center',
      verticalPosition: 'top',
      panelClass: refClass
    });
  }
}
