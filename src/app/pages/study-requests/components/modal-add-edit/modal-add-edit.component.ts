import { Component, OnInit, ChangeDetectionStrategy, Inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDatepickerInputEvent } from '@angular/material/datepicker';
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef } from '@angular/material/dialog';
import moment from 'moment';
import { Observable, Subject, Subscription } from 'rxjs';
import { Patient } from 'src/app/pages/patients/interfaces/patient';
import { PatientsService } from 'src/app/pages/patients/services/patients.service';
import { Sample } from 'src/app/pages/samples/interfaces/sample';
import { ItemOption } from 'src/app/shared/components/input-select-add-item/Interfaces/item-option';
import { ResultsService } from 'src/app/shared/components/results/services/results.service';
import { StudyRequestsService } from '../../services/study-requet.service';
import { debounceTime, map, startWith, take, takeUntil, tap } from 'rxjs/operators';
import { StudyRequest } from '../../interfaces/study-request';
import { AuthService } from 'src/app/auth/auth.service';
import { User } from 'src/app/pages/users/interfaces/user';
import { Router } from '@angular/router';
import { SamplesService } from 'src/app/pages/samples/services/samples.service';
import { ResultComponent } from 'src/app/shared/components/results/result/result.component';
import { GenResultService } from 'src/app/shared/components/results/result/result-study-type/service/gen-result.service';

@Component({
  selector: 'mvd-modal-add-edit',
  templateUrl: './modal-add-edit.component.html',
  styleUrls: ['./modal-add-edit.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ModalAddEditComponent implements OnInit {


  form : FormGroup
  patient: Patient;
  routePatient: string;

  valueTypeStudySelect: string;
  valueFinanciacionSelect: string;
  valueSampleSelect: string | undefined
  valueLabSelect: any;

  financings: any[];
  studies: any[];
  labs: any[];
  samples: any[];

  optionsFinancings: ItemOption[] = [];
  optionsStudies: ItemOption[] = [];
  optionsLabs: ItemOption[] = [];
  optionsSample: ItemOption[] = [];

  edit=true

  subscriptions: Subscription = new Subscription();
  studyRequest$: Observable<StudyRequest>;
  authUser: User;

  patients: Patient[];
  patients$: Observable<Patient[]>;
  destroy$= new Subject();
  valuePatient_id: any;
  disable: boolean = false;
  dataSelects$: Observable<any>;




  constructor(
    public dialogRef: MatDialogRef<ModalAddEditComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private fb:FormBuilder,
    private patientService: PatientsService,
    private resultService: ResultsService,
    private sampleService: SamplesService,
    private studyRequestsService: StudyRequestsService,
    private authService: AuthService,
    private router: Router,
    public dialog: MatDialog,
    private genResultService: GenResultService
  ) {
    this.createForm()
    this.dataSelects$ = this.studyRequestsService.dataSelect()

  }

  ngOnInit(): void {
    this.data?.id ? this.edit = true : this.edit = false


    this.form
        .get('patient_id')
        .valueChanges.pipe(
          takeUntil(this.destroy$),
          debounceTime(500),
          startWith(''),
        )
        .subscribe((val) => {
            this.patients$ = this.patientService.filterPatients(1,20, val).pipe(take(1),map(v=> {
              this.patients = v.data.data

              return v.data.data
            }))
            // return val ? this._filterConsultaCgu(val) : this.consultaCgus;
        });


    if( this.data && Object.keys(this.data).length > 1 ){
      this.patient= this.data?.patient
      this.studyRequest$= this.studyRequestsService.studyRequestOnEdit.pipe(tap(result=> {
        if(result){
          this.updateForm(result)
        }
      }))

    }else{
      if (this.data?.patient_id) {
        this.subscriptions.add(
          this.patientService.patientOnEdit.subscribe(patient=> {
            this.patient = patient
            const data = {...this.data}
            data.patient = patient;
            this.updateForm(data)

          })
        )
      }
      this.subscriptions.add(
        this.authService.currentUser.asObservable().subscribe(user=> {
          if(!this.data?.user_id){
            this.form.patchValue({
              user_id: user.id
            })
          }
        })
      )
    }




  }

  createForm(){
    this.form = this.fb.group({
      id: [null],
      user_id:[null, Validators.required],
      patient_id:[null],
      sample_id:[null],
      date_sol:[null],
      financing_id:[null],
      lab_id:[null],
      study_type_id:[null],
      status_req:["Solicitado"]
    })
    this.formatOptionsSelect()
  }

  updateForm(data?){

    this.patients = [data.patient]

    this.form.patchValue({
      id: data?.id,
      user_id: data?.user_id,
      patient_id: data?.patient_id,
      sample_id: this.data?.sample_id,
      date_sol: data?.date_sol,
      lab_id: data?.lab_id,
      financing_id: data?.financing_id,
      study_type_id: data?.study_type_id,
      status_req: data?.status_req || "Solicitado"

    })
    this.changeLab(data?.lab_id);
    this.changeFinanciacion(data?.financing_id)
    this.changeStudyType(data?.study_type_id)
    this.changeSample(data?.sample_id)
    if(this.data?.result_id){
      this.form.get('user_id').disable();
      this.form.get('patient_id').disable();
      this.form.get('sample_id').disable();
      this.form.get('date_sol').disable();
      this.form.get('lab_id').disable();
      this.form.get('financing_id').disable();
      this.form.get('study_type_id').disable();
      this.disable = true;
    }

  }


  submit(addResult = false){
    const data = { ...this.form.value};
    data.addResult = addResult
    this.dialogRef.close(data);
  }

  formatDate(e : MatDatepickerInputEvent<Date>, campo){
    let brt = e.value;
    this.form.get(campo).setValue(moment(brt).format('YYYY-MM-DD HH:mm:ss'))
  }

  onNoClick(): void {
    this.dialogRef.close();
  }


  changeFinanciacion(value){
    this.form.get('financing_id').setValue(value)
    this.valueFinanciacionSelect = value
  }
  addFinanciacion(value){
    this.subscriptions.add(
      this.resultService.storeFinancings$(value).subscribe()
    )
  }

  changeLab(value){
    this.form.get('lab_id').setValue(value)
    this.valueLabSelect = value

  }
  addLab(value){
    this.subscriptions.add(
      this.resultService.storeLabs$(value).subscribe()
    )
  }


  changeStudyType(value){
    this.form.get('study_type_id').patchValue(value)
    this.valueTypeStudySelect = value
  }

  addStudyType(value){
    this.subscriptions.add(
      this.resultService.storeTypeSudy$(value).subscribe()
    )
  }


  changeSample(value){
    this.form.get('sample_id').setValue(value)
    this.valueSampleSelect = value
  }
  addSample(){
    this.dialogRef.close();
  }


  formatOptionsSelect(){
    this.subscriptions.add(
      this.resultService.typeStudies$.subscribe(items=>{
        this.studies = items,
        this.optionsStudies = items?.map(v=> ( {value: v.id, text: v.name,} ) )
      })
    )
    this.subscriptions.add(
      this.resultService.financing$.subscribe(items=>{
        this.financings = items,
        this.optionsFinancings = items?.map(v=> ( {value: v.id, text: v.name,} ) )
      })
    )
    this.subscriptions.add(
      this.resultService.labs$.subscribe(items=>{
        this.labs = items,
        this.optionsLabs = items?.map(v=> ( {value: v.id, text: v.name,} ) )
      })
    )
  }

  formatOptionsSamples(patient: Patient){
    this.subscriptions.add(
      this.sampleService.getSamples(1,1000,'', 'desc', patient.id.toString()).subscribe(items=>{
        this.samples = items.data.data,
        this.optionsSample = this.samples?.map(v=> ( {value: v.id, text: `${v.code} -- ${v?.type_sample?.name} ${v?.topography?.name ? '-- '+ v?.topography?.name : ''}`,} ) )
      })
    )
  }


  //Patients
  getNamePatient(id: number){

    if (id && this.patients) {
      this.patient = this.patients.find((item) => item.id === id);
      this.routePatient= `/pacientes/paciente/${id}`
      this.formatOptionsSamples(this.patient)
      return `(${this.patient.code}) ${this.patient.name} ${this.patient.last_name}`;
    }

  }
  addPatient(){
    this.router.navigate(['/pacientes/paciente'])
    this.dialogRef.close();
  }

  /////

  openAddResult(){
    this.submit(true);
  }

  openEditResult(){
    const element = {
      patient_id: this.form.get('patient_id').value,
      sample_id: this.form.get('sample_id').value,
    }
    this.subscriptions.add(this.resultService.showResult(this.data.result_id).subscribe())
    this.openDialogResult(this.patient, element)

  }

  openDialogResult(patient, element?) {

    const data = {patient: patient, element: element}
    const dialogRef = this.dialog.open(ResultComponent, {
      width: '70vw',
      minWidth: '370px',
      height: '90vh',
      data: data
    });

    return dialogRef.afterClosed().pipe(take(1)).subscribe(result => {
      // this.animal = result;
      this.resultService.setItem(null)
      this.genResultService.setGenResults(null)
      this.onNoClick()
    });
  }


  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
    this.subscriptions.unsubscribe()
  }

}
