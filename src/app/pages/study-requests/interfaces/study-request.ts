import { Financing, Lab, Result, StudyType } from "src/app/shared/components/results/result/result-study-type/Interfaces/result.interface";
import { Patient } from "../../patients/interfaces/patient";
import { Sample } from "../../samples/interfaces/sample";

export interface StudyRequest {
  id: number;
  user_id: number;
  sample_id: number;
  sample: Sample;
  patient: Patient;
  date_sol: string;
  financing_name: string;
  financing: Financing;
  lab: Lab;
  financing_id: number;
  lab_name: string;
  lab_id: number;
  study_type_id: number;
  study_type_name: string;
  study_type: StudyType;
  patient_full_name: string
  result: Result;
  status_req: string;
}
