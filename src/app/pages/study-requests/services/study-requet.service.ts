import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { concatMap, map, take } from 'rxjs/operators';
import { ResponsePaginate, Response } from 'src/app/shared/interfaces/response';
import { UbicationService } from 'src/app/shared/services/ubications/ubication.service';
import { environment } from 'src/environments/environment';
import { StudyRequest } from '../interfaces/study-request';

@Injectable({
  providedIn: 'root'
})
export class StudyRequestsService {


  private dataOptionsForms$ : BehaviorSubject<any> = new BehaviorSubject<any>(null)
  public get optionsForms$() {
    return this.dataOptionsForms$
  }

  private studyRequestEditSubject$ : BehaviorSubject<StudyRequest> = new BehaviorSubject<StudyRequest>(null)
  private urlUtils = "src/app/shared/utils/data/";

  private resultSubject$: BehaviorSubject<ResponsePaginate> = new BehaviorSubject(null)
  public get resultItems$() {
    return this.resultSubject$
  }
  public setItems(value) {
    this.resultSubject$.next(value)
  }



  constructor(
    private http:HttpClient,
    private snackBar: MatSnackBar,
  ) {
    // this.loadData()
  }

  get studyRequestOnEdit():Observable<StudyRequest>{
    return this.studyRequestEditSubject$
  }

  setStudyRequestOnEdit(value){
    this.studyRequestEditSubject$.next(value)
  }

  storeStudyRequest(data): Observable<StudyRequest>{
    return this.http.post<Response>(`${environment.API}study_requests`, data).pipe(
      map( v => {
        //snacbarr
        this.openSnackBar('Se creó correctamente','success-snack-bar')
        //////////
        return v.data
      })

    )

  }

  updateStudyRequest(data): Observable<StudyRequest>{
    const id = data.id
    data._method = "put";
    return this.http.post<Response>(`${environment.API}study_requests/${id}`, data).pipe(
      map( v => {
        // this.getStudyRequests(1, 10, '', 'desc').pipe(take(1)).subscribe()
        //snacbarr
        this.openSnackBar('Actualizado correctamente','success-snack-bar')
        //////////
        return v.data
      })

    )

  }

  deleteStudyRequest(id){
    return this.http.delete<Response>(`${environment.API}study_requests/${id}`).pipe(
      map( v => {

        // this.getStudyRequests(1, 10, '', 'desc').pipe(take(1)).subscribe()
        //snacbarr
        this.openSnackBar('Eliminado correctamente','success-snack-bar')
        //////////
        return v.data

      })

    )
  }


  getStudyRequest(id) : Observable<StudyRequest>{
    return this.http.get<Response>(`${environment.API}study_requests/${id}`).pipe(map(
      res => {
        const resp = res.data
        return resp;
      }
    )

    )
  }


///listar
  getStudyRequests(currentPage = 1, perPage = 20, filter='', sort= 'desc', patient_id:any='') : Observable<ResponsePaginate>{
    return this.http.get<ResponsePaginate>(`${environment.API}study_requests`, {
      params: new HttpParams()
        .set('page', currentPage.toString())
        .set('filter', filter)
        .set('sort', sort)
        .set('patient_id', patient_id)
        .set('per_page', perPage.toString())

    }).pipe(map(
      res => {

        this.setStudyRequestOnEdit(null)
        this.setItems(res)
        const resp = res
        return resp;
      }
    )

    )
  }

  dataSelect():Observable<any>{
    return this.http.get<any>('assets/data/data_selects.json').pipe(
      map(v=> v)
    )
  }

  openSnackBar(message: string, refClass:string, action: string = '') {
    this.snackBar.open(message, action, {
      duration: 2000,
      horizontalPosition: 'center',
      verticalPosition: 'top',
      panelClass: refClass
    });
  }
}
