import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { StudyRequestsComponent } from './study-requests.component';

const routes: Routes = [{ path: '', component: StudyRequestsComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class StudyRequetsRoutingModule { }
