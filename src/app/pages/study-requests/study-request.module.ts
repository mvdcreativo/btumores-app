import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StudyRequetsRoutingModule } from './study-request-routing.module';
import { StudyRequestsComponent } from './study-requests.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { ModalAddEditComponent } from './components/modal-add-edit/modal-add-edit.component';



@NgModule({
  declarations: [
    StudyRequestsComponent,
    ModalAddEditComponent
  ],
  imports: [
    CommonModule,
    StudyRequetsRoutingModule,
    SharedModule
  ],
  exports: [
    StudyRequestsComponent,
    ModalAddEditComponent
  ]
})
export class StudyRequetsModule { }
