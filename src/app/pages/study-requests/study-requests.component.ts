import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { Observable, Subscription } from 'rxjs';
import { map, take } from 'rxjs/operators';
import { Column } from 'src/app/shared/components/data-table/interfaces/table';
import { ResponsePaginate } from 'src/app/shared/interfaces/response';
import { StudyRequest } from './interfaces/study-request';
import { StudyRequestsService } from './services/study-requet.service';
import { ModalAddEditComponent } from './components/modal-add-edit/modal-add-edit.component';
import { AuthService } from 'src/app/auth/auth.service';
import { User } from '../users/interfaces/user';
import { Patient } from '../patients/interfaces/patient';
import { ResultComponent } from 'src/app/shared/components/results/result/result.component';
import { ResultsService } from 'src/app/shared/components/results/services/results.service';
import { GenResultService } from 'src/app/shared/components/results/result/result-study-type/service/gen-result.service';

@Component({
  selector: 'mvd-study-requests',
  templateUrl: './study-requests.component.html',
  styleUrls: ['./study-requests.component.scss']
})
export class StudyRequestsComponent implements OnInit {

  @Input() typeTab = false;
  @Input() patient_id: number | undefined;

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  ////COLUMNAS TABLA
  public columns: Column[] = [
    { title: 'Fecha', col: 'date_sol', pipe: "dd/MM/yyyy", class: 'w-5-100' },
    { title: 'Paciente', col: 'patient_full_name', class: 'w-30-100' },
    { title: 'Tipo de estudio', col: 'study_type_name', class: 'w-10-100'},
    { title: 'Estado', col: 'status_req', class: 'w-10-100', classCell: 'classCellStatus' },
    { title: 'Financiación', col: 'financing_name', class: 'w-10-100'},
    { title: 'Laboratiorio', col: 'lab_name', class: 'w-20-100' },
    { title: 'Resultado', col: 'result_true' , type: 'boolean', class: 'w-5-100' },
  ]

  totalResut: Observable<number>;
  pageDefault = 1;
  perPage: number = 10;
  orden: string = 'desc';
  filter: string = '';
  result: Observable<ResponsePaginate>;
  subscriptions: Subscription = new Subscription();
  dataSource: Observable<any[]>;
  authUser: User;


  constructor(
    private studyRequestService: StudyRequestsService,
    private resultService: ResultsService,
    public dialog: MatDialog,
    private genResultService: GenResultService
  ) {
    this.result = this.studyRequestService.resultItems$
  }

  ngOnInit(): void {
    this.getStudyRequests(this.pageDefault, this.perPage, this.filter, this.orden)
  }

  paginatorChange(e: PageEvent) {
    //console.log(e);
    this.getStudyRequests(e.pageIndex + 1, e.pageSize)

  }

  getStudyRequests(currentPage = 1, perPage = 20, filter='', sort= 'desc') {
    this.subscriptions.add(
      this.studyRequestService.getStudyRequests(currentPage, perPage, filter, sort, this.patient_id).subscribe(next => this.loadData())
    )
  }


  loadData() {

    this.dataSource = this.result.pipe(map(v => {

      const dataTable = v.data.data.map(x => {

        let classCell;
        switch (x?.status_req) {
          case 'Solicitado':
            classCell = 'danger-word'
            break;
          case 'Pendiente':
            classCell = 'warn-word'
            break;
          case 'Finalizado':
            classCell = 'success-word'
            break;

          default:
            classCell = ''
            break;
        }


        return {

          id: x?.id,
          user_id: x?.user_id,
          sample_id: x?.sample_id,
          sample: x?.sample,
          patient: x?.patient,
          patient_id: x?.patient_id,
          date_sol: x?.date_sol,
          financing_name: x?.financing?.name,
          financing: x?.financing,
          lab: x?.lab,
          financing_id: x?.financing_id,
          lab_name: x?.lab?.name,
          lab_id: x?.lab_id,
          study_type_id: x?.study_type_id,
          study_type_name: x?.study_type?.name,
          study_type: x?.study_type,
          patient_full_name: `(${x?.patient?.code}) ${x?.patient?.name} ${x?.patient?.last_name}`,
          result: x?.result,
          result_id: x?.result?.id,
          result_true: x?.result?.id ? 1 : 0,
          status_req: x?.status_req,
          classCellStatus: classCell

        }
      })
      return dataTable;
    }))

    this.totalResut = this.result.pipe(map(v => v.data.total))
  }

  search(filter) {
    this.getStudyRequests(this.pageDefault, this.perPage, filter, this.orden)
  }


  deleteItem(id): Observable<any> {
    return this.studyRequestService.deleteStudyRequest(id)
  }

  itemAction(event) {
    //console.log(event);

    if (event.action === "delete") {
      this.subscriptions.add(
        this.deleteItem(event.element.id).pipe(take(1)).subscribe()
      )
    }
    if (event.action === "edit") {
      this.studyRequestService.setStudyRequestOnEdit(event.element)
      this.openDialog(event.element)
    }
  }

  openDialog(data?) {


    const dataDielog =  this.patient_id && !data ? { patient_id: this.patient_id } : data;

    const dialogRef = this.dialog.open(ModalAddEditComponent, {
      width: '950px',
      data: dataDielog
    });

    return dialogRef.afterClosed().pipe(take(1)).subscribe(dataSubmit => {
      // this.animal = dataSubmit;
      if (dataSubmit) {
        if (dataSubmit.id) {
          //console.log(dataSubmit);
          if(dataSubmit.addResult){
            this.updateStudyRequest(dataSubmit,true)
          }else{
            this.updateStudyRequest(dataSubmit)
          }

        } else {
          if(dataSubmit.addResult){
            this.storeStudyRequest(dataSubmit, true)
          }else{
            this.storeStudyRequest(dataSubmit)
          }
        }

      }else{
        this.getStudyRequests(this.pageDefault, this.perPage, this.filter, this.orden)
      }

    });
  }

  updateStudyRequest(data, addResult?) {
    this.studyRequestService.updateStudyRequest(data).pipe(take(1)).subscribe(
      res => {
        this.getStudyRequests(this.pageDefault, this.perPage, this.filter, this.orden)
        if (addResult) {
          this.openAddResult(res)
        }
      }
    )
  }

  storeStudyRequest(data, addResult?) {
    this.studyRequestService.storeStudyRequest(data).pipe(take(1)).subscribe(
      res => {
        this.getStudyRequests(this.pageDefault, this.perPage, this.filter, this.orden)
        if (addResult) {
          this.openAddResult(res)
        }
      }
    )
  }

  ngOnDestroy(): void {
    this.subscriptions.unsubscribe()
  }



  openDialogResult(patient, element?) {

    const data = {patient: patient, element: element}
    const dialogRef = this.dialog.open(ResultComponent, {
      width: '70vw',
      minWidth: '370px',
      height: '90vh',
      data: data
    });

    return dialogRef.afterClosed().pipe(take(1)).subscribe(result => {
      // this.animal = result;
      this.resultService.setItem(null)
      this.genResultService.setGenResults(null)

    });
  }


  openAddResult(data){
    const element = {
      patient_id: data.patient_id,
      sample_id: data.sample_id,
      date_sol: data.date_sol,
      financing_id:data.financing_id,
      lab_id: data.lab_id,
      study_type_id: data.study_type_id,
      study_request_id: data.id,
    }
    this.openDialogResult(data.patient, element)
  }

}
