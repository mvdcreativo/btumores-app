import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { concatMap, map, take } from 'rxjs/operators';
import { ResponsePaginate, Response } from 'src/app/shared/interfaces/response';
import { UbicationService } from 'src/app/shared/services/ubications/ubication.service';
import { environment } from 'src/environments/environment';
import { Surgery } from '../interfaces/surgery'

@Injectable({
  providedIn: 'root'
})
export class SurgeriesService {


  private dataOptionsForms$ : BehaviorSubject<any> = new BehaviorSubject<any>(null)
  public get optionsForms$() {
    return this.dataOptionsForms$
  }

  private surgeryEditSubject$ : BehaviorSubject<Surgery> = new BehaviorSubject<Surgery>(null)
  private urlUtils = "src/app/shared/utils/data/";

  private resultSubject$: BehaviorSubject<ResponsePaginate> = new BehaviorSubject(null)
  public get resultItems$() {
    return this.resultSubject$
  }
  public setItems(value) {
    this.resultSubject$.next(value)
  }



  constructor(
    private http:HttpClient,
    private snackBar: MatSnackBar,
    private ubicationService: UbicationService,
  ) {
    // this.loadData()
  }

  get surgeryOnEdit():Observable<Surgery>{
    return this.surgeryEditSubject$
  }

  setSurgeryOnEdit(value){
    this.surgeryEditSubject$.next(value)
  }

  storeSurgery(data): Observable<Surgery>{
    return this.http.post<Response>(`${environment.API}surgeries`, data).pipe(
      map( v => {
        this.getSurgeries(1, 10, '', 'desc').pipe(take(1)).subscribe()
        //snacbarr
        this.openSnackBar('Se creó correctamente','success-snack-bar')
        //////////
        return v.data
      })

    )

  }

  updateSurgery(data): Observable<Surgery>{
    const id = data.id
    data._method = "put";
    return this.http.post<Response>(`${environment.API}surgeries/${id}`, data).pipe(
      map( v => {
        this.getSurgeries(1, 10, '', 'desc').pipe(take(1)).subscribe()
        //snacbarr
        this.openSnackBar('Actualizado correctamente','success-snack-bar')
        //////////
        return v.data
      })

    )

  }

  deleteSurgery(id){
    return this.http.delete<Response>(`${environment.API}surgeries/${id}`).pipe(
      map( v => {

        this.getSurgeries(1, 10, '', 'desc').pipe(take(1)).subscribe()
        //snacbarr
        this.openSnackBar('Eliminado correctamente','success-snack-bar')
        //////////
        return v.data

      })

    )
  }


  getSurgery(id) : Observable<Surgery>{
    return this.http.get<Response>(`${environment.API}surgeries/${id}`).pipe(map(
      res => {
        const resp = res.data
        return resp;
      }
    )

    )
  }


///listar
  getSurgeries(currentPage = 1, perPage = 20, filter='', sort= 'desc') : Observable<ResponsePaginate>{
    return this.http.get<ResponsePaginate>(`${environment.API}surgeries`, {
      params: new HttpParams()
        .set('page', currentPage.toString())
        .set('filter', filter)
        .set('sort', sort)
        .set('per_page', perPage.toString())

    }).pipe(map(
      res => {

        this.setSurgeryOnEdit(null)
        this.setItems(res)
        const resp = res
        return resp;
      }
    )

    )
  }



  openSnackBar(message: string, refClass:string, action: string = '') {
    this.snackBar.open(message, action, {
      duration: 2000,
      horizontalPosition: 'center',
      verticalPosition: 'top',
      panelClass: refClass
    });
  }
}
