import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SurgeriesRoutingModule } from './surgeries-routing.module';
import { SurgeriesComponent } from './surgeries.component';
import { SharedModule } from 'src/app/shared/shared.module';


@NgModule({
  declarations: [SurgeriesComponent],
  imports: [
    CommonModule,
    SurgeriesRoutingModule,
    SharedModule
  ]
})
export class SurgeriesModule { }
