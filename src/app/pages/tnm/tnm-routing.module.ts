import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TnmComponent } from './tnm.component';

const routes: Routes = [{ path: '', component: TnmComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TnmRoutingModule { }
