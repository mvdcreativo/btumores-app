import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TnmComponent } from './tnm.component';

describe('TnmComponent', () => {
  let component: TnmComponent;
  let fixture: ComponentFixture<TnmComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TnmComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TnmComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
