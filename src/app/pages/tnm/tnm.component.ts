import { Component, OnInit, ViewChild } from '@angular/core';
import { Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { Observable, Subscription } from 'rxjs';
import { map, take } from 'rxjs/operators';
import { Column } from 'src/app/shared/components/data-table/interfaces/table';
import { ModalReutilComponent } from 'src/app/shared/components/modals/modal-reutil/modal-reutil.component';
import { ResponsePaginate } from 'src/app/shared/interfaces/response';
import { Tnm } from './interfaces/tnm';
import { TnmService } from './services/tnm.service';

@Component({
  selector: 'mvd-tnm',
  templateUrl: './tnm.component.html',
  styleUrls: ['./tnm.component.scss']
})
export class TnmComponent implements OnInit {



  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  ////COLUMNAS TABLA
  public columns: Column[] = [
    { title: 'Nombre', col: 'name' },

  ]

  totalResut: Observable<number>;
  pageDefault = 1;
  perPage: number = 10;
  orden: string = 'desc';
  filter: string = '';
  result: Observable<ResponsePaginate>;
  subscroption: Subscription;
  dataSource: Observable<any[]>;


  constructor(
    private tnmService: TnmService,
    public dialog: MatDialog,

  ) {
    this.result = this.tnmService.resultItems$

  }

  ngOnInit(): void {

    this.getTnms(this.pageDefault, this.perPage, this.filter, this.orden)

  }

  paginatorChange(e: PageEvent) {
    //console.log(e);
    this.getTnms(e.pageIndex + 1, e.pageSize)

  }

  getTnms(currentPage?, perPage?, filter?, sort?) {
    this.subscroption = this.tnmService.getTnms(currentPage, perPage, filter, sort).subscribe(next => this.loadData());
  }


  loadData() {

    this.dataSource = this.result.pipe(map(v => {
      const dataTable = v.data.data.map(x => {
        return {
          id: x.id,
          name: x.name,

        }
      })
      return dataTable;
    }))

    this.totalResut = this.result.pipe(map(v => v.data.total))
  }

  search(filter) {
    this.getTnms(this.pageDefault, this.perPage, filter, this.orden)
  }


  deleteItem(id): Observable<any> {
    return this.tnmService.deleteTnm(id)
  }

  itemAction(event) {
    //console.log(event);

    if (event.action === "delete") {
      this.deleteItem(event.element.id).pipe(take(1)).subscribe()
    }
    if (event.action === "edit") {
      this.openDialog('Edita Tnm',event.element)
    }
  }

  openDialog(title_form = 'Agrega Tnm', data?) {


    const dataDielog = { title: title_form, data: this.setFields(data) };

    const dialogRef = this.dialog.open(ModalReutilComponent, {
      width: '550px',
      data: dataDielog
    });

    return dialogRef.afterClosed().subscribe(result => {
      // this.animal = result;
      if (result) {
        if (result.id) {
          //console.log(result);

          this.updateTnm(result)

        } else {
          this.storeTnm(result)
        }

      }

    });
  }

  updateTnm(data) {
    this.tnmService.updateTnm(data).pipe(take(1)).subscribe()
  }

  storeTnm(data) {
    this.tnmService.storeTnm(data).pipe(take(1)).subscribe(
      res => {
        //console.log(res);
      }
    )
  }

  setFields(elementEdit? : Tnm) {
    // console.log(elementEdit);

    const fields = [
      { nameControl: 'id', type: 'hidden', value: elementEdit?.id, label: 'Id' },
      { nameControl: 'name', type: 'text', value: elementEdit?.name, label: 'Nombre', class:'mvd-col1--1', validators: [Validators.required] },
    ]

    return fields
  }

  ngOnDestroy(): void {
    this.subscroption.unsubscribe()
  }

}
