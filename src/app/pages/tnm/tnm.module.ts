import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TnmRoutingModule } from './tnm-routing.module';
import { TnmComponent } from './tnm.component';
import { SharedModule } from 'src/app/shared/shared.module';


@NgModule({
  declarations: [TnmComponent],
  imports: [
    CommonModule,
    TnmRoutingModule,
    SharedModule
  ]
})
export class TnmModule { }
