import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { concatMap, map, take } from 'rxjs/operators';
import { ResponsePaginate, Response } from 'src/app/shared/interfaces/response';
import { UbicationService } from 'src/app/shared/services/ubications/ubication.service';
import { environment } from 'src/environments/environment';
import { Topography } from '../interfaces/topography'

@Injectable({
  providedIn: 'root'
})
export class TopographiesService {


  private dataOptionsForms$ : BehaviorSubject<any> = new BehaviorSubject<any>(null)
  public get optionsForms$() {
    return this.dataOptionsForms$
  }

  private topographyEditSubject$ : BehaviorSubject<Topography> = new BehaviorSubject<Topography>(null)
  private urlUtils = "src/app/shared/utils/data/";

  private resultSubject$: BehaviorSubject<ResponsePaginate> = new BehaviorSubject(null)
  private listTopographiesSubject$: BehaviorSubject<Topography[]> = new BehaviorSubject([])

  public get resultItems$() {
    return this.resultSubject$
  }
  public setItems(value) {
    this.resultSubject$.next(value)
  }


  public get listTopographiesItems$() {
    if (!this.listTopographiesSubject$.getValue().length) {
      this.getTopographies(1, 1000, '', 'desc').pipe(take(1)).subscribe()
    }
    return this.listTopographiesSubject$.asObservable()
  }
  public setListTopographiesItems(value) {
    this.listTopographiesSubject$.next(value)
  }


  constructor(
    private http:HttpClient,
    private snackBar: MatSnackBar,
    private ubicationService: UbicationService,
  ) {
    // this.loadData()
  }

  get topographyOnEdit():Observable<Topography>{
    return this.topographyEditSubject$
  }

  setTopographyOnEdit(value){
    this.topographyEditSubject$.next(value)
  }

  storeTopography(data): Observable<Topography>{
    return this.http.post<Response>(`${environment.API}topographies`, data).pipe(
      map( v => {
        this.getTopographies(1, 10, '', 'desc').pipe(take(1)).subscribe()
        //snacbarr
        this.openSnackBar('Se creó correctamente','success-snack-bar')
        //////////
        return v.data
      })

    )

  }

  updateTopography(data): Observable<Topography>{
    const id = data.id
    data._method = "put";
    return this.http.post<Response>(`${environment.API}topographies/${id}`, data).pipe(
      map( v => {
        this.getTopographies(1, 10, '', 'desc').pipe(take(1)).subscribe()
        //snacbarr
        this.openSnackBar('Actualizado correctamente','success-snack-bar')
        //////////
        return v.data
      })

    )

  }

  deleteTopography(id){
    return this.http.delete<Response>(`${environment.API}topographies/${id}`).pipe(
      map( v => {

        this.getTopographies(1, 10, '', 'desc').pipe(take(1)).subscribe()
        //snacbarr
        this.openSnackBar('Eliminado correctamente','success-snack-bar')
        //////////
        return v.data

      })

    )
  }


  getTopography(id) : Observable<Topography>{
    return this.http.get<Response>(`${environment.API}topographies/${id}`).pipe(map(
      res => {
        const resp = res.data
        return resp;
      }
    )

    )
  }


///listar
  getTopographies(currentPage = 1, perPage = 20, filter='', sort= 'desc') : Observable<ResponsePaginate>{
    return this.http.get<ResponsePaginate>(`${environment.API}topographies`, {
      params: new HttpParams()
        .set('page', currentPage.toString())
        .set('filter', filter)
        .set('sort', sort)
        .set('per_page', perPage.toString())

    }).pipe(map(
      res => {

        this.setTopographyOnEdit(null)

        this.setListTopographiesItems(res?.data?.data)
        this.setItems(res)
        const resp = res
        return resp;
      }
    )

    )
  }



  openSnackBar(message: string, refClass:string, action: string = '') {
    this.snackBar.open(message, action, {
      duration: 2000,
      horizontalPosition: 'center',
      verticalPosition: 'top',
      panelClass: refClass
    });
  }
}
