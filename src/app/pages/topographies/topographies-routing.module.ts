import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TopographiesComponent } from './topographies.component';

const routes: Routes = [{ path: '', component: TopographiesComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TopographiesRoutingModule { }
