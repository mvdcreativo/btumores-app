import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TopographiesComponent } from './topographies.component';

describe('TopographiesComponent', () => {
  let component: TopographiesComponent;
  let fixture: ComponentFixture<TopographiesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TopographiesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TopographiesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
