import { Component, OnInit, ViewChild } from '@angular/core';
import { Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { Observable, Subscription } from 'rxjs';
import { map, take } from 'rxjs/operators';
import { Column } from 'src/app/shared/components/data-table/interfaces/table';
import { ModalReutilComponent } from 'src/app/shared/components/modals/modal-reutil/modal-reutil.component';
import { ResponsePaginate } from 'src/app/shared/interfaces/response';
import { Topography } from './interfaces/topography';
import { TopographiesService } from './services/topography.service';

@Component({
  selector: 'mvd-topographies',
  templateUrl: './topographies.component.html',
  styleUrls: ['./topographies.component.scss']
})
export class TopographiesComponent implements OnInit {



  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  ////COLUMNAS TABLA
  public columns: Column[] = [
    { title: 'Nombre', col: 'name' },
    { title: 'Cie10', col: 'cie10' },

  ]

  totalResut: Observable<number>;
  pageDefault = 1;
  perPage: number = 10;
  orden: string = 'desc';
  filter: string = '';
  result: Observable<ResponsePaginate>;
  subscroption: Subscription;
  dataSource: Observable<any[]>;


  constructor(
    private topographyService: TopographiesService,
    public dialog: MatDialog,

  ) {
    this.result = this.topographyService.resultItems$

  }

  ngOnInit(): void {

    this.getTopographies(this.pageDefault, this.perPage, this.filter, this.orden)

  }

  paginatorChange(e: PageEvent) {
    //console.log(e);
    this.getTopographies(e.pageIndex + 1, e.pageSize)

  }

  getTopographies(currentPage?, perPage?, filter?, sort?) {
    this.subscroption = this.topographyService.getTopographies(currentPage, perPage, filter, sort).subscribe(next => this.loadData());
  }


  loadData() {

    this.dataSource = this.result.pipe(map(v => {
      const dataTable = v.data.data.map(x => {
        return {
          id: x.id,
          name: x.name,
          cie10: x.cie10

        }
      })
      return dataTable;
    }))

    this.totalResut = this.result.pipe(map(v => v.data.total))
  }

  search(filter) {
    this.getTopographies(this.pageDefault, this.perPage, filter, this.orden)
  }


  deleteItem(id): Observable<any> {
    return this.topographyService.deleteTopography(id)
  }

  itemAction(event) {
    //console.log(event);

    if (event.action === "delete") {
      this.deleteItem(event.element.id).pipe(take(1)).subscribe()
    }
    if (event.action === "edit") {
      this.openDialog('Edita Topografía',event.element)
    }
  }

  openDialog(title_form = 'Agrega Topografía', data?) {


    const dataDielog = { title: title_form, data: this.setFields(data) };

    const dialogRef = this.dialog.open(ModalReutilComponent, {
      width: '550px',
      data: dataDielog
    });

    return dialogRef.afterClosed().subscribe(result => {
      // this.animal = result;
      if (result) {
        if (result.id) {
          //console.log(result);

          this.updateTopography(result)

        } else {
          this.storeTopography(result)
        }

      }

    });
  }

  updateTopography(data) {
    this.topographyService.updateTopography(data).pipe(take(1)).subscribe()
  }

  storeTopography(data) {
    this.topographyService.storeTopography(data).pipe(take(1)).subscribe(
      res => {
        //console.log(res);
      }
    )
  }

  setFields(elementEdit? : Topography) {
    // console.log(elementEdit);

    const fields = [
      { nameControl: 'id', type: 'hidden', value: elementEdit?.id, label: 'Id' },
      { nameControl: 'name', type: 'text', value: elementEdit?.name, label: 'Nombre', class:'mvd-col1--1', validators: [Validators.required]},
      { nameControl: 'cie10', type: 'text', value: elementEdit?.cie10, label: 'Cie10', class:'mvd-col1--1', validators: [Validators.required]},
]

    return fields
  }

  ngOnDestroy(): void {
    this.subscroption.unsubscribe()
  }


}
