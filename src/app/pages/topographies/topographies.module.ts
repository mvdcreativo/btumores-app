import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TopographiesRoutingModule } from './topographies-routing.module';
import { TopographiesComponent } from './topographies.component';
import { SharedModule } from 'src/app/shared/shared.module';


@NgModule({
  declarations: [TopographiesComponent],
  imports: [
    CommonModule,
    TopographiesRoutingModule,
    SharedModule
  ]
})
export class TopographiesModule { }
