import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { map, take } from 'rxjs/operators';
import { ResponsePaginate, Response } from 'src/app/shared/interfaces/response';
import { environment } from 'src/environments/environment';
import { TumorLineage } from '../interfaces/tumor-lineage'

@Injectable({
  providedIn: 'root'
})
export class TumorLineageService {


  private dataOptionsForms$ : BehaviorSubject<any> = new BehaviorSubject<any>(null)
  public get optionsForms$() {
    return this.dataOptionsForms$
  }

  private tumorLineageEditSubject$ : BehaviorSubject<TumorLineage> = new BehaviorSubject<TumorLineage>(null)
  private urlUtils = "src/app/shared/utils/data/";

  private resultSubject$: BehaviorSubject<ResponsePaginate> = new BehaviorSubject(null)
  public get resultItems$() {
    return this.resultSubject$
  }
  public setItems(value) {
    this.resultSubject$.next(value)
  }


  private listTumorLineageSubject$: BehaviorSubject<TumorLineage[]> = new BehaviorSubject([])

  public get listTumorLineageItems$() {
    if (!this.listTumorLineageSubject$.getValue().length) {
      this.getTumorLineages(1, 1000, '', 'desc').pipe(take(1)).subscribe()
    }
    return this.listTumorLineageSubject$.asObservable()
  }
  public setListTumorLineageItems(value) {
    this.listTumorLineageSubject$.next(value)
  }


  constructor(
    private http:HttpClient,
    private snackBar: MatSnackBar,
  ) {
    // this.loadData()
  }

  get tumorLineageOnEdit():Observable<TumorLineage>{
    return this.tumorLineageEditSubject$
  }

  setTumorLineageOnEdit(value){
    this.tumorLineageEditSubject$.next(value)
  }

  storeTumorLineage(data): Observable<TumorLineage>{
    return this.http.post<Response>(`${environment.API}tumor_lineages`, data).pipe(
      map( v => {
        this.getTumorLineages(1, 10, '', 'desc').pipe(take(1)).subscribe()
        //snacbarr
        this.openSnackBar('Se creó correctamente','success-snack-bar')
        //////////
        return v.data
      })

    )

  }

  updateTumorLineage(data): Observable<TumorLineage>{
    const id = data.id
    data._method = "put";
    return this.http.post<Response>(`${environment.API}tumor_lineages/${id}`, data).pipe(
      map( v => {
        this.getTumorLineages(1, 10, '', 'desc').pipe(take(1)).subscribe()
        //snacbarr
        this.openSnackBar('Actualizado correctamente','success-snack-bar')
        //////////
        return v.data
      })

    )

  }

  deleteTumorLineage(id){
    return this.http.delete<Response>(`${environment.API}tumor_lineages/${id}`).pipe(
      map( v => {

        this.getTumorLineages(1, 10, '', 'desc').pipe(take(1)).subscribe()
        //snacbarr
        this.openSnackBar('Eliminado correctamente','success-snack-bar')
        //////////
        return v.data

      })

    )
  }


  getTumorLineage(id) : Observable<TumorLineage>{
    return this.http.get<Response>(`${environment.API}tumor_lineages/${id}`).pipe(map(
      res => {
        const resp = res.data
        return resp;
      }
    )

    )
  }


///listar
  getTumorLineages(currentPage = 1, perPage = 20, filter='', sort= 'desc') : Observable<ResponsePaginate>{
    return this.http.get<ResponsePaginate>(`${environment.API}tumor_lineages`, {
      params: new HttpParams()
        .set('page', currentPage.toString())
        .set('filter', filter)
        .set('sort', sort)
        .set('per_page', perPage.toString())

    }).pipe(map(
      res => {

        this.setTumorLineageOnEdit(null)
        this.setListTumorLineageItems(res?.data?.data)
        this.setItems(res)
        const resp = res
        return resp;
      }
    )

    )
  }



  openSnackBar(message: string, refClass:string, action: string = '') {
    this.snackBar.open(message, action, {
      duration: 2000,
      horizontalPosition: 'center',
      verticalPosition: 'top',
      panelClass: refClass
    });
  }
}
