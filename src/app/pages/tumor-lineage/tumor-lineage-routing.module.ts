import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TumorLineageComponent } from './tumor-lineage.component';

const routes: Routes = [{ path: '', component: TumorLineageComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TumorLineageRoutingModule { }
