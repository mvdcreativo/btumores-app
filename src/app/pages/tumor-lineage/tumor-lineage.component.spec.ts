import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TumorLineageComponent } from './tumor-lineage.component';

describe('TumorLineageComponent', () => {
  let component: TumorLineageComponent;
  let fixture: ComponentFixture<TumorLineageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TumorLineageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TumorLineageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
