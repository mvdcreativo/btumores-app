import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TumorLineageRoutingModule } from './tumor-lineage-routing.module';
import { TumorLineageComponent } from './tumor-lineage.component';
import { SharedModule } from 'src/app/shared/shared.module';


@NgModule({
  declarations: [TumorLineageComponent],
  imports: [
    CommonModule,
    TumorLineageRoutingModule,
    SharedModule
  ]
})
export class TumorLineageModule { }
