import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TypeLocationSampleComponent } from './type-location-sample.component';

const routes: Routes = [{ path: '', component: TypeLocationSampleComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TypeLocationSampleRoutingModule { }
