import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TypeLocationSampleComponent } from './type-location-sample.component';

describe('TypeLocationSampleComponent', () => {
  let component: TypeLocationSampleComponent;
  let fixture: ComponentFixture<TypeLocationSampleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TypeLocationSampleComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TypeLocationSampleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
