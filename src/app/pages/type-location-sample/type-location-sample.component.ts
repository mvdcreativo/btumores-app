import { Component, OnInit, ViewChild } from '@angular/core';
import { Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { Observable, Subscription } from 'rxjs';
import { map, take } from 'rxjs/operators';
import { Column } from 'src/app/shared/components/data-table/interfaces/table';
import { ModalReutilComponent } from 'src/app/shared/components/modals/modal-reutil/modal-reutil.component';
import { ResponsePaginate } from 'src/app/shared/interfaces/response';
import { TypeLocation } from '../samples/interfaces/storage-sample';
import { TypeLocationSamplesService } from './services/type-location-sample.service';

@Component({
  selector: 'mvd-type-location-sample',
  templateUrl: './type-location-sample.component.html',
  styleUrls: ['./type-location-sample.component.scss']
})
export class TypeLocationSampleComponent implements OnInit {


  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  ////COLUMNAS TABLA
  public columns: Column[] = [
    { title: 'Nombre', col: 'name' },

  ]

  totalResut: Observable<number>;
  pageDefault = 1;
  perPage: number = 10;
  orden: string = 'desc';
  filter: string = '';
  result: Observable<ResponsePaginate>;
  subscroption: Subscription;
  dataSource: Observable<any[]>;


  constructor(
    private typeLocationSampleService: TypeLocationSamplesService,
    public dialog: MatDialog,

  ) {
    this.result = this.typeLocationSampleService.resultItems$

  }

  ngOnInit(): void {

    this.getTypeLocationSamples(this.pageDefault, this.perPage, this.filter, this.orden)

  }

  paginatorChange(e: PageEvent) {
    //console.log(e);
    this.getTypeLocationSamples(e.pageIndex + 1, e.pageSize)

  }

  getTypeLocationSamples(currentPage?, perPage?, filter?, sort?) {
    this.subscroption = this.typeLocationSampleService.getTypeLocationSamples(currentPage, perPage, filter, sort).subscribe(next => this.loadData());
  }


  loadData() {

    this.dataSource = this.result.pipe(map(v => {
      const dataTable = v.data.data.map(x => {
        return {
          id: x.id,
          name: x.name,

        }
      })
      return dataTable;
    }))

    this.totalResut = this.result.pipe(map(v => v.data.total))
  }

  search(filter) {
    this.getTypeLocationSamples(this.pageDefault, this.perPage, filter, this.orden)
  }


  deleteItem(id): Observable<any> {
    return this.typeLocationSampleService.deleteTypeLocationSample(id)
  }

  itemAction(event) {
    //console.log(event);

    if (event.action === "delete") {
      this.deleteItem(event.element.id).pipe(take(1)).subscribe()
    }
    if (event.action === "edit") {
      this.openDialog('Edita Tipo de Almacén de Muestras',event.element)
    }
  }

  openDialog(title_form = 'Agrega Tipo de Almacén de Muestras', data?) {


    const dataDielog = { title: title_form, data: this.setFields(data) };

    const dialogRef = this.dialog.open(ModalReutilComponent, {
      width: '550px',
      data: dataDielog
    });

    return dialogRef.afterClosed().subscribe(result => {
      // this.animal = result;
      if (result) {
        if (result.id) {
          //console.log(result);

          this.updateTypeLocationSample(result)

        } else {
          this.storeTypeLocationSample(result)
        }

      }

    });
  }

  updateTypeLocationSample(data) {
    this.typeLocationSampleService.updateTypeLocationSample(data).pipe(take(1)).subscribe()
  }

  storeTypeLocationSample(data) {
    this.typeLocationSampleService.storeTypeLocationSample(data).pipe(take(1)).subscribe(
      res => {
        //console.log(res);
      }
    )
  }

  setFields(elementEdit? : TypeLocation) {
    // console.log(elementEdit);

    const fields = [
      { nameControl: 'id', type: 'hidden', value: elementEdit?.id, label: 'Id' },
      { nameControl: 'name', type: 'text', value: elementEdit?.name, validators: [Validators.required], label: 'Nombre', class:'mvd-col1--1' },
    ]

    return fields
  }

  ngOnDestroy(): void {
    this.subscroption.unsubscribe()
  }
}
