import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TypeLocationSampleRoutingModule } from './type-location-sample-routing.module';
import { TypeLocationSampleComponent } from './type-location-sample.component';
import { SharedModule } from 'src/app/shared/shared.module';


@NgModule({
  declarations: [
    TypeLocationSampleComponent
  ],
  imports: [
    CommonModule,
    TypeLocationSampleRoutingModule,
    SharedModule
  ]
})
export class TypeLocationSampleModule { }
