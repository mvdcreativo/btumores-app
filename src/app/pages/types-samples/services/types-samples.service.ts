import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { map, take } from 'rxjs/operators';
import { ResponsePaginate, Response } from 'src/app/shared/interfaces/response';
import { environment } from 'src/environments/environment';
import { TypeSample } from '../interfaces/type-sample'

@Injectable({
  providedIn: 'root'
})
export class TypeSamplesService {


  private dataOptionsForms$ : BehaviorSubject<any> = new BehaviorSubject<any>(null)
  public get optionsForms$() {
    return this.dataOptionsForms$
  }

  private typeSampleEditSubject$ : BehaviorSubject<TypeSample> = new BehaviorSubject<TypeSample>(null)
  private urlUtils = "src/app/shared/utils/data/";

  private resultSubject$: BehaviorSubject<ResponsePaginate> = new BehaviorSubject(null)
  public get resultItems$() {
    return this.resultSubject$
  }
  public setItems(value) {
    this.resultSubject$.next(value)
  }


  private listTypeSampleSubject$: BehaviorSubject<TypeSample[]> = new BehaviorSubject([])

  public get listTypeSampleItems$() {
    if (!this.listTypeSampleSubject$.getValue().length) {
      this.getTypeSamples(1, 1000, '', 'desc').pipe(take(1)).subscribe()
    }
    return this.listTypeSampleSubject$.asObservable()
  }
  public setListTypeSampleItems(value) {
    this.listTypeSampleSubject$.next(value)
  }


  constructor(
    private http:HttpClient,
    private snackBar: MatSnackBar,
  ) {
    // this.loadData()
  }

  get typeSampleOnEdit():Observable<TypeSample>{
    return this.typeSampleEditSubject$
  }

  setTypeSampleOnEdit(value){
    this.typeSampleEditSubject$.next(value)
  }

  storeTypeSample(data): Observable<TypeSample>{
    return this.http.post<Response>(`${environment.API}type_samples`, data).pipe(
      map( v => {
        this.getTypeSamples(1, 10, '', 'desc').pipe(take(1)).subscribe()
        //snacbarr
        this.openSnackBar('Se creó correctamente','success-snack-bar')
        //////////
        return v.data
      })

    )

  }

  updateTypeSample(data): Observable<TypeSample>{
    const id = data.id
    data._method = "put";
    return this.http.post<Response>(`${environment.API}type_samples/${id}`, data).pipe(
      map( v => {
        this.getTypeSamples(1, 10, '', 'desc').pipe(take(1)).subscribe()
        //snacbarr
        this.openSnackBar('Actualizado correctamente','success-snack-bar')
        //////////
        return v.data
      })

    )

  }

  deleteTypeSample(id){
    return this.http.delete<Response>(`${environment.API}type_samples/${id}`).pipe(
      map( v => {

        this.getTypeSamples(1, 10, '', 'desc').pipe(take(1)).subscribe()
        //snacbarr
        this.openSnackBar('Eliminado correctamente','success-snack-bar')
        //////////
        return v.data

      })

    )
  }


  getTypeSample(id) : Observable<TypeSample>{
    return this.http.get<Response>(`${environment.API}type_samples/${id}`).pipe(map(
      res => {
        const resp = res.data
        return resp;
      }
    )

    )
  }


///listar
  getTypeSamples(currentPage = 1, perPage = 20, filter='', sort= 'desc') : Observable<ResponsePaginate>{
    return this.http.get<ResponsePaginate>(`${environment.API}type_samples`, {
      params: new HttpParams()
        .set('page', currentPage.toString())
        .set('filter', filter)
        .set('sort', sort)
        .set('per_page', perPage.toString())

    }).pipe(map(
      res => {

        this.setTypeSampleOnEdit(null)
        this.setListTypeSampleItems(res?.data?.data)

        this.setItems(res)
        const resp = res
        return resp;
      }
    )

    )
  }



  openSnackBar(message: string, refClass:string, action: string = '') {
    this.snackBar.open(message, action, {
      duration: 2000,
      horizontalPosition: 'center',
      verticalPosition: 'top',
      panelClass: refClass
    });
  }
}
