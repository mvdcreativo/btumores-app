import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TypesSamplesComponent } from './types-samples.component';

const routes: Routes = [{ path: '', component: TypesSamplesComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TypesSamplesRoutingModule { }
