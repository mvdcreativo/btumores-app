import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TypesSamplesComponent } from './types-samples.component';

describe('TypesSamplesComponent', () => {
  let component: TypesSamplesComponent;
  let fixture: ComponentFixture<TypesSamplesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TypesSamplesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TypesSamplesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
