import { Component, OnInit, ViewChild } from '@angular/core';
import { Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { Observable, Subscription } from 'rxjs';
import { map, take } from 'rxjs/operators';
import { Column } from 'src/app/shared/components/data-table/interfaces/table';
import { ModalReutilComponent } from 'src/app/shared/components/modals/modal-reutil/modal-reutil.component';
import { ResponsePaginate, Response } from 'src/app/shared/interfaces/response';
import { TypeSample } from './interfaces/type-sample';
import { TypeSamplesService } from './services/types-samples.service';

@Component({
  selector: 'mvd-types-samples',
  templateUrl: './types-samples.component.html',
  styleUrls: ['./types-samples.component.scss']
})
export class TypesSamplesComponent implements OnInit {



  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  ////COLUMNAS TABLA
  public columns: Column[] = [
    { title: 'Nombre', col: 'name' },

  ]

  totalResut: Observable<number>;
  pageDefault = 1;
  perPage: number = 10;
  orden: string = 'desc';
  filter: string = '';
  result: Observable<ResponsePaginate>;
  subscroption: Subscription;
  dataSource: Observable<any[]>;


  constructor(
    private typeSampleService: TypeSamplesService,
    public dialog: MatDialog,

  ) {
    this.result = this.typeSampleService.resultItems$

  }

  ngOnInit(): void {

    this.getTypeSamples(this.pageDefault, this.perPage, this.filter, this.orden)

  }

  paginatorChange(e: PageEvent) {
    //console.log(e);
    this.getTypeSamples(e.pageIndex + 1, e.pageSize)

  }

  getTypeSamples(currentPage?, perPage?, filter?, sort?) {
    this.subscroption = this.typeSampleService.getTypeSamples(currentPage, perPage, filter, sort).subscribe(next => this.loadData());
  }


  loadData() {

    this.dataSource = this.result.pipe(map(v => {
      const dataTable = v.data.data.map(x => {
        return {
          id: x.id,
          name: x.name,

        }
      })
      return dataTable;
    }))

    this.totalResut = this.result.pipe(map(v => v.data.total))
  }

  search(filter) {
    this.getTypeSamples(this.pageDefault, this.perPage, filter, this.orden)
  }


  deleteItem(id): Observable<any> {
    return this.typeSampleService.deleteTypeSample(id)
  }

  itemAction(event) {
    //console.log(event);

    if (event.action === "delete") {
      this.deleteItem(event.element.id).pipe(take(1)).subscribe()
    }
    if (event.action === "edit") {
      this.openDialog('Edita Tipo de Muestra',event.element)
    }
  }

  openDialog(title_form = 'Agrega Tipo de Muestra', data?) {


    const dataDielog = { title: title_form, data: this.setFields(data) };

    const dialogRef = this.dialog.open(ModalReutilComponent, {
      width: '550px',
      data: dataDielog
    });

    return dialogRef.afterClosed().subscribe(result => {
      // this.animal = result;
      if (result) {
        if (result.id) {
          //console.log(result);

          this.updateTypeSample(result)

        } else {
          this.storeTypeSample(result)
        }

      }

    });
  }

  updateTypeSample(data) {
    this.typeSampleService.updateTypeSample(data).pipe(take(1)).subscribe()
  }

  storeTypeSample(data) {
    this.typeSampleService.storeTypeSample(data).pipe(take(1)).subscribe(
      res => {
        //console.log(res);
      }
    )
  }

  setFields(elementEdit? : TypeSample) {
    // console.log(elementEdit);

    const fields = [
      { nameControl: 'id', type: 'hidden', value: elementEdit?.id, label: 'Id' },
      { nameControl: 'name', type: 'text', value: elementEdit?.name, validators: [Validators.required], label: 'Nombre', class:'mvd-col1--1' },
    ]

    return fields
  }

  ngOnDestroy(): void {
    this.subscroption.unsubscribe()
  }


}
