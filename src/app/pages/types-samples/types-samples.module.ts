import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TypesSamplesRoutingModule } from './types-samples-routing.module';
import { TypesSamplesComponent } from './types-samples.component';
import { SharedModule } from 'src/app/shared/shared.module';


@NgModule({
  declarations: [TypesSamplesComponent],
  imports: [
    CommonModule,
    TypesSamplesRoutingModule,
    SharedModule
  ]
})
export class TypesSamplesModule { }
