import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { Router } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import { map, take } from 'rxjs/operators';
import { Patient } from 'src/app/pages/patients/interfaces/patient';
import { Column } from 'src/app/shared/components/data-table/interfaces/table';
import { ResponsePaginate } from 'src/app/shared/interfaces/response';
import { User } from '../../interfaces/user';
import { UsersService } from '../../services/users.service';

@Component({
  selector: 'mvd-users-table',
  templateUrl: './users-table.component.html',
  styleUrls: ['./users-table.component.scss']
})
export class UsersTableComponent implements OnInit {

  // displayedColumns: string[] = ['code', 'title', 'user_owner.name', 'address', 'neighborhood.name'];

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  ////COLUMNAS TABLA
  public columns: Column[] = [
    { title: 'Código', col: 'id' },
    { title: 'Nombre', col: 'name' },
    { title: 'Email', col: 'email' },
  ]

  dataSource: Observable<any[]>;
  users: User[];

  ////paginator
  totalResut: Observable<number>;
  pageDefault = 1;
  perPage: number = 10;
  orden: string = 'desc';
  filter: string = '';
  result: Observable<ResponsePaginate>;
  subscroption: Subscription;
  

  paginatorChange(e: PageEvent) {
  //console.log(e);
    this.getUsers(e.pageIndex + 1, e.pageSize)

  }
  /////////////


  constructor(
    private userService: UsersService,
    public router: Router,
  ) {
    this.result = this.userService.resultItems$

  }


  ngOnInit(): void {
    this.getUsers(this.pageDefault, this.perPage, this.filter, this.orden)
    
  }


  add(){
    this.router.navigate(['/usuarios/usuario'] , { queryParams: { urlReturn: this.router.url} })
  }


  getUsers(currentPage?, perPage?, filter?, sort?) {
    this.subscroption = this.userService.getUsers(currentPage, perPage, filter, sort).subscribe(next => this.loadData());
  }

  loadData() {

    this.dataSource = this.result.pipe(map(v => {
      const dataTable = v.data.data.map(x => {
        return {
          id: x.id,
          name: `${x.name} ${x.last_name}`,
          email: x.email
        }
      })
      return dataTable;
    }))

    this.totalResut = this.result.pipe(map(v => v.data.total))
  }

  search(filter) {
    this.getUsers(this.pageDefault, this.perPage, filter, this.orden )
  }

  deleteItem(id): Observable<any> {
    return this.userService.deleteUser(id)
  }

  itemAction(event) {
  //console.log(event);

    if (event.action === "delete") {
      this.deleteItem(event.element.id).pipe(take(1)).subscribe(res => console.log(res))
    }

    if (event.action === "edit") {
      this.router.navigate(['/usuarios/usuario', event.element.id], {queryParams:{urlReturn : this.router.url}})
    }

  }



  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    this.subscroption.unsubscribe()
  }

}
