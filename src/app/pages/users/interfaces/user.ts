import { Role } from "../../roles/interfaces/role";

export interface User {
    id?: number;
    name?: string;
    last_name: any;
    email: string;
    password?: string;
    email_verified_at?: any;
    created_at?: string;
    updated_at?: string;
    roles?: Role[];
}
export interface CurrentUser {
    token: string;
}