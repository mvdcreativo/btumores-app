import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { concatMap, map, take } from 'rxjs/operators';
import { ResponsePaginate, Response } from 'src/app/shared/interfaces/response';
import { UbicationService } from 'src/app/shared/services/ubications/ubication.service';
import { environment } from 'src/environments/environment';
import { User } from '../interfaces/user'

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  user: User;

  private dataOptionsForms$ : BehaviorSubject<any> = new BehaviorSubject<any>(null)

  public get optionsForms$() {
    return this.dataOptionsForms$
  }

  private userEditSubject$ : BehaviorSubject<User> = new BehaviorSubject<User>(null)
  private urlUtils = "src/app/shared/utils/data/";

  private resultSubject$: BehaviorSubject<ResponsePaginate> = new BehaviorSubject(null)
  public get resultItems$() {
    return this.resultSubject$
  }
  public setItems(value) {
    this.resultSubject$.next(value)
  }



  constructor(
    private http:HttpClient,
    private snackBar: MatSnackBar,
    private ubicationService: UbicationService,
  ) {
    // this.loadData()
    this.userOnEdit.subscribe( res=> { this.user = res })
  }

  get userOnEdit():Observable<User>{
    return this.userEditSubject$
  }

  setUserOnEdit(value){
    this.userEditSubject$.next(value)
  }


  storeUser(data): Observable<User>{
    return this.http.post<Response>(`${environment.API}users`, data).pipe(
      map( v => {
        this.setUserOnEdit(v.data)
        //snacbarr
        this.openSnackBar('Se creó correctamente','success-snack-bar')
        //////////
        return v.data
      })

    )

  }

  updateUser(data): Observable<User>{
    const id = this.userEditSubject$.value.id
    data._method = "put";
    return this.http.post<Response>(`${environment.API}users/${id}`, data).pipe(
      map( v => {
        this.setUserOnEdit(v.data)
        //snacbarr
        this.openSnackBar('Actualizado correctamente','success-snack-bar')
        //////////
        return v.data
      })

    )

  }

  deleteUser(id){
    return this.http.delete<Response>(`${environment.API}users/${id}`).pipe(
      map( v => {
        // console.log(v.data);

        this.getUsers(1, 20, '', 'desc').pipe(take(1)).subscribe()
        //snacbarr
        this.openSnackBar('Eliminado correctamente','success-snack-bar')
        //////////
        return v.data

      })

    )
  }


  getUser(id) : Observable<User>{
    return this.http.get<Response>(`${environment.API}users/${id}`).pipe(map(
      res => {
        this.setUserOnEdit(res.data)
        const resp = res.data
        return resp;
      }
    )

    )
  }


///listar
  getUsers(currentPage = 1, perPage = 20, filter='', sort= 'desc') : Observable<ResponsePaginate>{
    return this.http.get<ResponsePaginate>(`${environment.API}users`, {
      params: new HttpParams()
        .set('page', currentPage.toString())
        .set('filter', filter)
        .set('sort', sort)
        .set('per_page', perPage.toString())

    }).pipe(map(
      res => {
      //console.log(res);

        this.setUserOnEdit(null)
        this.setItems(res)
        const resp = res
        return resp;
      }
    )

    )
  }



  openSnackBar(message: string, refClass:string, action: string = '') {
    this.snackBar.open(message, action, {
      duration: 2000,
      horizontalPosition: 'center',
      verticalPosition: 'top',
      panelClass: refClass
    });
  }

  checkEmailExist(email){
    return this.http.get<string>(`${environment.API}check_email_exist`, {
      params: new HttpParams()
      .set('email', email)
      .set('email_exclude', this.user?.email)
    })
  }
}
