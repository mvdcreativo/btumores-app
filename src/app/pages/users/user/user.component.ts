import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import { map, take } from 'rxjs/operators';
import { MvdValidations } from 'src/app/shared/components/forms/mvd-validations/mvd-validations';
import { ConfirmComponent } from 'src/app/shared/components/modals/confirm/confirm.component';
import { ModalConfirmNoChangesComponent } from 'src/app/shared/components/modals/modal-confirm-no-changes/modal-confirm-no-changes.component';
import { RolesService } from '../../roles/services/roles.service';
import { User } from '../interfaces/user';
import { UsersService } from '../services/users.service';

@Component({
  selector: 'mvd-user',
  templateUrl: 'user.component.html',
  styleUrls: ['user.component.scss']
})
export class UserComponent implements OnInit {
  userEdit: User;
  form: FormGroup;
  urlReturn: string;
  changes: boolean = false;
  subscriptions: Subscription[] = [];
  userId: number;
  roles: any[];

  constructor(
    private userService: UsersService,
    private activatedRoute: ActivatedRoute,
    private fb: FormBuilder,
    private router: Router,
    public dialog: MatDialog,
    public roleService: RolesService
  ) {
    this.createForm()
  }

  ngOnInit(): void {


    this.activatedRoute.paramMap.subscribe(
      (params: Params) => {
        if (params.params.id) {
          this.subscriptions.push(
            this.userService.getUser(params.params.id).subscribe(res=>{
              this.getRoles()

            })
          )
        }

        if (!params.params.id) {
          this.userEdit = null
          this.getRoles()

        }
      }
    )
    this.activatedRoute.queryParamMap.subscribe(
      (res: Params) => {
        this.urlReturn = res?.params?.urlReturn
        this.userId = res?.params?.id
      //console.log(res);

      }
    )

  }

  createForm() {
    this.form = this.fb.group({
      name: [null, Validators.required],
      last_name: [null, Validators.required],
      email: [null, [Validators.required, Validators.email], MvdValidations.validateEmailExist(this.userService)],
      roles: new FormArray([])
    })
  }
  private updateForm(user: User) {
    this.form.patchValue({
      name: user?.name,
      last_name: user?.last_name,
      email: user?.email,
      roles: this.buildRolesFormArray()
    })
  }

  getRoles() {

    this.subscriptions.push(
      this.roleService.getRoles(1, 1000).pipe(map(v => v.data.data)).subscribe(
        res => {
        //console.log(res);
          this.roles = res;
          this.roles = res.map((v, i) => {
            this.roles[i]['group'] = v.name.split('.', 1)[0]
            return v;
          })

          this.subscriptions.push(
            this.userService.userOnEdit.subscribe(res => {
              this.userEdit = res
              this.userEdit ? this.updateForm(this.userEdit) : this.buildRolesFormArray()
            })
          )
          
        //console.log(this.roles);


        })
    )

  }


  buildRolesFormArray() {
    this.roles.forEach((v) => {

      if (this.userEdit?.roles?.length >= 1) {
        // console.log('tine permissos');
        const searchRoles = this.userEdit.roles.find(f => f.name === v.name)
        searchRoles ? this.control.push(new FormControl(true)) : this.control.push(new FormControl(false))
      } else {
        this.control.push(new FormControl(false))
      }

    })
  }

  get control() {
    return this.form.get('roles') as FormArray;
  }

  onSubmit() {

    let userData = Object.assign({}, this.form.value)
    let submitRoles = userData.roles.map((v,i)=>{
        v ? v = this.roles[i].name : v = false
        return v
    }).filter(x=> x!== false)

    userData.roles= submitRoles;

  //console.log(userData);
    

    if (this.userEdit) {
      this.subscriptions.push(
        this.userService.updateUser(userData).subscribe(res => res ? this.back() : false)
      )
    } else {
      this.subscriptions.push(
        this.userService.storeUser(userData).subscribe(res => res ? this.back() : false)

      )
    }
  }

  back() {
    this.urlReturn ? this.router.navigate([this.urlReturn]) : this.router.navigate(['/usuarios'])

  }

  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    this.subscriptions.map(v => v.unsubscribe())
  }
}
