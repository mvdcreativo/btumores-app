import { Component, OnInit, Input, Output, EventEmitter, ViewEncapsulation } from '@angular/core';

import { MatTableDataSource } from '@angular/material/table';

import { Column } from "./interfaces/table";
import { ConfirmComponent } from '../modals/confirm/confirm.component';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'mvd-data-table',
  templateUrl: './data-table.component.html',
  styleUrls: ['./data-table.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class DataTableComponent implements OnInit {

  @Input() dataSource
  @Input() columns: Column[];
  @Input() page: String;
  @Input() actions: boolean = true;
  @Input() btnEdit: boolean = true;
  @Input() btnDelete: boolean = true;
  @Input() btnClone: boolean = false;
  @Input() btnPreview: boolean = false;
  @Input() btnDownload: boolean = false;
  @Input() widthActions : string
  @Output() actionChange: EventEmitter<any> = new EventEmitter;
  displayedColumns: any[];
  url: any;




  constructor(
    public dialog: MatDialog,
    private router: Router

  ) {
    new MatTableDataSource(this.dataSource)

  }

  ngOnInit(): void {
    this.displayedColumns = this.columns.map(v => v.col)
    this.actions ? this.displayedColumns.push('actions') : false
  }

  editItem(element) {
    const action = { action: 'edit', element: element }
    this.actionChange.emit(action)
  }
  addResearch(element) {
    const action = { action: 'addResearch', element: element }
    this.actionChange.emit(action)
  }

  detail(element){
    const action = { action: 'detail', element: element }
    this.actionChange.emit(action)
  }

  preview(element){
    const action = { action: 'preview', element: element }
    this.actionChange.emit(action)
  }

  cloneItem(element) {
    let message = "CLONAR: Creará una nueva muestra con las mismas características que la seleccionada y lo redirigirá para que finalice su edición."
    this.openDialog(element,'CLON', message)
  }

  downloadItem(element) {
    const action = { action: 'download', element: element }
    this.actionChange.emit(action)
  }

  deleteItem(element, message?) {
    this.openDialog(element,'DEL', message)
  }

  openDialog(element, accion, message?): void {
    !message ? message = "" : message = message;
    let name
    if (element?.name) {
      name = element.name
    } else {
      name = element.title
    }

    const dialogRef = this.dialog.open(ConfirmComponent, {
      width: '350px',
      data: { name: name, message: message, value: false }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result?.value) {
        if (accion === "DEL") {
          const action = { action: 'delete', element: element }
          this.actionChange.emit(action)
        }

        if (accion === "CLON") {
          const action = { action: 'clone', element: element }
          this.actionChange.emit(action)
        }
      }
    });

  }


  icon(i) {
  //console.log(i);
  //console.log(this.dataSource[i].ico);
    return this.dataSource[i].ico
  }


  isPageTubes() {
    return this.page === "tube" ? true : false;
  }




}
