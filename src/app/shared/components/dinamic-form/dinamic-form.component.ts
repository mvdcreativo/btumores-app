import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Fields } from "./interfaces/fields";
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { debounceTime } from 'rxjs/operators';
import { Observable, Subscription } from 'rxjs';

import { default as _rollupMoment } from 'moment';

import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { MAT_MOMENT_DATE_FORMATS, MomentDateAdapter } from '@angular/material-moment-adapter';
import { MatDialog } from '@angular/material/dialog';

import { User } from 'src/app/pages/users/interfaces/user';
import { MatDatepickerInputEvent } from '@angular/material/datepicker';
import moment from 'moment';

@Component({
  selector: 'mvd-dinamic-form',
  templateUrl: './dinamic-form.component.html',
  styleUrls: ['./dinamic-form.component.scss'],
  providers: [
    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
    { provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS },
  ],
})
export class DinamicFormComponent implements OnInit {

  @Input() fields: Fields[] = null;
  @Input() locationFieldsData: any;
  @Input() userEdit: User;
  @Output() value: EventEmitter<any> = new EventEmitter

  subscription: Subscription
  colorField: Fields[];
  messageUpload: string = null;
  imgPreview: string | ArrayBuffer;
  dataLocation: { typeLocation: string; elementEdit: { city: any; city_id: any; code: any; neighborhood_id: any; name: any; state_id: any; }; } = null;
  neighborhood_id: number = null;
  formLocation: FormGroup = null;
  formValid = false;
  colorPreview
  public form: FormGroup

  configEditor = {
    modules: {
      syntax: false,

      toolbar: [
        ['bold', 'italic', 'underline', 'strike'],        // toggled buttons
        // ['blockquote', 'code-block'],
        [{ 'color': [] }],          // dropdown with defaults from theme
        // [{ 'font': [] }],
        [{ 'align': [] }],
        [{ 'header': [1, 2, 3, 4, 5, 6, false] }],

        [{ 'header': 1 }, { 'header': 2 }],               // custom button values
        [{ 'list': 'ordered' }, { 'list': 'bullet' }],
        // [{ 'script': 'sub' }, { 'script': 'super' }],      // superscript/subscript
        [{ 'indent': '-1' }, { 'indent': '+1' }],          // outdent/indent
        [{ 'direction': 'rtl' }],                         // text direction

        // [{ 'size': ['small', false, 'large', 'huge'] }],  // custom dropdown


        // ['clean'],                                         // remove formatting button

        // ['link', 'image', 'video']
      ]
    }
  }

  constructor(
    private fb: FormBuilder,
    public dialog: MatDialog,
  ) {
    this.form = this.fb.group({})
  }

  ngOnInit(): void {
    this.buildForm()
  //console.log(this.form.value);

  }


  changeForm(value) {
    // console.log(value);
    const valueform = value;
    if (this.neighborhood_id && this.dataLocation) {
      valueform.neighborhood_id = this.neighborhood_id
    } else {
      this.form.setErrors({ 'invalid': true })
    }

    this.value.emit(value)
  }

  private buildForm() {

    if (this.fields) {

      const controls = this.fields.map(
        v => {
          if (v.type === 'image' && v.value) {
            this.imgPreview = v.value
          }

          return this.form.addControl(v.nameControl, this.fb.control(v.value ?? v.sugerido, v.validators));
        }
      )

    }


  }


  // chanhgeColor() {

  //   const fieldColorControl = this.fields.filter(x => x.type === 'color').map(v => v.nameControl);
  //   this.form.get(fieldColorControl).patchValue(
  //     this.colorPreview
  //   )
  // //console.log(this.colorPreview);

  // }


  formatDate(e : MatDatepickerInputEvent<Date>, campo){
    let brt = e.value;
    this.form.get(campo).setValue(moment(brt).format('YYYY-MM-DD HH:mm:ss'))
  }


  uploadImage(e) {
  //console.log(e.target.files);
    if (e?.target?.files[0]) {

      const selectedFile = e.target.files[0];

      var mimeType = selectedFile.type;
      if (mimeType.match(/image\/*/) == null) {
        this.messageUpload = "No es una imágen";
        this.imgPreview = null;
        this.form.get('image').patchValue(null)
        return;
      }

      this.form.get('image').patchValue(selectedFile)
      this.messageUpload = null
      var reader = new FileReader();
      reader.readAsDataURL(selectedFile);
      reader.onload = (_event) => {
        this.imgPreview = reader.result;
      }
      const fileNames = [];
    //console.log(selectedFile);

      // console.log(this.files);

      // this.imagesServices.uploadImage(this.data, selectedFiles)

    } else {
      const imgExist = this.form.get('image').value
      if (imgExist) {
        this.imgPreview = imgExist
      } else {
        this.imgPreview = null;
        this.form.get('image').patchValue(null)
      }
    }
  }



  setFormLocation(e: FormGroup) {
    this.formLocation = e;
    if (!this.form.get('neighborhood_id')) {
      this.form.addControl('neighborhood_id', this.fb.control(e.get('neighborhood_id').value, Validators.required));
    } else {
      this.form.get('neighborhood_id').setValue(e.get('neighborhood_id').value)
    }
  //console.log(this.form.value);


  }
  setDataLocation() {
    // const neighborhood = this.userEdit?.account?.neighborhood
    // const formatNeighborhood = {
    //   city: neighborhood?.city.name,
    //   city_id: neighborhood?.city_id,
    //   code: neighborhood?.code,
    //   neighborhood_id: neighborhood?.id,
    //   name: neighborhood?.name,
    //   state_id: neighborhood?.city.state_id,
    // }
    // this.dataLocation = {
    //   typeLocation: "user",
    //   elementEdit: formatNeighborhood
    // }
  }




  getErrorMessage(validator){
    let message
    switch (validator) {
      case 'notIncludedIn':{
        message = "El email ya existe"
        break;
      }
      case 'email':{
        message = "Email no válido"
        break;
      }
      case 'date':{
        message = "Formato de fecha inválido"
        break;
      }
      case 'number':{
        message = "Solo números"
        break;
      }
      case 'required':{
        message = "Campo requerido"
        break;
      }

      default:
        message = "No válido"
        break;
    }

    return message
  }
}
