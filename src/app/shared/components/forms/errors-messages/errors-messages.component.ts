import { Component, OnInit, ChangeDetectionStrategy, Input } from '@angular/core';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'mvd-errors-messages',
  templateUrl: './errors-messages.component.html',
  styleUrls: ['./errors-messages.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ErrorsMessagesComponent implements OnInit {

  @Input() campo: any;
  @Input() name: string;
  constructor() { }

  ngOnInit(): void {
  }

  getErrorMessage(validator){
    let message
    switch (validator) {
      
      case 'validateNumDoc':{
        message = `${this.name} ya existe` 
        break;
      }
      case 'validateCode':{
        message = `${this.name} ya existe` 
        break;
      }
      case 'notIncludedIn':{
        message = `${this.name} ya existe` 
        break;
      }
      case 'validateEmailExist':{
        message = "Email ya existe"
        break;
      } 
      case 'email':{
        message = "Email no válido"
        break;
      }      
      case 'date':{
        message = "Formato de fecha inválido"
        break;
      }      
      case 'number':{
        message = `${this.name} admite solamente números`
        break;
      }      
      case 'required':{
        message = `${this.name} es necesario`
        break;
      } 
      case 'validateRoleExist':{
        message = `El rol ya existe`
        break;
      }  
    
      default:
        message = "No válido"
        break;
    }

    return message
  }
}
