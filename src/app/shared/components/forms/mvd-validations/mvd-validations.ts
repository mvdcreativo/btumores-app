import { AbstractControl } from '@angular/forms';
import { map } from 'rxjs/operators';
import { PatientsService } from 'src/app/pages/patients/services/patients.service';
import { RolesService } from 'src/app/pages/roles/services/roles.service';
import { UsersService } from 'src/app/pages/users/services/users.service';

export class MvdValidations {

  constructor() { }

  static validateNumDoc(service){
    return ( control: AbstractControl) =>{
      const value = control.value;
      return service.checkExist('n_doc', value)
      .pipe(
        map(
          (res:any)=> {
            return res.exist === null ? null : {'validateNumDoc': res.exist}
          }
        )
      )
    }
  }

  static validateCode(service){
    return ( control: AbstractControl) =>{
      const value = control.value;
      return service.checkExist('code', value)
      .pipe(
        map(
          (res:any)=> {
            return res.exist === null ? null : {'validateCode': res.exist}
          }
        )
      )
    }
  }

  static validateEmailExist(userService: UsersService){
    return ( control: AbstractControl) =>{
      const value = control.value;
      return userService.checkEmailExist(value)
      .pipe(
        map(
          (res:any)=> {
            return res.exist === null ? null : {'validateEmailExist': res.exist}
          }
        )
      )
    }
  }

  static validateRoleExist(roleService: RolesService){
    return ( control: AbstractControl) =>{
      const value = control.value;
      return roleService.checkRoleExist(value)
      .pipe(
        map(
          (res:any)=> {
            return res.exist === null ? null : {'validateRoleExist': res.exist}
          }
        )
      )
    }
  }
}
