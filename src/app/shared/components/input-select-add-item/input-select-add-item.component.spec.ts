import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InputSelectAddItemComponent } from './input-select-add-item.component';

describe('InputSelectAddItemComponent', () => {
  let component: InputSelectAddItemComponent;
  let fixture: ComponentFixture<InputSelectAddItemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InputSelectAddItemComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InputSelectAddItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
