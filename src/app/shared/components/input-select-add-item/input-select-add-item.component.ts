import { Component, EventEmitter, Input, OnChanges, OnInit, Output, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatAutocompleteTrigger } from '@angular/material/autocomplete';
import { MatDialog } from '@angular/material/dialog';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { Fields } from '../dinamic-form/interfaces/fields';
import { ModalReutilComponent } from '../modals/modal-reutil/modal-reutil.component';
import { ItemOption } from './Interfaces/item-option';
import { Router } from '@angular/router';

@Component({
  selector: 'mvd-input-select-add-item',
  templateUrl: './input-select-add-item.component.html',
  styleUrls: ['./input-select-add-item.component.scss']
})
export class InputSelectAddItemComponent implements OnInit , OnChanges{

  @Input() options : ItemOption[] = []
  @Input() value : any;
  @Input() namePlaceholder : string;
  @Input() addOptionEnabled : boolean = true
  @Input() disabled : boolean = false
  @Input() addOptionRoute: string | undefined

  @Input() fieldsAddItem : Fields[] = [
    { nameControl: 'name', type: 'text', value:"", validators: [Validators.required], label: 'Nombre', class:'mvd-col1--1' },
  ];
  @Output() changeValue : EventEmitter<any> = new EventEmitter();
  @Output() changeClean : EventEmitter<any> = new EventEmitter();
  @Output() addOption : EventEmitter<any> = new EventEmitter();

  @ViewChild(MatAutocompleteTrigger) autocomplete: MatAutocompleteTrigger;



  form: FormGroup;
  filteredItems$: Observable<ItemOption[]>;

  constructor(
    private fb:FormBuilder,
    public dialog: MatDialog,
    private router: Router
  ) {
    this.createForm()
  }


  ngOnInit(): void {
    if (this.value) {
      this.updateForm(this.value)
    }
  }

  createForm(){
    this.form = this.fb.group({
      select: []
    })
  }

  updateForm(value){
    this.form.reset();
    this.form.patchValue({
      select : value
    })
    if (this.disabled) {
      this.form.get('select').disable()
    }else{
      this.form.get('select').enable()
    }

  }

  ngOnChanges(): void {
    this.updateForm(this.value)
    if(this.options?.length){
      this.filteredItems$ =  this.form.get('select').valueChanges
      .pipe(
        startWith(''),
        map(value => {

          const setValueValid = this.options.filter(f=> f.value === value).length
          setValueValid ? this.changeValue.emit(value) : false
          return value ? this._filter(value) : this.options
        })
      );
    }

  }



  private _filter(value: string): ItemOption[] {
    const filterValue = typeof value === 'string' ? value.toLowerCase() : "";
    if(this.options){
      return this.options.filter(item => item.text.toLowerCase().includes(filterValue))
    }

  }

  addItemOption(){

    if (this.addOptionRoute) {
      this.router.navigate([this.addOptionRoute])
      this.addOption.emit()
    }else{
      const title_form = `Agregar ${this.namePlaceholder}`

      const dataDielog = { title: title_form, data: this.fieldsAddItem };

      setTimeout(() => {
        const dialogRef = this.dialog.open(ModalReutilComponent, {
          width: '350px',
          data: dataDielog
        });
        return dialogRef.afterClosed().subscribe(result => {
          // this.animal = result;
          if (result) {
            this.addOption.emit(result)
          }

        });
      }, 100);
    }

  }

  getTextItem(value: any) {
    if(value && this.options && this.options.length){
      console.log(this.options.find(item => item.value === value));

        return this.options.find(item => item.value === value).text;
    }else{
      return value
    }
  }
}
