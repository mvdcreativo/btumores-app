import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalConfirmNoChangesComponent } from './modal-confirm-no-changes.component';

describe('ModalConfirmNoChangesComponent', () => {
  let component: ModalConfirmNoChangesComponent;
  let fixture: ComponentFixture<ModalConfirmNoChangesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ModalConfirmNoChangesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalConfirmNoChangesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
