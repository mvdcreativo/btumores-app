import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Data } from '@angular/router';
import { ConfirmComponent } from '../confirm/confirm.component';

@Component({
  templateUrl: './modal-confirm-no-changes.component.html',
  styleUrls: ['./modal-confirm-no-changes.component.scss']
})
export class ModalConfirmNoChangesComponent {

  constructor(
    public dialogRef: MatDialogRef<ConfirmComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Data) 
    {}


  confirm(){
    this.data.value = true;
    this.dialogRef.close(this.data)
  }
  onNoClick(): void {
    this.dialogRef.close();
  }


}
