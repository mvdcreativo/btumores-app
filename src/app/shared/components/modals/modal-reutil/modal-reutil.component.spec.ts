import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ModalReutilComponent } from './modal-reutil.component';

describe('ModalReutilComponent', () => {
  let component: ModalReutilComponent;
  let fixture: ComponentFixture<ModalReutilComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalReutilComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalReutilComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
