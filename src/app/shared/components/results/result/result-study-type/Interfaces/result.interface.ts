export interface GenResult {
  result_id?: number;
  gen_id: number;
  clasification_id?: number;
  localization: string;
  gen:Gen;
  clasification?:Clasification;
  result?: Result;
  id?: number;
  name?: string;
  created_at?: string;
  updated_at?: string;
  pivot?: {
      result_id: number;
      gen_id: number;
      localization: string;
      clasification_id: number;
  }
}

export interface ClasificationResult {
  result_id?: number;
  gen_id: number;
  clasification_id?: number;
  localization: string;
  gen:Gen;
  result?: Result;
  id?: number;
  name?: string;
  created_at?: string;
  updated_at?: string;
  pivot?: {
      result_id: number;
      gen_id: number;
      localization: string;
      clasification_id: number;
  }
}

export interface GenResultResponse{
  id: number;
  name: string;
  created_at: string;
  updated_at: string;
  pivot: {
      result_id: number;
      gen_id: number;
      localization: string;
      clasification_id: number;
  }
}


export interface Gen {
  id:number;
  name:string;
}

export interface Clasification {
  id:number;
  name:string;
}

export interface Result {
  id?:number;
  patient_id?: number;
  sample_id: number;
  study_type_id:number;
  study_type: StudyType;
  result_study_type_id:number;
  result_study_type: ResultStudyType;
  lab_id: number;
  financing_id: number;
  date_sol: string;
  date_recep: string;
  date_entrega: string;
  obs_result: string;
  reclasificado: boolean;
  reclas_obs: string;
  lab?: Lab;
  financing?: Financing;
  gens_results?: GenResult[]
  clasifications_results?: ClasificationResult[]
}

export interface Lab{
  id?:number;
  name:string;
}

export interface StudyType{
  id?:number;
  name:string;
}

export interface Financing{
  id?:number;
  name:string;
}

export interface ResultStudyType {
  id:number;
  study_type_id:number;
  name:string;
}
