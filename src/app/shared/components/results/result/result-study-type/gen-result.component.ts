import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Observable, Subscription } from 'rxjs';
import { take } from 'rxjs/operators';
import { Patient } from 'src/app/pages/patients/interfaces/patient';
import { ItemOption } from '../../../input-select-add-item/Interfaces/item-option';
import { ResultsService } from '../../services/results.service';
import { GenResult, Result } from './Interfaces/result.interface';
import { GenResultService } from './service/gen-result.service';

@Component({
  selector: 'mvd-gen-result',
  templateUrl: './gen-result.component.html',
  styleUrls: ['./gen-result.component.scss']
})
export class GenResultComponent implements OnInit {




  optionsGens: ItemOption[] = [];
  valueGenSelect: string;
  valueClasificationSelect: string;
  gens: any[];
  options: ItemOption[] = [];
  form: FormGroup
  subscriptions: Subscription [] = [];
  gens_results$: Observable<GenResult[]>;
  result: Result;
  clasifications: any;
  optionsClasifications: any;

  constructor(
    private genResultService : GenResultService,
    private resultService: ResultsService,
    private fb: FormBuilder
  ) {
    this.formatOptionsSelect()
    this.resultService.resultOnEdit$.subscribe(result=>{
      this.result = result;
      console.log(this.result);
      this.createForm()
      if(this.result && this.result.gens_results) {
        const genResults : GenResult []= this.result.gens_results.map(stt=>{

          return {
            result_id: this.result?.id,
            gen_id: stt.id,
            localization: stt.pivot?.localization,
            clasification_id: stt.pivot?.clasification_id,
            gen: {
              id: stt.id,
              name: stt.name
            },
            clasification: {
              id: stt.pivot.clasification_id,
              name: this.result.clasifications_results.filter(f=> f.id === stt.pivot.clasification_id )[0]?.name
            },
          }
        })



        this.genResultService.setGenResults(genResults)
      }

      this.gens_results$ = this.genResultService.genResults$

    })
  }

  ngOnInit(): void {
  }

  createForm(){
    this.form = this.fb.group({
      result_id:[this.result?.id],
      gen_id: [],
      clasification_id: [],
      localization: [null, Validators.required],
      gen_name:[],
    })
  }

  submit(){

    console.log(this.form.value);

    const genResult : GenResult = {
      result_id: this.result?.id,
      gen_id: this.form.get('gen_id').value ,
      clasification_id: this.form.get('clasification_id').value ,
      localization: this.form.get('localization').value,
      clasification: {
        id: this.form.get('clasification_id').value,
        name: this.optionsClasifications.filter(f=> f.value===this.form.get('clasification_id').value )[0].text
      },
      gen: {
        id: this.form.get('gen_id').value,
        name: this.optionsGens.filter(f=> f.value===this.form.get('gen_id').value )[0].text
      },

    }


    this.genResultService.addGenResult(genResult)

    this.form.reset()
    this.valueGenSelect = null
    this.valueClasificationSelect = null

  }

  remove(index){
    this.genResultService.removeGenResult(index)
  }

  ngOnDestroy(): void {
    this.subscriptions.map(s=>s.unsubscribe())
  }



  addGen(value){
    this.resultService.storeGen$(value).subscribe()
  }

  addClasification(value){
    this.resultService.storeClasification$(value).subscribe()
  }


  changeGen(value){
    this.form.get('gen_id').setValue(value)
    this.valueGenSelect = value

  }

  changeClasification(value){
    this.form.get('clasification_id').setValue(value)
    this.valueClasificationSelect = value

  }

  formatOptionsSelect(){
    this.resultService.gens$.subscribe(items=>{
      this.gens = items,
      this.optionsGens = items?.map(v=> ( {value: v.id, text: v.name,} ) )
    })

    this.resultService.clasifications$.subscribe(items=>{
      this.clasifications = items,
      this.optionsClasifications = items?.map(v=> ( {value: v.id, text: v.name,} ) )
    })
  }


}
