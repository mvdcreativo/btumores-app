import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GenResultComponent } from './gen-result.component';

describe('GenResultComponent', () => {
  let component: GenResultComponent;
  let fixture: ComponentFixture<GenResultComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GenResultComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GenResultComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
