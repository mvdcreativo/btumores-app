import { TestBed } from '@angular/core/testing';

import { GenResultService } from './gen-result.service';

describe('GenResultService', () => {
  let service: GenResultService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(GenResultService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
