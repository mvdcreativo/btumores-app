import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { BehaviorSubject, Observable } from 'rxjs';
import { Patient } from 'src/app/pages/patients/interfaces/patient';
import { GenResult } from '../Interfaces/result.interface';


@Injectable({
  providedIn: 'root'
})
export class GenResultService {

  genResultsSubject$ : BehaviorSubject<GenResult[]> = new BehaviorSubject([])

  patient: Patient


  constructor(
    private snackBar: MatSnackBar,

  ) { }


  setGenResults(value : any[]){
    this.genResultsSubject$.next(value)
  }

  get genResults$() : Observable<GenResult[]>{
    return this.genResultsSubject$.asObservable()
  }

  addGenResult(result: GenResult){
    let results
    if(this.genResultsSubject$?.getValue()){
      results =  [...this.genResultsSubject$?.getValue(), result]
    }else{
      results =  [result]
    }

    console.log(result);

    this.setGenResults(results)
  }

  removeGenResult(index){
    const results = [...this.genResultsSubject$.getValue()]
    results.splice(index, 1);
    this.setGenResults(results)
  }

  openSnackBar(message: string, refClass:string, action: string = '') {
    this.snackBar.open(message, action, {
      duration: 2000,
      horizontalPosition: 'center',
      verticalPosition: 'top',
      panelClass: refClass
    });
  }
}
