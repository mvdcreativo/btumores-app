import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDatepickerInputEvent } from '@angular/material/datepicker';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import moment from 'moment';
import { Observable, Subscription } from 'rxjs';
import { tap } from 'rxjs/operators';
import { Patient } from 'src/app/pages/patients/interfaces/patient';
import { PatientsService } from 'src/app/pages/patients/services/patients.service';
import { Sample } from 'src/app/pages/samples/interfaces/sample';
import { SamplesService } from 'src/app/pages/samples/services/samples.service';
import { ItemOption } from '../../input-select-add-item/Interfaces/item-option';
import { ResultsComponent } from '../results.component';
import { ResultsService } from '../services/results.service';
import { Result } from './result-study-type/Interfaces/result.interface';
import { GenResultService } from './result-study-type/service/gen-result.service';

@Component({
  selector: 'mvd-result',
  templateUrl: './result.component.html',
  styleUrls: ['./result.component.scss']
})
export class ResultComponent implements OnInit {
  patient: Patient;
  sample: Sample;
  subscriptions: Subscription = new Subscription();
  form: FormGroup;

  optionsStudies: ItemOption[] = [];
  optionsFinancings: ItemOption[] = [];
  optionsLabs: ItemOption[] = [];
  optionsResultStudyType: ItemOption[] = [];

  valueTypeStudySelect: string;
  valueFinanciacionSelect: string;
  valueLabSelect: any;
  valueResultTypeStudySelect: any;

  studies: any[];
  financings: any[];
  labs: any[];
  result$: Observable<Result>;
  result: any;
  resultStudyTypes: any;
  showResult: boolean = false;
  edit=true

  constructor(
    private resultService: ResultsService,
    public dialogRef: MatDialogRef<ResultsComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private fb: FormBuilder,
    private genResultService: GenResultService,
  ) {

    this.createForm()
    this.formatOptionsSelect()
   }

  ngOnInit(): void {


    this.patient= this.data?.patient
    this.data?.element?.id ? this.edit = true : this.edit = false
    this.patchForm()
    this.result$= this.resultService.resultOnEdit$.pipe(tap(result=> {
      console.log(result);
      this.updateForm(result)
    }))
    this.subscriptions.add(this.genResultService.genResults$.subscribe(res=> this.form.get('gens_results').setValue(res)))


  }

  createForm(){
    this.form = this.fb.group({
      id: [null],
      patient_id:[null , Validators.required],
      sample_id:[null, Validators.required],
      date_sol:[null],
      date_recep:[null],
      date_entrega:[null],
      financing_id:[null],
      lab_id:[null],
      reclasificado:[false],
      reclas_obs:[null],
      obs_result:[null],
      result_study_type_id:[null],
      study_type_id:[null],
      gens_results:[null],
      study_request_id: [null]
    })

  }
  patchForm(){
    this.form.patchValue({
      patient_id: this.patient.id,
      sample_id: this.data.element.sample_id
    })
  }
  updateForm(result?){

    console.log(result);
    console.log(this.data);


    this.form.patchValue({
      id: result?.id,
      patient_id: this.patient.id,
      sample_id: this.data?.element?.sample_id,
      date_sol: result?.date_sol || this.data?.element?.date_sol,
      date_recep: result?.date_recep,
      date_entrega: result?.date_entrega,
      financing_id: result?.financing_id || this.data?.element?.financing_id,
      lab_id: result?.lab_id || this.data?.element?.lab_id,
      study_type_id: result?.study_type_id || this.data?.element?.study_type_id,

      reclasificado: result?.reclasificado,
      reclas_obs: result?.reclas_obs,
      obs_result: result?.obs_result,
      study_request_id: result?.study_request_id || this.data?.element?.study_request_id
    })
    this.changeLab(result?.lab_id || this.data?.element?.lab_id);
    this.changeFinanciacion(result?.financing_id || this.data?.element?.financing_id)
    this.changeStudyType(result?.study_type_id || this.data?.element?.study_type_id, result?.result_study_type_id)

  }

  submit(){
    if(this.form.get('id').value){
      this.subscriptions.add(
        this.resultService.updateResult(this.form.value, this.form.get('id').value).subscribe(res => this.dialogRef.close())
      )
    }else{
      this.subscriptions.add(
        this.resultService.storeResult(this.form.value).subscribe(res => this.dialogRef.close())
      )
    }
  }

  ngOnDestroy(): void {
    this.subscriptions.unsubscribe()
  }



  changeFinanciacion(value){
    this.form.get('financing_id').setValue(value)
    this.valueFinanciacionSelect = value
  }
  addFinanciacion(value){
    this.subscriptions.add(this.resultService.storeFinancings$(value).subscribe())
  }


  changeLab(value){
    this.form.get('lab_id').setValue(value)
    this.valueLabSelect = value

  }
  addLab(value){
    this.subscriptions.add(this.resultService.storeLabs$(value).subscribe())
  }


  changeStudyType(value, result_study_type_id?){
    this.form.get('study_type_id').patchValue(value)



    this.form.get('study_type_id').value ? this.showResult = true : false;


      this.resultStudyTypes = this.studies.filter(v=> v.id === value )[0]?.study_type_results

      this.optionsResultStudyType = this.resultStudyTypes?.map(v=> ( {value: v.id, text: v.name,} ) )
      if (value !== this.valueTypeStudySelect) {
        this.changeResultStudyType(null)
      }


    this.valueTypeStudySelect = value
    result_study_type_id && this.changeResultStudyType(result_study_type_id)
  }

  addStudyType(value){
    this.subscriptions.add(this.resultService.storeTypeSudy$(value).subscribe())
  }

  changeResultStudyType(value){
    this.form.get('result_study_type_id').setValue(value)
    this.valueResultTypeStudySelect = value
  }
  addResultStudyType(value){
    if(this.form.get('study_type_id').value){
      const newData = {
        name : value.name,
        study_type_id : this.form.get('study_type_id').value
      }

      this.subscriptions.add(this.resultService.storeRersultStudiesTypes$(newData).subscribe())
    }
  }

  onNoClick(){
    this.dialogRef.close()
  }

  formatDate(e : MatDatepickerInputEvent<Date>, campo){
    let brt = e.value;
    this.form.get(campo).setValue(moment(brt).format('YYYY-MM-DD HH:mm:ss'))
  }

  formatOptionsSelect(){
    this.subscriptions.add(
      this.resultService.typeStudies$.subscribe(items=>{
        this.studies = items,
        this.optionsStudies = items?.map(v=> ( {value: v.id, text: v.name,} ) )
      })
    )
    this.subscriptions.add(
      this.resultService.rersultStudiesTypes$.subscribe(items=>{
        this.resultStudyTypes = items,
        this.optionsResultStudyType = items?.map(v=> ( {value: v.id, text: v.name,} ) )
      })
    )
    this.subscriptions.add(
      this.resultService.financing$.subscribe(items=>{
        this.financings = items,
        this.optionsFinancings = items?.map(v=> ( {value: v.id, text: v.name,} ) )
      })
    )
    this.subscriptions.add(
      this.resultService.labs$.subscribe(items=>{
        this.labs = items,
        this.optionsLabs = items?.map(v=> ( {value: v.id, text: v.name,} ) )
      })
    )
  }

}
