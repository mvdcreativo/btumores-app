import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { MatDialog} from '@angular/material/dialog';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { Router } from '@angular/router';
import { combineLatest, Observable, Subscription } from 'rxjs';
import { map, take } from 'rxjs/operators';
import { Patient } from 'src/app/pages/patients/interfaces/patient';
import { PatientsService } from 'src/app/pages/patients/services/patients.service';
import { Sample } from 'src/app/pages/samples/interfaces/sample';
import { SamplesService } from 'src/app/pages/samples/services/samples.service';
import { ResponsePaginate } from '../../interfaces/response';
import { Column } from '../data-table/interfaces/table';
import { GenResultService } from './result/result-study-type/service/gen-result.service';
import { ResultComponent } from './result/result.component';
import { ResultsService } from './services/results.service';

@Component({
  selector: 'mvd-results',
  templateUrl: './results.component.html',
  styleUrls: ['./results.component.scss']
})
export class ResultsComponent implements OnInit {

  subscriptions: Subscription = new Subscription();
  patient: Patient;
  sample: Sample;
  patient$: Observable<Patient>;
  sample$: Observable<Sample>;
  // displayedColumns: string[] = ['code', 'title', 'user_owner.name', 'address', 'neighborhood.name'];

  dataSource: Observable<any[]>;
  samples: Sample[];

  ////paginator
  totalResut: Observable<number>;
  pageDefault = 1;
  perPage: number = 10;
  orden: string = 'desc';
  filter: string = '';
  currentPage: number;

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  ////COLUMNAS TABLA
  public columns: Column[] = [
    { title: 'Id', col: 'id' },
    { title: 'Código Muestras', col: 'sample_code' },
    { title: 'Fecha de Solicitud', col: 'date_sol',pipe: 'dd/MM/yyyy' },
    { title: 'Tipo de estudio', col: 'study_type' },
    { title: 'Resultado Tipo', col: 'result_study_type' },

    { title: 'Recepción', col: 'date_recep',pipe: 'dd/MM/yyyy' },
    { title: 'Entrega', col: 'date_entrega',pipe: 'dd/MM/yyyy' },
    { title: 'Observaciones', col: 'obs_result' },
    { title: 'Genes', col: 'genesCount', type:'genesCount' },
  ]
  results$: Observable<ResponsePaginate>;


  constructor(
    public dialog: MatDialog,
    private patientServices : PatientsService,
    private sampleServices : SamplesService,
    private resultService: ResultsService,
    private genResultService: GenResultService,
    public router: Router,
  ) {
    this.results$ = this.resultService.resultsItems$
    this.patient$ = this.patientServices.patientOnEdit
    this.sample$= this.sampleServices.sampleOnEdit
  }

  ngOnInit(): void {
    this.subscriptions.add(
      combineLatest([this.patient$,this.sample$]).subscribe( ([patient,sample]) => {

        this.patient = patient ? patient : sample?.patient;
        this.sample= sample;
        this.getResults(this.pageDefault, this.perPage, this.filter, this.orden,this.sample?.id,this.patient?.id)
      })
    )


  }

  openDialog(patient, element?) {

    const data = {patient: patient, element: element}
    const dialogRef = this.dialog.open(ResultComponent, {
      width: '70vw',
      minWidth: '370px',
      height: '90vh',
      data: data
    });

    return dialogRef.afterClosed().subscribe(result => {
      // this.animal = result;
      this.resultService.setItem(null)
      this.genResultService.setGenResults(null)
      this.getResults(this.currentPage, this.perPage, this.filter,'', this.sample?.id, this.patient?.id)
    });
  }







  paginatorChange(e: PageEvent) {
  //console.log(e);
    this.getResults(e.pageIndex + 1, e.pageSize)
    this.currentPage = e.pageIndex + 1;

  }
  /////////////



  add(){
    const el = {
          patient_id: this.patient.id,
          sample_id: this.sample.id,
        }
    this.openDialog(this.patient, el)
  }


  getResults(currentPage?, perPage?, filter?, sort?, sample_id?,patient_id?) {
    this.subscriptions.add(
      this.resultService.getResults$(currentPage, perPage, filter, sort,sample_id,patient_id).subscribe(next => this.loadData())
    )
  }

  loadData() {

    this.dataSource = this.results$.pipe(map(v => {

      console.log(v);
      const dataTable = v.data.data.map(x => {

        return {
          id: x.id,
          patient_id: x?.patient_id,
          sample_id: x?.sample_id,
          lab_name: x?.lab?.name,
          financing_name: x?.financing?.name,
          reclasificado_full: `${x?.reclasificado} - ${x?.reclas_obs}`,
          date_sol: x?.date_sol,
          date_recep: x?.date_recep,
          date_entrega: x?.date_entrega,
          obs_result: x?.obs_result,
          sample_code: x?.sample?.code,
          lab_id: x?.lab_id,
          financing_id: x?.financing_id,
          reclasificado: x?.reclasificado,
          reclas_obs: x?.reclas_obs,
          study_type: x?.study_type?.name,
          result_study_type: x?.result_study_type?.name,
          genesCount: x?.gens_results,
          study_request_id: x?.study_request_id
        }
      })
      return dataTable;
    }))

    this.totalResut = this.results$.pipe(map(v => v.data.total))
  }

  search(filter) {
    this.getResults(this.pageDefault, this.perPage, filter, this.orden, this.sample?.id, this.patient?.id )
  }

  deleteItem(id): Observable<any> {
    return
    // return this.resultService
  }


  itemAction(event) {
  //console.log(event);

    if (event.action === "delete") {
      this.deleteItem(event.element.id).pipe(take(1)).subscribe(res => console.log(res))
    }

    if (event.action === "edit") {
      this.subscriptions.add(this.resultService.showResult(event.element.id).subscribe())
      this.openDialog(this.patient, event.element)
    }


  }



  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    this.subscriptions.unsubscribe()
  }
}
