import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { ResponsePaginate, Response } from 'src/app/shared/interfaces/response';
import { environment } from 'src/environments/environment';
import { Result, GenResult, GenResultResponse, Gen } from '../result/result-study-type/Interfaces/result.interface';
import { GenResultService } from '../result/result-study-type/service/gen-result.service';

@Injectable({
  providedIn: 'root'
})
export class ResultsService {

  resultSubject$ : BehaviorSubject<Result> = new BehaviorSubject(null)
  resultsSubject$ : BehaviorSubject<ResponsePaginate> = new BehaviorSubject(null)

  rersultStudiesTypesSubject$: BehaviorSubject<any> = new BehaviorSubject([]);
  typeStudiesSubject$ : BehaviorSubject<any> = new BehaviorSubject([])
  genSubject$ : BehaviorSubject<any> = new BehaviorSubject([])
  financingSubject$: BehaviorSubject<any> = new BehaviorSubject([])
  labSubject$: BehaviorSubject<any> = new BehaviorSubject([])
  clasificationSubject$: BehaviorSubject<any> = new BehaviorSubject([]);


  constructor(
    private http: HttpClient,
    private snackBar: MatSnackBar,
    private genResultService: GenResultService
  ) { }

  public setItems(value) {
    this.resultsSubject$.next(value)
  }
  public setItem(value) {
    this.resultSubject$.next(value)
  }


  get resultOnEdit$() {
    return this.resultSubject$.asObservable()
  }

  get resultsItems$(){
    return this.resultsSubject$.asObservable()
  }
  getResults$(currentPage = 1, perPage = 20, filter='', sort= 'desc',sample_id='', patient_id=''): Observable<any>{
    return this.http.get<ResponsePaginate>(`${environment.API}results`, {
      params: new HttpParams()
        .set('page', currentPage.toString())
        .set('filter', filter)
        .set('sort', sort)
        .set('per_page', perPage.toString())
        .set('patient_id', patient_id.toString())
        .set('sample_id', sample_id.toString())

    }).pipe(map(
      res => {

        this.setItem(null)
        this.setItems(res)
        const resp = res
        return resp;
      }
    )

    )
  }

  storeResult(data: Result){
    const newData: any = {... data}
    if (data.gens_results?.length) {
      const gens_resultsTransform = data.gens_results.map(v=> {
        delete v.gen;
        delete v.clasification

        return { clasification_id: v.clasification_id , gen_id : v.gen_id , localization: v.localization}
      });

      newData.gens_results = gens_resultsTransform;
    }




    return this.http.post<Response>(`${environment.API}results`, newData).pipe(
      map( v => {
        const data = {...v.data};
        console.log(data.gens_results);

        const gens_results = data.gens_results;
        this.genResultService.setGenResults(gens_results);

        this.setItem(data);
        //snacbarr
        this.openSnackBar('Se creó correctamente','success-snack-bar')
        //////////
        return data
      })

    )
  }

  updateResult(data: Result, id){
    const newData: any = {... data}
    if (data.gens_results?.length) {
      const gens_resultsTransform = data.gens_results.map(v=> {
        delete v.gen;
        delete v.clasification

        return { clasification_id: v.clasification_id , gen_id : v.gen_id , localization: v.localization}
      });
      newData.gens_results = gens_resultsTransform;
    }

    return this.http.put<Response>(`${environment.API}results/${id}`, newData).pipe(
      map( v => {
        const data = {...v.data};
        const gens_results = data.gens_results;
        this.genResultService.setGenResults(gens_results);

        this.setItem(data);
        //snacbarr
        this.openSnackBar('Se actualizó correctamente','success-snack-bar')
        //////////
        return data
      })

    )
  }



  showResult(id){

    return this.http.get<Response>(`${environment.API}results/${id}`).pipe(
      map( v => {
        const data = {...v.data};
        const gens_results = data.gens_results;
        this.genResultService.setGenResults(gens_results);

        this.setItem(data);
        return data
      })

    )
  }

  transformResult(gens_resultsResponse : GenResultResponse[]) : GenResult[]{

    console.log(gens_resultsResponse);
    const gens_results = gens_resultsResponse.map(res=>{
      return {
        gen_id: res.id,
        localization: res.pivot.localization,
        clasification_id: res.pivot.clasification_id,
        gen: res,
      }
    })
    return gens_results
  }



//TYPE STUDIES
  get typeStudies$(): Observable<any[]>{
    if (this.typeStudiesSubject$.value?.length) {
      return this.typeStudiesSubject$.asObservable()
    }else{
      this.getStudies().subscribe()
      return this.typeStudiesSubject$.asObservable()
    }
  }
  setTypesStudies(value : any[]){
    this.typeStudiesSubject$.next(value)
  }
  getStudies(): Observable<any[]>{
    return this.http.get<ResponsePaginate>(`${environment.API}study_types`, {
      params: new HttpParams()
        .set('page', 1)
        .set('sort', 'asc')
        .set('per_page', 1000)
    }).pipe(
      map( v => {
        if (v.data.data.length) {
          this.setTypesStudies(v.data.data);
        }else{
          this.setTypesStudies([]);
        }
        return v.data.data
      })

    )
  }
  storeTypeSudy$(data) {
    return this.http.post<Response>(`${environment.API}study_types`, data).pipe(
      map( v => {
        let bds = this.typeStudiesSubject$.getValue();
        if (v.data) {
          bds = [...bds, v.data]
        }
        this.setTypesStudies(bds);
        //snacbarr
        this.openSnackBar('Se creó correctamente','success-snack-bar')
        //////////
        return v.data
      })

    )
  }
/////////////////


//RESULT STUDY TYPES
get rersultStudiesTypes$(): Observable<any[]>{
  if (this.rersultStudiesTypesSubject$.value?.length) {
    return this.rersultStudiesTypesSubject$.asObservable()
  }else{
    this.getStudies().subscribe()
    return this.rersultStudiesTypesSubject$.asObservable()
  }
}
setRersultStudiesTypes(value : any[]){
  this.rersultStudiesTypesSubject$.next(value)
}
getRersultStudiesTypes(studyTypeId= ''): Observable<any[]>{
  return this.http.get<ResponsePaginate>(`${environment.API}result_study_types`, {
    params: new HttpParams()
      .set('page', 1)
      .set('sort', 'asc')
      .set('per_page', 1000)
      .set('study_type_id', studyTypeId)
  }).pipe(
    map( v => {
      if (v.data.data.length) {
        this.setRersultStudiesTypes(v.data.data);
      }else{
        this.setRersultStudiesTypes([]);
      }
      return v.data.data
    })

  )
}
storeRersultStudiesTypes$(data) {
  return this.http.post<Response>(`${environment.API}result_study_types`, data).pipe(
    map( v => {
      let bds = this.rersultStudiesTypesSubject$.getValue();
      if (v.data) {
        bds = [...bds, v.data]
      }
      this.setRersultStudiesTypes(bds);
      const studyType = [...this.typeStudiesSubject$.getValue()].filter(f=> f.id === v.data.study_type_id)[0]
      const addStudyType = [v.data , ...studyType.study_type_results]
      studyType.study_type_results = addStudyType;
      const studyTypesSinStudyType = [...this.typeStudiesSubject$.getValue()].filter(f=> f.id !== v.data.study_type_id)
      this.typeStudiesSubject$.next([studyType, ...studyTypesSinStudyType])
      //snacbarr
      this.openSnackBar('Se creó correctamente','success-snack-bar')
      //////////
      return v.data
    })

  )
}
/////////////////



//GEN
get gens$(): Observable<any[]>{
  if (this.genSubject$.value?.length) {
    return this.genSubject$.asObservable()
  }else{
    this.getGens().subscribe()
    return this.genSubject$.asObservable()
  }
}
setGens(value : any[]){
  this.genSubject$.next(value)
}
getGens(): Observable<any[]>{
  return this.http.get<ResponsePaginate>(`${environment.API}gens`, {
    params: new HttpParams()
      .set('page', 1)
      .set('sort', 'asc')
      .set('per_page', 1000)
  }).pipe(
    map( v => {
      if (v.data.data.length) {
        this.setGens(v.data.data);
      }else{
        this.setGens([]);
      }
      return v.data.data
    })

  )
}
storeGen$(data) {
  return this.http.post<Response>(`${environment.API}gens`, data).pipe(
    map( v => {
      let bds = this.genSubject$.getValue();
      if (v.data) {
        bds = [...bds, v.data]
      }
      this.setGens(bds);
      //snacbarr
      this.openSnackBar('Se creó correctamente','success-snack-bar')
      //////////
      return v.data
    })

  )
}
/////////////////


//CLASIFICATION
get clasifications$(): Observable<any[]>{
  if (this.clasificationSubject$.value?.length) {
    return this.clasificationSubject$.asObservable()
  }else{
    this.getClasifications().subscribe()
    return this.clasificationSubject$.asObservable()
  }
}
setClasifications(value : any[]){
  this.clasificationSubject$.next(value)
}
getClasifications(): Observable<any[]>{
  return this.http.get<ResponsePaginate>(`${environment.API}clasifications`, {
    params: new HttpParams()
      .set('page', 1)
      .set('sort', 'asc')
      .set('per_page', 1000)
  }).pipe(
    map( v => {
      if (v.data.data.length) {
        this.setClasifications(v.data.data);
      }else{
        this.setClasifications([]);
      }
      return v.data.data
    })

  )
}
storeClasification$(data) {
  return this.http.post<Response>(`${environment.API}clasifications`, data).pipe(
    map( v => {
      let bds = this.clasificationSubject$.getValue();
      if (v.data) {
        bds = [...bds, v.data]
      }
      this.setClasifications(bds);
      //snacbarr
      this.openSnackBar('Se creó correctamente','success-snack-bar')
      //////////
      return v.data
    })

  )
}
/////////////////


//Financing
get financing$(): Observable<any[]>{
  if (this.financingSubject$.value?.length) {
    return this.financingSubject$.asObservable()
  }else{
    this.getFinancings().subscribe()
    return this.financingSubject$.asObservable()
  }
}
setFinancings(value : any[]){
  this.financingSubject$.next(value)
}
getFinancings(): Observable<any[]>{
  return this.http.get<ResponsePaginate>(`${environment.API}financings`, {
    params: new HttpParams()
      .set('page', 1)
      .set('sort', 'asc')
      .set('per_page', 1000)
  }).pipe(
    map( v => {
      if (v.data.data.length) {
        this.setFinancings(v.data.data);
      }else{
        this.setFinancings([]);
      }
      return v.data.data
    })

  )
}
storeFinancings$(data) {
  return this.http.post<Response>(`${environment.API}financings`, data).pipe(
    map( v => {
      let bds = this.financingSubject$.getValue();
      if (v.data) {
        bds = [...bds, v.data]
      }
      this.setFinancings(bds);
      //snacbarr
      this.openSnackBar('Se creó correctamente','success-snack-bar')
      //////////
      return v.data
    })

  )
}
/////////////////



//Lab
get labs$(): Observable<any[]>{
  if (this.labSubject$.value?.length) {
    return this.labSubject$.asObservable()
  }else{
    this.getLabs().subscribe()
    return this.labSubject$.asObservable()
  }
}
setLabs(value : any[]){
  this.labSubject$.next(value)
}

getLabs(): Observable<any[]>{
  return this.http.get<ResponsePaginate>(`${environment.API}labs_result`, {
    params: new HttpParams()
      .set('page', 1)
      .set('sort', 'asc')
      .set('per_page', 1000)
  }).pipe(
    map( v => {
      if (v.data.data.length) {
        this.setLabs(v.data.data);
      }else{
        this.setLabs([]);
      }
      return v.data.data
    })

  )
}
storeLabs$(data) {
  return this.http.post<Response>(`${environment.API}labs_result`, data).pipe(
    map( v => {
      let bds = this.labSubject$.getValue();
      if (v.data) {
        bds = [...bds, v.data]
      }
      this.setLabs(bds);
      //snacbarr
      this.openSnackBar('Se creó correctamente','success-snack-bar')
      //////////
      return v.data
    })

  )
}
/////////////////

openSnackBar(message: string, refClass:string, action: string = '') {
  this.snackBar.open(message, action, {
    duration: 2000,
    horizontalPosition: 'center',
    verticalPosition: 'top',
    panelClass: refClass
  });
}
}
