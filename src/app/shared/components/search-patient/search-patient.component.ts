import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Patient } from 'src/app/pages/patients/interfaces/patient';
import { PatientsService } from 'src/app/pages/patients/services/patients.service';

@Component({
  selector: 'mvd-search-patient',
  templateUrl: './search-patient.component.html',
  styleUrls: ['./search-patient.component.scss']
})
export class SearchPatientComponent implements OnInit {

  searched: boolean=false;
  resSearch: Observable<Patient[]>;
  resValue:string;
  patient: Patient;
  valueSearch: string;
  form:FormGroup;
  display = [['account','n_doc_iden'],'name']
  view: boolean = false;



  constructor(
    private router: Router,
    private patientService: PatientsService
  ) { }

  ngOnInit(): void {
  }



  setForm(){
    // this.form.patchValue({
    //   name: this.patient?.name,
    //   last_name: this.patient?.last_name,
    //   email: this.patient?.email,
    //   n_doc_iden: this.patient?.account?.n_doc_iden,
    //   type_doc_iden: 'CI',
    //   address_one: this.patient?.account?.address_one,
    //   phone_one: this.patient?.account?.phone_one,
    // })
    // console.log(this.form.value);
    
  }

  search(value:string){
    this.patient = null;
    if(value && value.length >= 3){
      this.valueSearch = value
      this.resSearch = this.patientService.getPatients(1,10,value).pipe(map(v=>v.data.data))
      
    }else{
      this.valueSearch =null
    }

  }

  addPatient(){
    this.router.navigate(['/alumnos/editar-usuario'],{ queryParams: { returnUrl: this.router.url } } )
  }

  selectPatient(item){
    this.patient = item
    this.resValue = null
    this.valueSearch = null
    this.resSearch=null
    this.searched= !this.searched
    if (this.patient) {
      this.setForm()
    }
  }
}
