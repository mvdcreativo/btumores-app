import { Directive, Input, TemplateRef, ViewContainerRef } from '@angular/core';
import { AuthService } from 'src/app/auth/auth.service';
import { User } from 'src/app/pages/users/interfaces/user';
import { UsersService } from 'src/app/pages/users/services/users.service';

@Directive({
  selector: '[mvdPermissions]'
})
export class PermissionsDirective {

  private currentUser: User;
  private permissions = []


  constructor(
    private userService: UsersService,
    private viewContainer: ViewContainerRef,
    private templateRef: TemplateRef<any>,
    private authService: AuthService
  ) { }

  ngOnInit(): void {
    this.authService.currentUser.subscribe(
      (user: User) => {
        this.currentUser = user;
        this.updateView();
      }
    )
  }

  @Input()
  set mvdPermissions(val: Array<string>) {
    // console.log('****', val);
    // this.viewContainer.createEmbeddedView(this.templateRef)
    this.permissions = val
    this.updateView();

  }

  private updateView(){
    this.viewContainer.clear();
    if(this.checkPermission()){
      this.viewContainer.createEmbeddedView(this.templateRef)
    }
  }

  private checkPermission():boolean{
    let hasPermission = false;
    if(this.currentUser?.roles){
      const permissionUser = this.currentUser.roles.map(v=>v.permissions)[0]
      // console.log(permissionUser);
      
      for(const checkPermission of this.permissions){
        // console.log(checkPermission);
        
        const permissionFound = permissionUser.find((p)=>{
          // console.log(p);
          
          return (p.name === checkPermission);
        })

        if(permissionFound){
          // console.log('Si tiene permiso: ', permissionFound);
          hasPermission = true;
          break;
          
        }
      }

    }
    return hasPermission;
  }
   
}
