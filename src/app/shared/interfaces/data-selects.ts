export interface DataSelects {
    selects:OptionsSelect[]
}

export interface OptionsSelect {
    name: string;
    value: any;
}
