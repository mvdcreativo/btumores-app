import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filterState'
})
export class FilterStatePipe implements PipeTransform {

  transform(items:Array<any>, id?) {    
    if(id){
      return items?.filter(item => item.country_id == id);
    } 
    return items;  
  }
}
