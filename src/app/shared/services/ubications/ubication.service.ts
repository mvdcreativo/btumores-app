import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpParams } from '@angular/common/http';

import { State, City, Neighborhood, Country } from "src/app/shared/interfaces/ubication";
import { map, take } from 'rxjs/operators';
import { Response, ResponsePaginate } from 'src/app/shared/interfaces/response';

@Injectable({
  providedIn: 'root'
})
export class UbicationService {

  private resultCountriesSubject$: BehaviorSubject<Country[]> = new BehaviorSubject([])
  private resultStatesSubject$: BehaviorSubject<State[]> = new BehaviorSubject([])
  private resultCitiesSubject$: BehaviorSubject<City[]> = new BehaviorSubject([])

  constructor(
    private http:HttpClient
  ) { }

    //countries
    public get countriesItems$() {
      if (!this.resultCountriesSubject$.getValue().length) {
        this.getCountries().pipe(take(1)).subscribe()
      }
      return this.resultCountriesSubject$.asObservable()
    }
    public setCountriesItems(value) {
      this.resultCountriesSubject$.next(value)
    }
    //states
    public get statesItems$() {
      if (!this.resultStatesSubject$.getValue().length) {
        this.getStates().pipe(take(1)).subscribe()
      }
      return this.resultStatesSubject$.asObservable()
    }
    public setStatesItems(value) {
      this.resultStatesSubject$.next(value)
    }
    //cities
    public get citiesItems$() {
      if (!this.resultCitiesSubject$.getValue().length) {
        this.getCities().pipe(take(1)).subscribe()
      }
      return this.resultCitiesSubject$.asObservable()
    }
    public setCitiesItems(value) {
      this.resultCitiesSubject$.next(value)
    }


  public getStates(): Observable<State[]> {
    return this.http.get<ResponsePaginate>(environment.API + 'states', {
      params: new HttpParams()
        .set('per_page',10000)
    }).pipe(
      map( v => {
        this.setStatesItems(v.data.data)
        return v.data.data
      })
    );
  }

  public getCities(): Observable<City[]> {
    return this.http.get<ResponsePaginate>(environment.API + 'cities', {
      params: new HttpParams()
        .set('per_page',10000)
    }).pipe(
      map( v => {
        this.setCitiesItems(v.data.data)
        return v.data.data
      })
    );
  }

  public getCountries(): Observable<Country[]> {
    return this.http.get<ResponsePaginate>(environment.API + 'countries', {
      params: new HttpParams()
        .set('per_page',10000)
    }).pipe(
      map( v => {
        this.setCountriesItems(v.data.data)
        return v.data.data
      })
    );
  }



}
