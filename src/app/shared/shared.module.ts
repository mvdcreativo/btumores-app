import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from './material/material.module';
import { HttpClientModule } from '@angular/common/http';
import { DataTableComponent } from './components/data-table/data-table.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

import { PipesModule } from "./pipes/pipes.module";
import { InputSearchComponent } from './components/input-search/input-search.component';
import { ModalLocationsComponent } from './components/modals/modal-locations/modal-locations.component';
import { ConfirmComponent } from './components/modals/confirm/confirm.component';
import { SnackBarComponent } from './components/snack-bar/snack-bar.component';
import { ModalReutilComponent } from './components/modals/modal-reutil/modal-reutil.component';
import { DinamicFormComponent } from './components/dinamic-form/dinamic-form.component';
import { FormLocationsComponent } from './components/forms/form-locations/form-locations.component';
import { ModalSearchComponent } from './components/modals/modal-search/modal-search.component';

import { InputAutocompleteComponent } from './components/forms/input-autocomplete/input-autocomplete.component';
import { QuillModule } from 'ngx-quill';
import { FlexLayoutModule } from '@angular/flex-layout';
import { SearchPatientComponent } from './components/search-patient/search-patient.component';
import { ErrorsMessagesComponent } from './components/forms/errors-messages/errors-messages.component';
import { ModalConfirmNoChangesComponent } from './components/modals/modal-confirm-no-changes/modal-confirm-no-changes.component';
import { CheckChildrenDirective } from './directives/check-children.directive';
import { SelectGroupDirective } from './directives/select-group.directive';
import { PermissionsDirective } from './directives/permissions.directive';
import { ModalPreviewComponent } from './components/modals/modal-preview/modal-preview.component';
import { NgxDocViewerModule } from 'ngx-doc-viewer';
import { InputSelectAddItemComponent } from './components/input-select-add-item/input-select-add-item.component';
import { ResultsComponent } from './components/results/results.component';
import { ResultComponent } from './components/results/result/result.component';
import { GenResultComponent } from './components/results/result/result-study-type/gen-result.component';

@NgModule({
  declarations: [
    DataTableComponent,
    InputSearchComponent,
    ModalLocationsComponent,
    ConfirmComponent,
    SnackBarComponent,
    ModalReutilComponent,
    DinamicFormComponent,
    FormLocationsComponent,
    ModalSearchComponent,
    InputAutocompleteComponent,
    SearchPatientComponent,
    ErrorsMessagesComponent,
    ModalConfirmNoChangesComponent,
    CheckChildrenDirective,
    SelectGroupDirective,
    PermissionsDirective,
    ModalPreviewComponent,
    InputSelectAddItemComponent,
    ResultsComponent,
    ResultComponent,
    GenResultComponent,
  ],
  imports: [
    CommonModule,
    MaterialModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule,
    PipesModule,
    FlexLayoutModule,
    NgxDocViewerModule,
    QuillModule.forRoot(),

  ],
  exports: [
    MaterialModule,
    ReactiveFormsModule,
    FormsModule,
    DataTableComponent,
    PipesModule,
    InputSearchComponent,
    ModalLocationsComponent,
    ModalReutilComponent,
    DinamicFormComponent,
    FormLocationsComponent,
    InputAutocompleteComponent,
    FlexLayoutModule,
    SearchPatientComponent,
    ErrorsMessagesComponent,
    CheckChildrenDirective,
    SelectGroupDirective,
    PermissionsDirective,
    ModalPreviewComponent,
    InputSelectAddItemComponent,
    ResultComponent,
    ResultsComponent
  ]
})
export class SharedModule { }
