export const environment = {
  production: true,
  API: "https://api.oncogene.uy/api/",
  APIAuth: "https://api.oncogene.uy/api/auth/",

  // API: "https://api.bt.picaworks.com/api/",
  // APIAuth: "https://api.bt.picaworks.com/api/auth/",

  // API: "http://api_bt.test/api/",
  // APIAuth: "http://api_bt.test/api/auth/",
  ////ROUTES API
  routesCRUD: {
    patients: "patients",
    states: "states",
    cities: "cities",
    countries: "countries",
    accounts: "accounts",
    municipalities: "municipalities",
    neighborhoods: "neighborhoods",
    categories: "categories",
    transactionTypes: "courses_sections",
    publications: "publications",
    currencies: "currencies",
    roles: "roles",
    length_units: "length_units",
    levels: "levels",
    instructors:"instructors",
    students: "students",
    adquired_skills: "adquired_skills",
    course_sections: "course_sections",
    lessons: "lessons",
    schedules: "schedules",
    users: "users",
    orders: "orders",
    enrollments:"enrollments"

  }

};
